<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<!-- include libraries(jQuery, bootstrap) -->
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
<!-- include summernote css/js-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<!-- include summernote-ko-KR -->
<script src="/resources/js/summernote-ko-KR.js"></script>

<title>게시판 등록</title>

<script src="js/board.js"></script>

<script>
$(document).ready(function() {
	  $('#summernote').summernote({
 	    	placeholder: 'content',
	        minHeight: 370,
	        maxHeight: null,
	        focus: true, 
	        lang : 'ko-KR'
	  });
	});
</script>

</head>
<body>
<div id="wrap">
	<h1>게시글 등록</h1>
	<form name="frm" method="post" action="./api/addBook" enctype="multipart/form-data">
	
			<table>
				<tr>
				<th>도서명</th>
				<td>
				<input type="text" name="title">
				</td>
				</tr>
				<tr>
				<th>저자명</th>
				<td>
				<input type="text" name="author">
				</td>
				</tr>
				<tr>
				<th>isbn</th>
				<td>
				<input type="text" name="isbn">
				</td>
				</tr>
				<tr>
				<th>페이지수</th>
				<td>
				<input type="text" name="pageNum">
				</td>
				</tr>
				
				<tr>
				<th>판형</th>
				<td>
				<input type="text" name="pan">
				</td>
				</tr>
				
				<tr>
				<th>원가</th>
				<td>
				<input type="text" name="price">
				</td>
				</tr>
				
				<tr>
				<th>세일가</th>
				<td>
				<input type="text" name="salePrice">
				</td>
				</tr>
				
				<tr>
				<th>발행일</th>
				<td>
				<input type="text" name="dt">
				</td>
				</tr>
				
				<tr>
				<th>도서분류</th>
				<td>
					<select name = "bookType">
						<option value = "1" selected>수험도서</option>
						<option value = "2">전문도서</option>
						<option value = "3">미디어북</option>
					</select>
				</td>
				</tr>
				
				<tr>
				<th>이미지등록</th>
				<td>
				<input type="file" name="image">
				</td>
				</tr>
				
				<tr>
				<th>책 소개</th>
				<td><input type="text" name="comment"></td>
				</tr>
			</table>
		<input type="submit" value="등록" onclick="return boardCheck()">
		<input type="reset" value="다시작성">
	 
	</form>
</div>
</body>
</html>
