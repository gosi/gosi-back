<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<!-- include libraries(jQuery, bootstrap) -->
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
<!-- include summernote css/js-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<!-- include summernote-ko-KR -->
<script src="/resources/js/summernote-ko-KR.js"></script>

<script src="js/board.js"></script>
<script>
$(document).ready(function() {
	  $('#summernote').summernote({
 	    	placeholder: 'content',
	        minHeight: 370,
	        maxHeight: null,
	        focus: true, 
	        lang : 'ko-KR'
	  });
	});
</script>

</head>
<body>
<div id="wrap">
	<h1>도서소개 수정</h1>
	<form name="frm" method="post" action="./api/book2" enctype="multipart/form-data">
	
			<table>
				<tr>
				<th>도서번호</th>
				<td>
				<input type="text" name="bookId">
				</td>
				</tr>
				
				
				<tr>
				<th>도서소개</th>
				<td><textarea id="summernote" name="content1"></textarea></td>
				</tr>
			</table>
		<input type="submit" value="등록" onclick="return boardCheck()">
		<input type="reset" value="다시작성">
	 
	</form>
</div>
</body>
</html>
