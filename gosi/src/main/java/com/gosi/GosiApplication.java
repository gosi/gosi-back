package com.gosi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class GosiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GosiApplication.class, args);
	}

}
