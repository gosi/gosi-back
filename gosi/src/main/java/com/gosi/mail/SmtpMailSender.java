package com.gosi.mail;

import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;

@Component
public class SmtpMailSender {
	@Resource
	SmtpClientSupporter smtpSupporter;

	public void sendEmailMessage(String sendId, String sendName, String recMailAddr, String recName, String title,
			String contents) {
		try {
			String[] recMailAddrs = recMailAddr.toString().split(";");
			String[] recNames = recName.toString().split(";");
			if (recMailAddrs.length > 0) {
				SmtpClient client = this.smtpSupporter.getSmtpClient();
				client.sendMail(sendId, sendName, recMailAddrs, recNames, title, contents);
			}
		} catch (Exception var10) {
			var10.printStackTrace();
		}

	}

	public void sendEmailMessage(String recMailAddr, String recName, String title, String contents) {
		try {
			String[] recMailAddrs = recMailAddr.toString().split(";");
			String[] recNames = recName.toString().split(";");
			if (recMailAddrs.length > 0) {
				SmtpClient client = this.smtpSupporter.getSmtpClient();
				client.sendMail(recMailAddrs, recNames, title, contents);
			}
		} catch (Exception var8) {
			var8.printStackTrace();
		}

	}

	public void sendEmailMessage(String recMailAddr, String recName, String title, Map<String, Object> map) {
		try {
			String[] recMailAddrs = recMailAddr.toString().split(";");
			String[] recNames = recName.toString().split(";");
			String contents = "";
			if (recMailAddrs.length > 0) {
				SmtpClient client = this.smtpSupporter.getSmtpClient();

				if(map.get("type").equals("join")) 
					contents = getJoinContent(map, recMailAddr);
				else if(map.get("type").equals("order")) 
					contents = getOrderContent(map, recMailAddr);
				else if(map.get("type").equals("delivery")) 
					contents  = getDeliveryContent(map);
				else if(map.get("type").equals("delete"))
					contents  = getDeleteContent(map, recMailAddr);
				client.sendMail(recMailAddrs, recNames, title, contents);
			}
		} catch (Exception var12) {
			var12.printStackTrace();
		}

	}

	public String getJoinContent(Map<String, Object> map, String recMailAddr) {
		String contents = "";
		contents = "<h1>가입인증</h1>"+
		"안녕하세요. "+map.get("name")+"님.<br/>"+
		"고시계사에 새로운 회원이 되신 것을 축하드립니다.<br/><br/><br/>"+
		"<h3>회원가입정보</h3>"+
		"<table align = \"center\"><td bgcolor=\"#CCEEFF\">이메일</td><td>"+recMailAddr+"</td>"+
		"<td bgcolor=\"#CCEEFF\">이름</td><td>"+map.get("name")+"</td>"+
		"</table>"
				;
		return contents;
	}
	
	public String getOrderContent(Map<String, Object> map, String recMailAddr) {
		String contents = "";
		contents = "<h1>주문확인서</h1>"+
				"안녕하세요. "+map.get("name")+"님.<br/>"+
				"귀하의 구매 내역입니다.<br/><br/><br/>"+
				"<h3>구매정보정보</h3>"+
				"<table align = \"center\"><td bgcolor=\"#CCEEFF\">이릅</td><td>"+map.get("name")+"</td>"+
				"<td bgcolor=\"#CCEEFF\">이메일</td><td>"+recMailAddr+"</td>"+
				"<td bgcolor=\"#CCEEFF\">전화번호</td><td>"+map.get("tel")+"</td>"+
				"<td bgcolor=\"#CCEEFF\">결제수단</td><td>"+map.get("payType")+"</td>"+
				"<td bgcolor=\"#CCEEFF\">구매금액</td><td>"+map.get("price")+"</td>"+
				"<td bgcolor=\"#CCEEFF\">배달비</td><td>"+map.get("deliveryFee")+"</td>"+
				"<td bgcolor=\"#CCEEFF\">총금액</td><td>"+map.get("totalPrice")+"</td>"+
				"</table></br>"+
				"<h3>구매정보정보</h3>";
		
		String str = "<table align = \"center\"><tr bgcolor=\"#CCEEFF\">책</tr><tr bgcolor=\"#CCEEFF\">부수</tr><tr bgcolor=\"#CCEEFF\">가격</tr>";
		List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("orderList");
		for(int i=0; i<list.size(); i++) {
			Map<String, Object> orderInfo = list.get(i);
			str = str.concat("<td>").concat((String)orderInfo.get("title")).concat("</td>");
			str = str.concat("<td>").concat(String.valueOf(orderInfo.get("count"))).concat("</td>");
			str = str.concat("<td>").concat(String.valueOf(orderInfo.get("salePrice"))).concat("</td>");
		}
		str = str.concat("</table>");
		
		contents = contents.concat(str);
		
		return contents;
	}
	
	public String getDeliveryContent(Map<String, Object> map) {
		String contents = "";
		contents = "<h1>" + map.get("name") + "</h1>님 그동안 고시계사를 이용해주셔서 감사드립니다.<br/>"
				;
		return contents;
	}
	
	public String getDeleteContent(Map<String, Object> map, String recMailAddr) {
		String contents = "";
		contents = "<h1>회원탈퇴</h1>"+
				"안녕하세요. "+map.get("name")+"님.<br/>"+
				"그동안 고시계사를 이용해주셔서 감사드립니다.<br/><br/><br/>"+
				"<h3>탈퇴회원정보</h3>"+
				"<table align = \"center\"><td bgcolor=\"#CCEEFF\">이메일</td><td>"+recMailAddr+"</td>"+
				"<td bgcolor=\"#CCEEFF\">이름</td><td>"+map.get("name")+"</td>"+
				"</table>"
						;
		return contents;
	}

	public String getContentMore(Map<String, Object> map) {
		String contents = "";
		contents = "<br/><br/>-------------------------------------------------------------------------------------------------<br/><br/><h2>"
				+ map.get("serviceName") + "</h2>category 수  :  " + map.get("cateCnt") + "<br/>bestItem 수  :  "
				+ map.get("bestItemCnt") + "<br/>eventItem 수  :  " + map.get("eventItemCnt") + "<br/>newItem 수  :  "
				+ map.get("newItemCnt") + "<br/>normalItem 수  :  " + map.get("normalItemCnt") + "<br/>item 수  :  "
				+ map.get("itemCnt") + "<br/>stock 수  :  " + map.get("stockCnt")
				+ "<br/><br/><table border=1 style=\"border-collapse:collapse;\" cellspacing=\"0\" cellsadding=\"15\" height=\"40%\"><tr><td bgcolor=\"#CCEEFF\">cate1 수집 갯수         </td><td width=\"10%\"> "
				+ map.get("cate1ColRPtrn") + "</td><td>cate1 실패 갯수           </td><td width=\"10%\">"
				+ map.get("cate1ColFPtrn")
				+ "</td></tr><tr><td bgcolor=\"#CCEEFF\">cate2 수집 갯수         </td><td width=\"10%\">  "
				+ map.get("cate2ColRPtrn") + "</td><td>cate2 실패 갯수           </td><td width=\"10%\">"
				+ map.get("cate2ColFPtrn")
				+ "</td></tr><tr><td bgcolor=\"#CCEEFF\">cate3 수집 갯수         </td><td width=\"10%\">  "
				+ map.get("cate3ColRPtrn") + "</td><td>cate3 실패 갯수           </td><td width=\"10%\">"
				+ map.get("cate3ColFPtrn")
				+ "</td></tr><tr><td bgcolor=\"#CCEEFF\">cate4 수집 갯수         </td><td width=\"10%\">  "
				+ map.get("cate4ColRPtrn") + "</td><td>cate4 실패 갯수           </td><td width=\"10%\">"
				+ map.get("cate4ColFPtrn")
				+ "</td></tr><tr><td bgcolor=\"#CCEEFF\">cate5 수집 갯수         </td><td width=\"10%\">  "
				+ map.get("cate5ColRPtrn") + "</td><td>cate5 실패 갯수           </td><td width=\"10%\">"
				+ map.get("cate5ColFPtrn")
				+ "</td></tr><tr><td bgcolor=\"#CCEEFF\">image 수집 갯수        </td><td width=\"10%\">  "
				+ map.get("imgColRPtrn") + "</td><td>imgage 실패 갯수         </td><td width=\"10%\">"
				+ map.get("imgColFPtrn")
				+ "</td></tr><tr><td bgcolor=\"#CCEEFF\">review 수집 갯수       </td><td width=\"10%\">  "
				+ map.get("reviewColRPtrn") + "</td><td>review 실패 갯수         </td><td width=\"10%\">"
				+ map.get("reviewColFPtrn")
				+ "</td></tr><tr><td bgcolor=\"#CCEEFF\">stock 수집 갯수         </td><td width=\"10%\">  "
				+ map.get("stockColRPtrn") + "</td><td>stock 실패 갯수           </td><td width=\"10%\">"
				+ map.get("stockColFPtrn")
				+ "</td></tr><tr><td bgcolor=\"#CCEEFF\">goodsName 수집 갯수  </td><td width=\"10%\">  "
				+ map.get("goodsNameColRPtrn") + "</td><td>goodsName 실패 갯수    </td><td width=\"10%\">"
				+ map.get("goodsNameColFPtrn")
				+ "</td></tr><tr><td bgcolor=\"#CCEEFF\">sellAmt 수집 갯수     </td><td width=\"10%\">  "
				+ map.get("sellAmtColRPtrn") + "</td><td>sellAmt 실패 갯수       </td><td width=\"10%\">"
				+ map.get("sellAmtColFPtrn") + "</td></tr></table>";
		return contents;
	}

	public void sendEmailMessage(String message) {
		this.sendEmailMessage();
	}

	private void sendEmailMessage() {
	}
}