package com.gosi.mail;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import org.springframework.stereotype.Component;

@Component
public class SmtpClient {
	private String HOST = "smtp.gmail.com";
	private String USER_ID ="jungdu92@gmail.com";
	private String USER_PW = "wjdekdns3190";
	private String PORT = "465";
	private String USER_NM_KR = "정다운";
	private String USER_NM_ENG = "JungDaUn";
	private boolean isDebug = true;
	private boolean isStartTLS = true;

	public void init() {
	}

	public SmtpClient() {
	}

	public SmtpClient(String serverUrl, String port, boolean isDebug, boolean isSSL) {
		this.HOST = serverUrl;
		this.PORT = port.trim();
		this.isDebug = isDebug;
		this.isStartTLS = isSSL;
	}

	public SmtpClient(String serverUrl, String port, String id, String pw, boolean isDebug, boolean isSSL) {
		this.HOST = serverUrl;
		this.PORT = port.trim();
		this.USER_ID = id;
		this.USER_PW = pw;
		this.isDebug = isDebug;
		this.isStartTLS = isSSL;
	}

	public void setIsDebug(boolean isDebug) {
		this.isDebug = isDebug;
	}

	public void setServerInfo(String serverUrl, String id, String pw) {
		this.HOST = serverUrl;
		this.USER_ID = id;
		this.USER_PW = pw;
	}

	public String sendMail(String sender, String sendername, String[] recipient, String[] recipientname, String subject,
			String contents) throws Exception {
		return this.sendMail(sender, sendername, recipient, recipientname, subject, contents, (String[]) null,
				(String[]) null, (String[]) null);
	}

	public String sendMail(String sender, String sendername, String[] recipient, String[] recipientname, String subject,
			String contents, String[] cc, String[] ccname) throws Exception {
		return this.sendMail(sender, sendername, recipient, recipientname, subject, contents, cc, ccname,
				(String[]) null);
	}

	public String sendMail(String sender, String sendername, String[] recipient, String[] recipientname, String subject,
			String contents, String[] cc, String[] ccname, String[] bcc, String[] bccname) throws Exception {
		return this.sendMail(sender, sendername, recipient, recipientname, subject, contents, cc, ccname, bcc, bccname,
				(String[]) null);
	}

	public String sendMail(String sender, String sendername, String[] recipient, String[] recipientname, String subject,
			String contents, String[] cc, String[] ccname, String[] filename) throws Exception {
		String strReturn = "";
		if (sender == null || recipient == null) {
			System.out.println("usage : java <from> <to>");
		}

		try {
			Session session = this.getSession();
			Message message = new MimeMessage(session);
			this.setFrom(message, sender, sendername);
			this.setTo(message, recipient, recipientname);
			this.setCC(message, cc, ccname);
			this.setSubject(message, subject);
			this.setBodyPart(message, contents, filename);
			this.setSendDate(message);
			Transport.send(message);
			return strReturn;
		} catch (Exception var13) {
			strReturn = var13.toString();
			var13.printStackTrace();
			throw new Exception(var13.getMessage());
		}
	}

	public String sendMail(String sender, String sendername, String[] recipient, String[] recipientname, String subject,
			String contents, String[] cc, String[] ccname, String[] bcc, String[] bccname, String[] filename)
			throws Exception {
		String strReturn = "";
		if (sender == null || recipient == null) {
			System.out.println("usage : java <from> <to>");
		}

		try {
			Session session = this.getSession();
			Message message = new MimeMessage(session);
			this.setFrom(message, sender, sendername);
			this.setTo(message, recipient, recipientname);
			this.setCC(message, cc, ccname);
			this.setBlindCC(message, bcc, bccname);
			this.setSubject(message, subject);
			this.setBodyPart(message, contents, filename);
			this.setSendDate(message);
			Transport.send(message);
			return strReturn;
		} catch (Exception var15) {
			strReturn = var15.toString();
			var15.printStackTrace();
			throw new Exception(var15.getMessage());
		}
	}

	private Session getSession() {
		if (this.HOST != null && this.USER_ID != null && this.USER_PW != null) {
			try {
				Properties props = new Properties();
				props.put("mail.smtp.host", this.HOST);
				String auth = "true";
				if (this.USER_ID == null || "".equals(this.USER_ID)) {
					auth = "false";
				}

				props.put("mail.smtp.auth", auth);
				props.put("mail.smtp.port", this.PORT);
				props.put("mail.debug", this.isDebug ? "true" : "false");
				if (this.isStartTLS) {
					props.put("mail.smtp.starttls.enable", String.valueOf(this.isStartTLS));
					props.put("mail.smtp.socketFactory.port", this.PORT);
					props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
					props.put("mail.smtp.socketFactory.fallback", "false");
				}

				SmtpAuthenticator uauth = new SmtpAuthenticator(this.USER_ID, this.USER_PW);
				Session session = Session.getInstance(props, uauth);
				return session;
			} catch (Exception var5) {
				System.out.println("SMTPServer.getSession()::Exception::" + this.USER_ID + ":" + this.USER_PW);
				var5.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

	private void setFrom(Message message, String sender, String sendername) throws Exception {
		try {
			message.setFrom(new InternetAddress(sender, sendername, "UTF-8"));
		} catch (UnsupportedEncodingException var5) {
			message.setFrom(new InternetAddress(sendername + "<" + sender + ">"));
		}

	}

	private void setTo(Message message, String[] recipient, String[] recipientname) throws Exception {
		try {
			for (int i = 0; i < recipient.length; ++i) {
				message.addRecipient(RecipientType.TO, new InternetAddress(recipient[i], recipientname[i], "UTF-8"));
			}
		} catch (UnsupportedEncodingException var5) {
			message.addRecipient(RecipientType.TO, new InternetAddress(recipientname + "<" + recipient + ">"));
		}

	}

	private void setCC(Message message, String[] cc, String[] ccname) throws Exception {
		if (cc != null) {
			for (int i = 0; i < cc.length; ++i) {
				try {
					message.addRecipient(RecipientType.CC, new InternetAddress(cc[i], ccname[i], "UTF-8"));
				} catch (UnsupportedEncodingException var6) {
					message.addRecipient(RecipientType.CC, new InternetAddress(ccname[i] + "<" + cc[i] + ">"));
				}
			}
		}

	}

	private void setBlindCC(Message message, String[] blindCc, String[] blindCcname) throws Exception {
		if (blindCc != null) {
			for (int i = 0; i < blindCc.length; ++i) {
				try {
					message.addRecipient(RecipientType.BCC, new InternetAddress(blindCc[i], blindCcname[i], "UTF-8"));
				} catch (UnsupportedEncodingException var6) {
					message.addRecipient(RecipientType.BCC,
							new InternetAddress(blindCcname[i] + "<" + blindCc[i] + ">"));
				}
			}
		}

	}

	private void setSubject(Message message, String subject) throws Exception {
		message.setSubject(subject);
	}

	private void setSendDate(Message message) throws Exception {
		message.setSentDate(new Date());
	}

	private void setBodyPart(Message message, String contents, String[] filename) throws Exception {
		Multipart multipart = new MimeMultipart();
		message.addHeader("IMT", "multi");
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(contents, "text/html; charset=UTF-8");
		messageBodyPart.setDisposition("inline");
		multipart.addBodyPart(messageBodyPart);
		this.addMultiPartContent(multipart, filename);
		message.setContent(multipart);
	}

	private void addMultiPartContent(Multipart multipart, String[] filename) throws Exception {
		if (filename != null && filename.length > 0) {
			MimeBodyPart messageBodyPart = new MimeBodyPart();

			for (int i = 0; i < filename.length; ++i) {
				if (!filename[i].equals("")) {
					DataSource fds = new FileDataSource(filename[i]);
					messageBodyPart.setDataHandler(new DataHandler(fds));
					messageBodyPart.setFileName(MimeUtility.encodeText(fds.getName(), "UTF-8", "Q"));
					messageBodyPart.setDisposition("attachment");
					multipart.addBodyPart(messageBodyPart);
				}
			}

		}
	}

	public String sendMail(String[] recipient, String[] recipientname, String subject, String contents)
			throws Exception {
		return this.sendMail(this.USER_NM_ENG, this.USER_NM_KR, recipient, recipientname, subject, contents,
				(String[]) null, (String[]) null, (String[]) null);
	}
}