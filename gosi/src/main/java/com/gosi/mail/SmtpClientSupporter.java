package com.gosi.mail;

import javax.annotation.Resource;
import org.springframework.stereotype.Component;

@Component
public class SmtpClientSupporter {
	@Resource
	private SmtpClient client;

	public void setSmtpClient(SmtpClient client) {
		this.client = client;
	}

	public SmtpClient getSmtpClient() throws Exception {
		return this.client;
	}
}