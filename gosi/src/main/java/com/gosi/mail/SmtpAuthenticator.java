package com.gosi.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class SmtpAuthenticator extends Authenticator {
	PasswordAuthentication pa;

	public SmtpAuthenticator(String id, String password) {
		this.pa = new PasswordAuthentication(id, password);
	}

	public PasswordAuthentication getPasswordAuthentication() {
		return this.pa;
	}
}