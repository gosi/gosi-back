package com.gosi.config.dbconfig;

import javax.sql.DataSource;
import net.sf.log4jdbc.Log4jdbcProxyDataSource;
import net.sf.log4jdbc.tools.Log4JdbcCustomFormatter;
import net.sf.log4jdbc.tools.LoggingType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@MapperScan(value = {"com.gosi.db.dao"}, sqlSessionFactoryRef = "sessionFactory")
@EnableTransactionManagement
public class DBConfig {
	@Value("maria")
	private String platform;

	@Bean(name = {"dataSource"}, destroyMethod = "close")
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource.tomcat")
	public DataSource db1DataSource() {
		DataSource db = DataSourceBuilder.create().build();
		return db;
	}

	@Bean(name = {"sessionFactory"})
	@Primary
	public SqlSessionFactory sessionFactory(@Qualifier("dataSource") DataSource db1DataSource,
			ApplicationContext applicationContext) throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(convertLog4jdbc(db1DataSource));
		sqlSessionFactoryBean.setConfigLocation(
				applicationContext.getResource("classpath:/config/mybatis/gosi/" + this.platform + ".xml"));
		return sqlSessionFactoryBean.getObject();
	}

	@Bean(name = {"sqlSession"})
	@Primary
	public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory db1SqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(db1SqlSessionFactory);
	}

	@Bean(name = {"transactionManager"})
	public PlatformTransactionManager transactionManager(@Qualifier("dataSource") DataSource dataSource1) {
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource1);
		transactionManager.setGlobalRollbackOnParticipationFailure(false);
		return transactionManager;
	}

	static DataSource convertLog4jdbc(DataSource dataSource) {
		boolean log4jdbc = Boolean.parseBoolean(System.getProperty("log4jdbc.use", "false"));
		if (log4jdbc) {
			int threshold = Integer.parseInt(System.getProperty("log4jdbc.threshold", "500"));
			System.setProperty("log4jdbc.sqltiming.warn.threshold", String.valueOf(threshold));
			Log4jdbcProxyDataSource logDataSoruce = new Log4jdbcProxyDataSource(dataSource);
			Log4JdbcCustomFormatter formatter = new Log4JdbcCustomFormatter();
			formatter.setLoggingType(LoggingType.MULTI_LINE);
			formatter.setSqlPrefix("SQL=> \n");
			logDataSoruce.setLogFormatter(formatter);
			return logDataSoruce;
		} else {
			return dataSource;
		}
	}
}