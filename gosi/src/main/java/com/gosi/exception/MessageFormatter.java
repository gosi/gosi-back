package com.gosi.exception;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageFormatter {
	public static String format(String data, String... args) {
		Pattern pattern = Pattern.compile("\\{\\}");
		Matcher matcher = pattern.matcher(data);
		StringBuilder builder = new StringBuilder();
		int index = 0;

		int i;
		for (i = 0; matcher.find(); ++index) {
			String replacement = null;
			if (index < args.length) {
				replacement = args[index];
			}

			builder.append(data.substring(i, matcher.start()));
			if (replacement == null) {
				builder.append(matcher.group(0));
			} else {
				builder.append(replacement);
			}

			i = matcher.end();
		}

		builder.append(data.substring(i, data.length()));
		return builder.toString();
	}
}