package com.gosi.exception;

public class MessageCode {
	
	public static final short SUCCESS = 0;
	public static final short JSON_PARSING_ERROR = 1;
}