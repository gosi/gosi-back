package com.gosi.exception;

public class PException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private String errorNumer = "";
	private String errorMsg = "";

	protected PException(String message) {
		super(message);
	}

	protected PException(Throwable cause) {
		super(cause);
	}

	protected PException(String errorNumber, String message, Throwable cause) {
		super(message, cause);
		this.errorNumer = errorNumber;
		this.errorMsg = message;
	}

	protected PException(String errorNumber, String message) {
		super(message);
		this.errorNumer = errorNumber;
		this.errorMsg = message;
	}

	protected PException(String message, Throwable cause) {
		super(message, cause);
	}

	protected PException(short errorCode, Throwable cause) {
		super(cause);
		this.errorNumer = String.valueOf(errorCode);
	}

	public String getMessage() {
		if (this.errorNumer == null) {
			return this.errorNumer;
		} else {
			StringBuffer sb = new StringBuffer();
			if (!this.errorNumer.equals("")) {
				sb.append("[");
				sb.append(this.errorNumer);
				sb.append("] ");
				sb.append(this.errorMsg);
			} else {
				sb.append(this.errorMsg);
			}

			return sb.toString();
		}
	}

	public short getErrorNumber() {
		return this.errorNumer.equals("") ? -1 : Short.parseShort(this.errorNumer);
	}

	public static PException buildException(String message) {
		return new PException(message);
	}

	public static PException buildException(String message, Throwable cause) {
		return new PException(message, cause);
	}

	public static PException buildException(String errorNumber, String message, Throwable cause) {
		return new PException(errorNumber, message, cause);
	}

	public static PException buildException(short errorNumber, String message, Throwable cause) {
		return new PException(String.valueOf(errorNumber), message, cause);
	}

	public static PException buildException(String errorNumber, Throwable cause, String message, String... args) {
		String errorMessage = MessageFormatter.format(message, args);
		return new PException(errorNumber, errorMessage, cause);
	}

	public static PException buildException(short errorNumber, Throwable cause, String message, String... args) {
		String errorMessage = MessageFormatter.format(message, args);
		return new PException(String.valueOf(errorNumber), errorMessage, cause);
	}

	public static PException buildException(short errorNumber, Throwable cause) {
		return new PException(errorNumber, cause);
	}

	public static PException buildException(String errorNumber, String message) {
		return new PException(errorNumber, message);
	}

	public static PException buildException(short errorNumber, String message) {
		return new PException(String.valueOf(errorNumber), message);
	}

	public static PException buildException(short errorNumber, String message, Object args) {
		String errorMessage = MessageFormatter.format(message, new String[]{String.valueOf(args)});
		return new PException(String.valueOf(errorNumber), errorMessage);
	}

	public static PException buildException(String errorNumber, String message, Object args) {
		String errorMessage = MessageFormatter.format(message, new String[]{String.valueOf(args)});
		return new PException(errorNumber, errorMessage);
	}

	public static PException buildException(short errorNumber, String message, String... args) {
		String errorMessage = MessageFormatter.format(message, args);
		return new PException(String.valueOf(errorNumber), errorMessage);
	}

	public static PException buildException(String errorNumber, String message, String... args) {
		String errorMessage = MessageFormatter.format(message, args);
		return new PException(errorNumber, errorMessage);
	}

	public static PException buildException(String message, String... args) {
		String errorMessage = MessageFormatter.format(message, args);
		return new PException(errorMessage);
	}
}