package com.gosi.dailyPro;

import org.springframework.stereotype.Component;

import com.gosi.db.DbHandler;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Component
public class DailyProject {
	
	private Timer timer;

	@Resource
	DbHandler dbHandler;
	
	@PostConstruct
	public void afterInit()
	{
		Calendar date = Calendar.getInstance();
//		date.set(Calendar.AM_PM, Calendar.AM);
//		date.set(Calendar.HOUR, 10);
//		date.set(Calendar.MINUTE, 25);
//		date.set(Calendar.SECOND, 0);
//		date.set(Calendar.MILLISECOND, 0);
		
//		Long period = (long) (60*60*1000);
		
		date.set(Calendar.AM_PM, Calendar.AM);
		date.set(Calendar.HOUR, 1);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		
		Long period = (long) (24*60*60*1000);
			
		timer = new Timer();
		MonitorTask task = new MonitorTask();
		timer.scheduleAtFixedRate(task, date.getTime(), period);
	}
	
	class MonitorTask extends TimerTask {
	
		public MonitorTask()
		{
			
		}

		@Override
		public void run() {
			
			//2일로 테스트 해보기
			//6개월 전의 데이터는 SEE_YN = 'N'로 변경
			List<Map<String, Object>> list1 = dbHandler.getMonitorList();
			for(int i=0; i<list1.size(); i++) {
				Map<String, Object> map = list1.get(i);
				String jNo = (String)map.get("jNo");
				dbHandler.updateMonitorList(jNo);
			}
			
			//1일로 테스트 해보기
			//입금이 3일 이상 이루어 지지 않은 부분에 대해서 주문 취소 처리하기
			List<Map<String, Object>> list2 = dbHandler.getMonitorList2();
			for(int i=0; i<list2.size(); i++) {
				Map<String, Object> map = list2.get(i);
				String jNo = (String)map.get("jNo");
				dbHandler.updateMonitorList2(jNo);
			}
			
			//2일로 테스트 해보기
			//배송시작 후 2주동안 배송 완료가 되지 않은 상품에 대해서 자동 배송완료 처리 해준다.
			List<Map<String, Object>> list3 = dbHandler.getMonitorList3();
			for(int i=0; i<list3.size(); i++) {
				Map<String, Object> map = list3.get(i);
				String jNo = (String)map.get("jNo");
				dbHandler.updateMonitorList3(jNo);
			}
			
			//이건 직접 테스트 해보고 봐야함
			//정기결제를 살펴보고 보내줘야 하는 경우 -> 배송목록에 자동적으로 넣어준다.
			List<Map<String, Object>> list4 = dbHandler.getMonitorList4();
			for(int i=0; i<list4.size(); i++) {
				Map<String, Object> map = list4.get(i);
				String jNo = (String)map.get("jNo");
				int curCnt = (Integer)map.get("curCnt");
				String deliDate = (String)map.get("deliDate");
				dbHandler.updateMonitorList4(jNo, curCnt, deliDate); //WEB은 안되는 SQL
			}
			
		}
		
	}
}
