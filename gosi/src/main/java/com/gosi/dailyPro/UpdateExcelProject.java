package com.gosi.dailyPro;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import com.gosi.db.DbHandler;

@Component
public class UpdateExcelProject {
	
	/*
	private Timer timer;

	@Resource
	DbHandler dbHandler;
	
	@PostConstruct
	public void afterInit()
	{
		Calendar date = Calendar.getInstance();
		date.set(Calendar.AM_PM, Calendar.PM);
		date.set(Calendar.HOUR, 07);
		date.set(Calendar.MINUTE, 48);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		
		Long period = (long) (24*60*60*1000);
		
		//저녁 10시
		//date.set(Calendar.AM_PM, Calendar.PM);
		//date.set(Calendar.HOUR, 10);
		//date.set(Calendar.MINUTE, 0);
		//date.set(Calendar.SECOND, 0);
		//date.set(Calendar.MILLISECOND, 0);
		
		//Long period = (long) (24*60*60*1000);
			
		timer = new Timer();
		MonitorTask task = new MonitorTask();
		timer.scheduleAtFixedRate(task, date.getTime(), period);
	}
	
	class MonitorTask extends TimerTask {
	
		public MonitorTask()
		{
			
		}

		@Override
		public void run() {
			
			try {
				//FileInputStream file = new FileInputStream("tomcat/webapps/file_1003/upload.xlsx");
	            FileInputStream file = new FileInputStream("C:\\Users\\jung\\test.xlsx");
	            XSSFWorkbook workbook = new XSSFWorkbook(file);
	            //입금완료update
	            Map<String, Object> map = new HashMap<>();
	            //주문취소
	            Map<String, Object> map2 = new HashMap<>();
	            //배송중, 송장번호 함께
	            Map<String, Object> map3 = new HashMap<>();
	            //배송완료
	            Map<String, Object> map4 = new HashMap<>();
	 
	            int rowindex=0;
	            int columnindex=0;
	            //시트 수 (첫번째에만 존재하므로 0을 준다)
	            //만약 각 시트를 읽기위해서는 FOR문을 한번더 돌려준다
	            XSSFSheet sheet=workbook.getSheetAt(0);
	            
	            int rows=sheet.getPhysicalNumberOfRows();
	            for(rowindex=1;rowindex<rows;rowindex++){
	                
	                XSSFRow row=sheet.getRow(rowindex);
	                if(row !=null){
	                    int cells=4;
	                    String jNo = "", deliNum = "";
	                    for(columnindex=0; columnindex<=cells; columnindex++){
	                        
	                        XSSFCell cell=row.getCell(columnindex);
	                        String value="";
	                        
	                        if(cell==null){
	                            continue;
	                        }else{
	                            switch (cell.getCellType()){
	                            case XSSFCell.CELL_TYPE_FORMULA:
	                                value=cell.getCellFormula();
	                                break;
	                            case XSSFCell.CELL_TYPE_NUMERIC:
	                                value=cell.getNumericCellValue()+"";
	                                break;
	                            case XSSFCell.CELL_TYPE_STRING:
	                                value=cell.getStringCellValue()+"";
	                                break;
	                            case XSSFCell.CELL_TYPE_BLANK:
	                                value=cell.getBooleanCellValue()+"";
	                                break;
	                            case XSSFCell.CELL_TYPE_ERROR:
	                                value=cell.getErrorCellValue()+"";
	                                break;
	                            }
	                        }
	                        
	                        if(columnindex==0) //주문번호
	                        	jNo = value;
	                        else if(columnindex==1) {//주문상태
	                        	if(value.equals("입금완료")) 
	                        		map.put("jNo", jNo);
	                        	else if(value.equals("주문취소")) 
	                        		map2.put("jNo", jNo);
	                        }
	                        else if(columnindex==2) {//송장번호
	                        	deliNum = value;
	                        }
	                        else {//배송상태
	                        	if(value.equals("배송완료")) 
	                        		map4.put("jNo", jNo);
	                        	else if(value.equals("배송중")) {
	                        		map3.put("jNo", jNo);
	                        		map3.put("deliNum", deliNum);
	                        	}
	                        }
	                    }
	 
	                }
	            }
	            
	            if(map.size()>0) dbHandler.updateDeli1(map);
	            if(map2.size()>0) dbHandler.updateDeli2(map);
	            if(map3.size()>0) dbHandler.updateDeli3(map);
	            if(map4.size()>0) dbHandler.updateDeli4(map);
	 
	        }catch(Exception e) {
	            e.printStackTrace();
	        }
		

		}
		
	

	}
*/
}
