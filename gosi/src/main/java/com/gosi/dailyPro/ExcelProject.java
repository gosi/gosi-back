package com.gosi.dailyPro;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import org.springframework.stereotype.Component;

import com.gosi.db.DbHandler;

@Component
public class ExcelProject {
	/*
	
	private Timer timer;

	@Resource
	DbHandler dbHandler;
	
	@PostConstruct
	public void afterInit()
	{
		Calendar date = Calendar.getInstance();
		date.set(Calendar.AM_PM, Calendar.AM);
		date.set(Calendar.HOUR, 00);
		date.set(Calendar.MINUTE, 9);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		
		Long period = (long) (60*60*1000);
		
		//아침 6시, 오후 2시, 저녁 10시
		//date.set(Calendar.AM_PM, Calendar.AM);
		//date.set(Calendar.HOUR, 6);
		//date.set(Calendar.MINUTE, 0);
		//date.set(Calendar.SECOND, 0);
		//date.set(Calendar.MILLISECOND, 0);
		
		//Long period = (long) (8*60*60*1000);
			
		timer = new Timer();
		MonitorTask task = new MonitorTask();
		timer.scheduleAtFixedRate(task, date.getTime(), period);
	}
	
	class MonitorTask extends TimerTask {
	
		public MonitorTask()
		{
			
		}

		@Override
		public void run() {
			
			//결제확인서 엑셀 생설하기
			Map<String, Object> param = new HashMap<>();
			
			//start  - 30일전, end - 오늘
			Date dt = new Date();
			Calendar cal = new GregorianCalendar(Locale.KOREA);
			cal.setTime(dt);
			SimpleDateFormat newFromat = new SimpleDateFormat("yyyy-MM-dd");
			String endDt = newFromat.format(cal.getTime());	
			cal.add(Calendar.DATE, -30);
			String startDt = newFromat.format(cal.getTime());
			
			param.put("status", "전체");
			param.put("payStatus", "전체");
			//param.put("startDt", startDt);
			//param.put("endDt", endDt);
			param.put("startDt", "2019-02-01");
			param.put("endDt", "2019-09-01");
			
			//도서주문목록
			List<Map<String,Object>> list = dbHandler.getTypeOneList(param);
			//잡지주문목록
			List<Map<String,Object>> list2 = dbHandler.getTypeTwoList(param);
			//웹으로보기 주문목록
			List<Map<String,Object>> list3 = dbHandler.getTypeThreeList(param);
			//정기구독 주문목록
			List<Map<String,Object>> list4 = dbHandler.getTypeFourList(param);
		
			//이걸로 excel을 만들면 됨
			String version = "xls";
			// var version = "xlsx";
			// Workbook 생성.
			Workbook workbook = createWorkbook(version);
			// Workbook안에 시트 생성. 오늘 날짜 생성
			Sheet sheet = workbook.createSheet(endDt);
			
			// 셀에 데이터 작성
			int count = 2;
			Cell cell = getCell(sheet, 0, 0);
			cell.setCellValue("주문 번호");
			cell = getCell(sheet, 0, 1);
			cell.setCellValue("주문 날짜");
			cell = getCell(sheet, 0, 2);
			cell.setCellValue("주문자");
			cell = getCell(sheet, 0, 3);
			cell.setCellValue("결제 방식");
			cell = getCell(sheet, 0, 4);
			cell.setCellValue("결제 상태");
			cell = getCell(sheet, 0, 5);
			cell.setCellValue("배송 상태");
			
			cell = getCell(sheet, 0, 6);
			cell.setCellValue("배송자 이름");
			cell = getCell(sheet, 0, 7);
			cell.setCellValue("배송 주소");
			cell = getCell(sheet, 0, 8);
			cell.setCellValue("배송 상세주소");
			cell = getCell(sheet, 0, 9);
			cell.setCellValue("배송 우편번호");
			cell = getCell(sheet, 0, 10);
			cell.setCellValue("배송자 핸드폰번호");
			cell = getCell(sheet, 0, 11);
			cell.setCellValue("배송 메모");
			
			cell = getCell(sheet, 0, 12);
			cell.setCellValue("책 제목"); 
			cell = getCell(sheet, 0, 13);
			cell.setCellValue("년도");
			cell = getCell(sheet, 0, 14);
			cell.setCellValue("월");
			cell = getCell(sheet, 0, 15);
			cell.setCellValue("권수");
			cell = getCell(sheet, 0, 16);
			cell.setCellValue("총 금액");
			
			count++;
			cell = getCell(sheet, count, 0);
			cell.setCellValue("책목록");
			count++;
			for(int i=0; i<list.size(); i++) {
				Map<String, Object> map = list.get(i);
				
				int cnt = 0;
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("jNo"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("orderDt"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("name"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("payType"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("payStatus"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliveryStatus"));
				
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliName"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliAddr1"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliAddr2"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliZipCd"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliTel"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliMemo"));
				
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("title"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("");
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("");
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue(String.valueOf((Integer)map.get("count")));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue(String.valueOf((Integer)map.get("totalPrice")));
				count++;
			}
			
			count++;
			count++;
			cell = getCell(sheet, count, 0);
			cell.setCellValue("잡지목록");
			count++;
			for(int i=0; i<list2.size(); i++) {
				Map<String, Object> map = list2.get(i);
				
				int cnt = 0;
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("jNo"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("orderDt"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("name"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("payType"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("payStatus"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliveryStatus"));
				
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliName"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliAddr1"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliAddr2"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliZipCd"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliTel"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliMemo"));
				
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("title"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("year"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("month"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue(String.valueOf((Integer)map.get("count")));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue(String.valueOf((Integer)map.get("totalPrice")));
				count++;
			}
			
			count++;
			count++;
			cell = getCell(sheet, count, 0);
			cell.setCellValue("웹목록");
			count++;
			for(int i=0; i<list3.size(); i++) {
				Map<String, Object> map = list3.get(i);
				
				int cnt = 0;
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("jNo"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("orderDt"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("name"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("payType"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("payStatus"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("배송불필요");
				
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("");
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("");
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("");
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("");
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("");
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("");
				
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("title"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("year"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("month"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("1");
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue(String.valueOf((Integer)map.get("totalPrice")));
				count++;
			}
			
			count++;
			count++;
			cell = getCell(sheet, count, 0);
			cell.setCellValue("정기구독목록");
			count++;
			//일반적으로 주문되는 부분은 megazineList나 bookList에 기재되어야 정상
			for(int i=0; i<list4.size(); i++) {
				Map<String, Object> map = list4.get(i);
				
				int cnt = 0;
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("jNo"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("orderDt"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("name"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("payType"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("payStatus"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("배송불필요");
				
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliName"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliAddr1"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliAddr2"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliZipCd"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliTel"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("deliMemo"));
				
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue("정기구독주문");
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("startDt"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue((String)map.get("endDt"));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue(String.valueOf((Integer)map.get("count")));
				cell = getCell(sheet, count, cnt++);
				cell.setCellValue(String.valueOf((Integer)map.get("totalPrice")));
				count++;
			}
			
			// 셀에 데이터 포멧 지정
			CellStyle style = workbook.createCellStyle();
			// 날짜 포멧
			style.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));
			// 셀 색지정
			style.setFillBackgroundColor(IndexedColors.GOLD.index);
			// 폰트 설정
			Font font = workbook.createFont();
			font.setColor(IndexedColors.RED.index);
			cell.setCellStyle(style);
			//셀 너비 자동 지정
			sheet.autoSizeColumn(0);
			sheet.autoSizeColumn(1);
			sheet.autoSizeColumn(2);
	
			//writeExcel(workbook, "tomcat/webapps/file_1003/"+endDt+"." + version);
			writeExcel(workbook, "C:\\Users\\jung\\"+endDt+"." + version);

		}
		
		// Workbook 생성
		public Workbook createWorkbook(String version) {
			//표준 xls 버젼
			if ("xls".equals(version)) {
				return new HSSFWorkbook();
				//확장 xlsx 버젼
			} else if ("xlsx".equals(version)) {
			return new HSSFWorkbook();
			}
			throw new NoClassDefFoundError();
		}
		
		// Sheet로 부터 Row를 취득, 생성하기
		public Row getRow(Sheet sheet, int rownum) {
			Row row = sheet.getRow(rownum);
			if (row == null) {
			row = sheet.createRow(rownum);
			}
			return row;
		}
		
		// Row로 부터 Cell를 취득, 생성하기
		public Cell getCell(Row row, int cellnum) {
			Cell cell = row.getCell(cellnum);
			if (cell == null) {
			cell = row.createCell(cellnum);
			}
			return cell;
		}
		
		public Cell getCell(Sheet sheet, int rownum, int cellnum) {
			Row row = getRow(sheet, rownum);
			return getCell(row, cellnum);
		}
		
		public void writeExcel(Workbook workbook, String filepath) {
			try (FileOutputStream stream = new FileOutputStream(filepath)) {
			workbook.write(stream);
			} catch (Throwable e) {
			e.printStackTrace();
			}
		}


	}
*/
}
