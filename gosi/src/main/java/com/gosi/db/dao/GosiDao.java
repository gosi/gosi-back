package com.gosi.db.dao;

import com.gosi.db.dto.Book;
import com.gosi.db.dto.Cart;
import com.gosi.db.dto.Member;
import com.gosi.db.dto.NoMember;
import com.gosi.db.dto.Notice;
import com.gosi.db.dto.Order;
import com.gosi.db.dto.OrderBundle;
import com.gosi.db.dto.OrderMegazine;
import com.gosi.db.dto.StandingOrder;
import com.gosi.db.dto.WebBook;
import java.util.List;
import java.util.Map;

public interface GosiDao {
	void update(Map<String, Object> map);
	List<Map<String, Object>> getList();
	//자동 프로젝트
	List<Map<String, Object>> getMonitorList();
	void updateMonitorBundle(String jNo);
	void updateMonitorPri(String jNo);
	List<Map<String, Object>> getMonitorList2();
	List<Map<String, Object>> getMonitorList3();
	List<Map<String, Object>> getMonitorList4(String date);
	void updateMonitorBundle2(String jNo);
	void updateMonitorPri2(String jNo);
	void updateMonitorList3(String jNo);
	List<Map<String, Object>> getMonitorBundle4(String jNo);
	void updateMonitorList4(Map<String, Object> map);
	void updateMonitorListPri(Map<String, Object> map);
	void updateMonitorListBundle(Map<String, Object> map);
	void updateMonitorListCart(Map<String, Object> map);
	
	void updateDeli1(Map<String, Object> map);
	void updateDeli21(Map<String, Object> map);
	void updateDeli22(Map<String, Object> map);
	void updateDeli3(Map<String, Object> map);
	void updateDeli4(Map<String, Object> map);
	
	//common
	List<Notice> getBbsList(Map<String, Object> var1);
	List<Notice> getSubBbsList(String var1);
	List<Book> getBookList(Map<String, Object> var1);
	public Integer getPageNum(Map<String, Object> param);
	List<WebBook> getWebBookList(String var1);
	Notice getDetailInfo(int var1);
	Book getBookDetail(int var1);
	Integer getSubBbsPageNum(String var1);

	//login
	boolean checkMember(Map<String, Object> var1);
	void addInfo(Member var1);
	void updateNoMemberInfo(Map<String, Object> var1);
	boolean checkId(Map<String, Object> var1);
	Map<String, Object> getHash(String var1);
	String getOnlyHash(String var1);
	String findId(Map<String, Object> var1);
	String findPassWd(Map<String, Object> var1);
	void changePassWd(Map<String, Object> var1);
	String getNonHash(String var1);

	//cart
	List<Map<String, Object>> getCartBookList(String var1);
	List<Map<String, Object>> getCartMagazineList(String var1);
	List<Map<String, Object>> getCartWebList(String var1);
	void addMegazineCart(Cart var1);
	void addWebCart(Cart var1);
	void addCart(Cart var1);
	void deleteCart(int var1);
	void addOrderCartNo(Map<String, Object> map);
	void addOrderCartYes(Map<String, Object> map);
	boolean check(Map<String, Object> map);
	List<Map<String, Object>> getOrderCart(Map<String, Object> map);
	void deleteOrderCart(Map<String, Object> map);

	//mypage
	List<Map<String, Object>> getNonmemberOrderList(String var1);
	List<Map<String, Object>> getOrderList(Map<String, Object> var1);
	List<String> getJnoList(Map<String, Object> var1);
	List<Map<String, Object>> getWebOrderList(String id);
	void changeInfo(Member var1);
	void changePassWd2(Map<String, Object> var1);
	void deleteInfo(Map<String, Object> var1);
	boolean checkMem(Map<String, Object> var1);
	Map<String, Object> getMemberInfo(String id);
	boolean checkOrder(String jNo);
	void deleteOrder(Map<String, Object> map);
	
	//admin
	void addBook(Book var1);
	void deleteBook(int bookId);
	void changeBook(Book var1);
	void addMegazine(Book var1);
	void changeMegazine(Book var1);
	void addWebBook(WebBook var1);
	void deleteWebBook(int bookId);
	void changeWebBook(WebBook var1);
	List<Book> getAdminBookList(Map<String, Object> var1);
	List<Book> getMegazineList(Map<String, Object> var1);
	List<WebBook> getWebList(Map<String, Object> var1);
	List<Map<String, Object>> OneOne(Map<String, Object> map);
	List<Map<String, Object>> OneTwo(Map<String, Object> map);
	List<Map<String, Object>> OneThree(Map<String, Object> map);
	List<Map<String, Object>> OneFour(Map<String, Object> map);
	List<Map<String, Object>> OneFive(Map<String, Object> map);
	List<Map<String, Object>> OneSix(Map<String, Object> map);
	List<Map<String, Object>> OneSeven(Map<String, Object> map);
	List<Map<String, Object>> OneEight(Map<String, Object> map);
	List<Map<String, Object>> OneNine(Map<String, Object> map);
	List<Map<String, Object>> OneTen(Map<String, Object> map);
	List<Map<String, Object>> One(Map<String, Object> map);
	
	List<Map<String, Object>> TwoOne(Map<String, Object> map);
	List<Map<String, Object>> TwoTwo(Map<String, Object> map);
	List<Map<String, Object>> TwoThree(Map<String, Object> map);
	List<Map<String, Object>> TwoFour(Map<String, Object> map);
	List<Map<String, Object>> TwoFive(Map<String, Object> map);
	List<Map<String, Object>> TwoSix(Map<String, Object> map);
	List<Map<String, Object>> TwoSeven(Map<String, Object> map);
	List<Map<String, Object>> TwoEight(Map<String, Object> map);
	List<Map<String, Object>> TwoNine(Map<String, Object> map);
	List<Map<String, Object>> TwoTen(Map<String, Object> map);
	List<Map<String, Object>> Two(Map<String, Object> map);
	
	List<Map<String, Object>> ThreeSeven(Map<String, Object> map);
	List<Map<String, Object>> ThreeEight(Map<String, Object> map);
	List<Map<String, Object>> ThreeNine(Map<String, Object> map);
	List<Map<String, Object>> ThreeTen(Map<String, Object> map);
	List<Map<String, Object>> Three(Map<String, Object> map);
	
	List<Map<String, Object>> FourSeven(Map<String, Object> map);
	List<Map<String, Object>> FourEight(Map<String, Object> map);
	List<Map<String, Object>> FourNine(Map<String, Object> map);
	List<Map<String, Object>> FourTen(Map<String, Object> map);
	List<Map<String, Object>> Four(Map<String, Object> map);
	
	Map<String, Object> getUpdateInfo(String jNo);
	void updateInfo1(Map<String, Object> map);
	void updateInfo2(Map<String, Object> map);
	void updateInfo3(Map<String, Object> map);
	void updateInfo4(Map<String, Object> map);
	void updateInfo5(Map<String, Object> map);
	void updateInfo6(Map<String, Object> map);
	void updateInfo7(Map<String, Object> map);
	void updateInfo8(Map<String, Object> map);
	Integer getBookId(Map<String, Object> map);
	Boolean getBookBoolean(Map<String, Object> map);
	Cart getMegazineCart(Integer cartKey);
	void updateMegazineCart(Integer cartKey);
	void insertMegazineCart(Cart cart);
	void updateMegazineMegazine(OrderMegazine megazine);
	void updateMegazineNoMember(String jNo);
	void updateMegazinePri(String jNo);
	void insertMegazinePri(Order order);
	void updateBundlePayment(Map<String, Object> map);
	void updatePriPayment1(Map<String, Object> map);
	void updatePriPayment2(Map<String, Object> map);
	void updateMegazinePayment(Map<String, Object> map);
	void updateNoMemberPayment(Map<String, Object> map);
	
	
	OrderMegazine getMegazineMegazine(String jNo);
	OrderBundle getMegazineBundle(String jNo);
	Order getMegazinePri(String jNo);
	NoMember getMegazineNoMember(String jNo);
	
	boolean checkWebbook(String jNo);
	void updateWebbookPri(String jNo);
	void updateWebbookBundle(String jNo);
	void updateWebbookNoMem(String jNo);
	
	void updateInfo9(Map<String, Object> map);
	void updateInfo10(Map<String, Object> map);
	void updateInfo11(Map<String, Object> map);
	void updateInfo12(Map<String, Object> map);
	void updateInfo13(Map<String, Object> map);
	void updateInfo14(Map<String, Object> map);
	void updateInfo15(Map<String, Object> map);
	
	boolean getMegazine(String jNo);
	void updateMegazine(Map<String, Object> map);
	
	List<Member> getNOMember(String name);
	List<Member> getMember(String name);
	List<Member> getNOMemberNoName();
	List<Member> getMemberNoName();
	List<Member> getAllMember(String name);
	List<Member> getAllMemberNoName();
	void updateMemberInfo(Member member);
	
	Map<String, Object> getManageInfo();
	List<Map<String, Object>> getSubOrderList(String jNo);
	
	//order
	Member getUserInfo(String var1);
	void setStandingOrderCart(StandingOrder order);
	void setOrderCart(Order order);
	void setOrderCartWebbook(Order order);
	void setStandingOrderCartNoMember(StandingOrder order);
	void setOrderCartNoMember(Order order);
	void setOrderCartNoMemberWebbook(Order order);
	void insertStandingOrderPriNoMember(StandingOrder order);
	void insertStandingOrderPri(StandingOrder order);
	void insertOrderPriNoMember(Order order);
	void insertOrderPriNoMemberWebbook(Order order);
	void insertOrderPri(Order order);
	void insertOrderPriWebbook(Order order);
	void insertOrderBundle(OrderBundle bundle);
	void insertOrderBundleA(OrderBundle bundle);
	void insertOrderBundleCardWeb(OrderBundle bundle);
	void insertOrderMegazine(StandingOrder order);
	void insertOrderMegazineNoMember(StandingOrder order);
	void insertStandingNoMember(StandingOrder order);
	void insertNoMember(Order order);
	Map<String, Object> sendMegazineInfo(Map<String, Object> map);
	
	boolean getCartKey(Map<String, Object> map);
	void updateCartKey(Map<String, Object> map);
	int getCartKeyId(Map<String, Object> map);
	int getCartWebKeyId(Map<String, Object> map);
	int getCartKeyId2(Map<String, Object> map);
	int getCartKeyId3(Cart cart);
	void updateMegazineBundle(OrderBundle bundle);
	void insertMegazineBundle(OrderBundle bundle);
	
	void updateOrderPri(Map<String, Object> map);
	void updateOrderBundle(Map<String, Object> map);
	void updateOrderBundleBank(Map<String, Object> map);
	void updateOrderPriBank(Map<String, Object> map);
	void updateCart(int cartKey);
	String getKey(String jNo);
	void updateNoMember(String jNo);
	void updateNoMemberBank(String jNo);
	void updateOrderMegazine(String str);
	void updateOrderMegazineBank(String jNo);
	
	void cancleOrderPri(Map<String, Object> map);
	void cancleOrderBundle(Map<String, Object> map);
	void cancleCart(int cartKey);
	void cancleNoMember(String jNo);
	void cancleOrderMegazine(String jNo);
	
	String getCartKeyList(String jNo);
	Map<String, Object> getCartMap(int cartKey);
	void insertCart(Map<String, Object> map);
	Map<String, Object> getOrderBunPri(String jNo);
	void updateOrderBundleMegazine(Map<String, Object> map);
	void insertOrderBundleMegazine(Map<String, Object> map);
	void insertOrderPriMegazine(Map<String, Object> map);
	
	void insertCartNoMem(Map<String, Object> param);
	void insertCartMem(Map<String, Object> param);
	
	Map<String, Object> getOrderBundleInfo(String jNo);
	List<Map<String, Object>> getOrderPriInfo(String jNo);
	
	Map<String, Object> getPayInfo(String id);
	
	void setBook(Map<String, Object> map);
	void setBook2(Map<String, Object> map);
	void setBook3(Map<String, Object> map);
	void addNewBook(Map<String, Object> map);
}