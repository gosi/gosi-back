package com.gosi.db;

import com.gosi.db.dao.GosiDao;
import com.gosi.db.dto.Book;
import com.gosi.db.dto.Cart;
import com.gosi.db.dto.Member;
import com.gosi.db.dto.NoMember;
import com.gosi.db.dto.Notice;
import com.gosi.db.dto.Order;
import com.gosi.db.dto.OrderBundle;
import com.gosi.db.dto.OrderMegazine;
import com.gosi.db.dto.StandingOrder;
import com.gosi.db.dto.WebBook;
import com.gosi.utils.input.Input;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;

@Component
public class GosiDbHandler implements DbHandler {
	@Resource(name = "sqlSession")
	private SqlSession sqlSession;
	
	@Override
	public void update(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.update(map);	
	}
	@Override
	public List<Map<String, Object>> getList() {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getList();
	}
	
	//자동 프로젝트
	@Override
	public List<Map<String, Object>> getMonitorList() {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getMonitorList();
	}
	
	@Override
	public void updateMonitorList(String jNo) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.updateMonitorBundle(jNo);
		dao.updateMonitorPri(jNo);
	}
	
	@Override
	public List<Map<String, Object>> getMonitorList2() {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getMonitorList2();
	}
	
	@Override
	public void updateMonitorList2(String jNo) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.updateMonitorBundle2(jNo);
		dao.updateMonitorPri2(jNo);
	}
	
	@Override
	public List<Map<String, Object>> getMonitorList3() {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getMonitorList3();
	}
	
	@Override
	public void updateMonitorList3(String jNo) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.updateMonitorList3(jNo);
	}
	
	@Override
	public List<Map<String, Object>> getMonitorList4() {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		String date = Input.getDay();
		return dao.getMonitorList4(date);
	}
	
	@Override
	public void updateMonitorList4(String jNo, int curCnt, String deliDate) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		Map<String, Object> param = new HashMap<>();
		param.put("curCnt", curCnt++);
		param.put("jNo", jNo);
		
		Map<String, Object> map = dao.getOrderBunPri(jNo);
		
		//확인하고 -> curCnt = 현재까지 주문 갯수가 동일해야함
		if(map.size() > 0) {
			String comment2 = (String)map.get("comment");
			String month2 = Input.getMonth(); //월
			String day2 = Input.getDay(); //일
			
			if(!comment2.substring(4).equals(month2)) { //보낸 월이 같으면 안됨
				if(Integer.parseInt(deliDate) <= Integer.parseInt(day2)) {
					//새롭게 보내기
					dao.updateMonitorList4(map);
					//기존 ORDER_BUNDLE UPDATE
					//cart, order_pri, bundle update
					String cartKeys = dao.getCartKeyList(jNo);
					String[] arr = cartKeys.split(",");
					String cartKey = arr[arr.length-1]; //가장 마지막 cartKey
					
					Map<String, Object> cartMap = dao.getCartMap(Integer.parseInt(cartKey));
					String comment = "";
					//comment
					String[] brr = comment2.split("-");
					if(brr[1].equals("12")) {
						int year = Integer.parseInt(brr[0])+1;
						comment = String.valueOf(year).concat("-01");
					}
					else {
						int month = Integer.parseInt(brr[1])+1;
						comment = brr[0].concat("-").concat(String.valueOf(month));
					}
					
					cartMap.put("comment", comment);
					dao.insertCart(cartMap);
					int cartKey2 = dao.getCartKeyId2(cartMap);
					Map<String, Object> orderMap = dao.getOrderBunPri(jNo);
					cartKey = cartKeys.concat(",").concat(String.valueOf(cartKey2)); 
					orderMap.put("cartKeyList", cartKey);
					orderMap.put("cartKey", cartKey2);
					//cartKey 물고와서
					dao.updateOrderBundleMegazine(orderMap);
					dao.insertOrderBundleMegazine(orderMap);
					dao.insertOrderPriMegazine(orderMap);
				}
			}
		}
	}
	
	@Override
	public void updateDeli1(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.updateDeli1(map);
	}
	
	@Override
	public void updateDeli2(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.updateDeli21(map);
		dao.updateDeli22(map);
	}
	
	@Override
	public void updateDeli3(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.updateDeli3(map);
	}
	
	@Override
	public void updateDeli4(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.updateDeli4(map);
	}
	
	
	//common
	public List<Notice> getBbsList(Map<String, Object> param, int type) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		List<Notice> info = new ArrayList<Notice>();
		if (type == 0) {
			List<Notice> list1 = dao.getSubBbsList("NOTICE");
			((List) info).addAll(list1);
			List<Notice> list2 = dao.getSubBbsList("LAW_NEWS");
			((List) info).addAll(list2);
		} else {
			param = this.getBbsCd(param);
			info = dao.getBbsList(param);
		}

		return (List) info;
	}
	
	//common
	public Integer getBbsPageNum(String subId) {
			GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
			int num = 0;
			
			if (subId.equals("1")) 
				num = dao.getSubBbsPageNum("NOTICE");
			else if (subId.equals("2")) 
				num = dao.getSubBbsPageNum("LAW_NEWS");
			
			
			return num;
		}

	public Map<String, Object> getBbsCd(Map<String, Object> param) {
		if (param.get("id").equals("1")) {
			param.put("cate", "NOTICE");
		} else if (param.get("id").equals("2")) {
			param.put("cate", "LAW_NEWS");
		}

		param.put("totalLimit", 10 * (Integer) param.get("pageNum"));
		return param;
	}

	public List<WebBook> getWebBookList(String yMon) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		List<WebBook> list = dao.getWebBookList(yMon);
		return list;
	}

	public List<Book> getBookList(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getBookList(param);
	}
	
	public Integer getPageNum(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getPageNum(param);
	}

	public Book getBookDetail(int str) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getBookDetail(str);
	}

	public Notice getDetailInfo(int str) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		Notice notice = dao.getDetailInfo(str);
		return notice;
	}

	
	//login
	public boolean checkMember(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.checkMember(param);
	}

	public Integer addInfo(Member mem) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		Map<String, Object> param = new HashMap<>();
		param.put("name", mem.getName());
		param.put("email", mem.getEmail());
		
		if(checkMember(param))
			return 0;
		else {
			dao.updateNoMemberInfo(param);
			dao.addInfo(mem);
			return 1;
		}
		
	}

	public boolean checkId(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.checkId(param);
	}

	public Map<String, Object> getHash(String id) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		Map<String, Object> map = dao.getHash(id);
		return map;
	}
	
	public String getOnlyHash(String id) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		String hash = dao.getOnlyHash(id);
		return hash;
	}

	public String findId(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		String hash = dao.findId(param);
		return hash;
	}

	public String findPassWd(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		String hash = dao.findPassWd(param);
		return hash;
	}

	public void changePassWd(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.changePassWd(param);
	}

	public String getNonHash(String id) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		String hash = dao.getNonHash(id);
		return hash;
	}

	
	//cart
	public List<Map<String, Object>> getCartBookList(String id) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getCartBookList(id);
	}

	public List<Map<String, Object>> getCartMagazineList(String id) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getCartMagazineList(id);
	}

	public List<Map<String, Object>> getCartWebList(String id) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getCartWebList(id);
	}

	public void addCart(Cart cart) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		//if (cart.getCount() > 0) {
		//	for (int i = 1; i <= cart.getCount(); ++i) {
				dao.addCart(cart);
		//	}
		//}

	}

	public void addMegazineCart(Cart cart) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		if(cart.getComment() .length()>3) {
			if(cart.getComment().charAt(4)=='-') {
				Map<String, Object> param = new HashMap<>();
				String comment = cart.getComment();
				String[] arr = comment.split("-");
				String searchWord = arr[0];
				param.put("search", searchWord);
				Map<String, Object> map = dao.sendMegazineInfo(param);
				cart.setBookId((int) map.get("bookId"));
				cart.setTitle((String)map.get("title"));
				dao.addMegazineCart(cart);
			}
			else 
				dao.addWebCart(cart);
		}
		else 
			dao.addWebCart(cart);
	}

	public void deleteCart(int key) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.deleteCart(key);
	}
	
	@Override
	public void addOrderCart(List<Order> orderList, String id, String pwd, String hash) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		List<Map<String, Object>> list = new ArrayList<>();
		
		OrderBundle bundle = new OrderBundle();
		for(int i=0; i<orderList.size(); i++) {
			Order order = orderList.get(i);
			bundle.setBookCate(order.getBookCate());
			bundle.setPrice(order.getSalePrice()*order.getCount());
			
			Map<String, Object> param = new HashMap<>();
			param.put("memYn", "N");
			param.put("id", id);
			param.put("pwd", pwd);
			param.put("pwd_hash", hash);
			param.put("bookId", order.getBookId());
			param.put("title", order.getTitle());
			param.put("comment", order.getComment());
			param.put("count", order.getCount());
			param.put("cartKey", order.getCartKey());
			param.put("price", order.getPrice());
			param.put("salePrice", order.getSalePrice());
			param.put("bookType", order.getBookType());
			param.put("bookCate", order.getBookCate());
			list.add(param);
		}
		
		for(int i=0; i<list.size(); i++) {
			Map<String, Object> map = list.get(i);
			map.put("totalPrice", bundle.getTotalPrice());
			map.put("deliFee", bundle.getDeliveryFee());
			dao.addOrderCartNo(map);
		}
	}
	
	@Override
	public void addOrderCart(List<Order> orderList, String id) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		List<Map<String, Object>> list = new ArrayList<>();
		
		OrderBundle bundle = new OrderBundle();
		for(int i=0; i<orderList.size(); i++) {
			Order order = orderList.get(i);
			bundle.setBookCate(order.getBookCate());
			bundle.setPrice(order.getSalePrice()*order.getCount());
			
			Map<String, Object> param = new HashMap<>();
			param.put("memYn", "Y");
			param.put("id", id);
			param.put("bookId", order.getBookId());
			param.put("title", order.getTitle());
			param.put("comment", order.getComment());
			param.put("count", order.getCount());
			param.put("cartKey", order.getCartKey());
			param.put("price", order.getPrice());
			param.put("salePrice", order.getSalePrice());
			param.put("bookType", order.getBookType());
			param.put("bookCate", order.getBookCate());
			list.add(param);
		}
		
		for(int i=0; i<list.size(); i++) {
			Map<String, Object> map = list.get(i);
			map.put("totalPrice", bundle.getTotalPrice());
			map.put("deliFee", bundle.getDeliveryFee());
			dao.addOrderCartYes(map);
		}
	}
	
	@Override
	public boolean check(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.check(map);
	}
	
	@Override
	public List<Map<String, Object>> getOrderCart(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getOrderCart(map);
	}
	
	@Override
	public void deleteOrderCart(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.deleteOrderCart(map);
	}

	//mypage
	public List<Map<String, Object>> getNonmemberOrderList(String jNo) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getNonmemberOrderList(jNo);
	}

	public List<Map<String, Object>> getOrderList(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		List<String> list = dao.getJnoList(param);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jNos", list);
		return dao.getOrderList(params);
	}
	
	@Override
	public List<Map<String, Object>> getWebOrderList(String id) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		List<Map<String, Object>> list = dao.getWebOrderList(id);
		return list;
	}

	public void changeInfo(Member mem) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.changeInfo(mem);
	}

	public void changePassWd2(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.changePassWd2(param);
	}

	public boolean deleteInfo(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		if(dao.checkMem(param)) {
			dao.deleteInfo(param);
			return true;
		}
		else 
			return false;
	}
	
	@Override
	public Map<String, Object> getMemberInfo(String id) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getMemberInfo(id);
	}
	
	@Override
	public void deleteOrder(String jNo) {
		Map<String, Object> map = new HashMap<>();
		map.put("jNo", jNo);
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.cancleOrderPri(map);
		dao.cancleOrderBundle(map);
		dao.cancleNoMember(jNo);
	}
	
	@Override
	public boolean checkOrder(String jNo) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.checkOrder(jNo);
	}

	
	//admin
	public void addBook(Book book) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.addBook(book);
	}

	public void changeBook(Book book) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.changeBook(book);
	}

	public void deleteBook(int bookId) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.deleteBook(bookId);
	}
	
	public List<Book> getAdminBookList(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getAdminBookList(param);
	}
	
	public void addMegazine(Book book) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.addMegazine(book);
	}

	public void changeMegazine(Book book) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.changeMegazine(book);
	}
	public void addWebBook(WebBook book) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.addWebBook(book);
	}

	public void changeWebBook(WebBook book) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.changeWebBook(book);
	}
	
	public List<Book> getMegazineList(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getMegazineList(param);
	}
	
	public List<WebBook> getWebList(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getWebList(param);
	}
	
	@Override
	public List<Map<String, Object>> getTypeOneList(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		
		String status = (String) map.get("status");
		String payStatus = (String) map.get("payStatus");
		
		if(status.equals("배송준비중")) {
			if(payStatus.equals("카드결제")) //pay_type = '신용카드', delivery_start_yn = 'n'
				return dao.OneOne(map); 
			else if(payStatus.equals("무통장 입금완료")) //pay_type = '무통장입금', depodit_yn = 'y', delivery_start_yn = 'n'
				return dao.OneTwo(map);
		}
		else if(status.equals("배송중")) {
			if(payStatus.equals("카드결제")) //pay_type = '신용카드', delivery_start_yn = 'y', delivery_end_yn = 'n'
				return dao.OneThree(map);
			else if(payStatus.equals("무통장 입금완료")) //pay_type = '무통장입금', delivery_start_yn = 'y', delivery_end_yn = 'n'
				return dao.OneFour(map);
		}
		else if(status.equals("배송완료")) {
			if(payStatus.equals("카드결제")) //pay_type = '신용카드', delivery_end_yn = 'y'
				return dao.OneFive(map);
			else if(payStatus.equals("무통장 입금완료")) //pay_type = '무통장입금', delivery_end_yn = 'y'
				return dao.OneSix(map);
		}
		else{ //전체
			if(payStatus.equals("카드결제")) //pay_type = '신용카드'
				return dao.OneSeven(map);
			else if(payStatus.equals("무통장 입금전")) //pay_type = '무통장입금', depodit_yn = 'n'
				return dao.OneEight(map);
			else if(payStatus.equals("무통장 입금완료")) //pay_type = '무통장입금', depodit_yn = 'y'
				return dao.OneNine(map);
			else if(payStatus.equals("취소")) //status = '주문취소'
				return dao.OneTen(map);
			else 
				return dao.One(map);
		}
		
		return null;
	}
	
	@Override
	public List<Map<String, Object>> getTypeTwoList(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		
		String status = (String) map.get("status");
		String payStatus = (String) map.get("payStatus");
		
		if(status.equals("배송준비중")) {
			if(payStatus.equals("카드결제")) //pay_type = '신용카드', delivery_start_yn = 'n'
				return dao.TwoOne(map); 
			else if(payStatus.equals("무통장 입금완료")) //pay_type = '무통장입금', depodit_yn = 'y', delivery_start_yn = 'n'
				return dao.TwoTwo(map);
		}
		else if(status.equals("배송중")) {
			if(payStatus.equals("카드결제")) //pay_type = '신용카드', delivery_start_yn = 'y', delivery_end_yn = 'n'
				return dao.TwoThree(map);
			else if(payStatus.equals("무통장 입금완료")) //pay_type = '무통장입금', delivery_start_yn = 'y', delivery_end_yn = 'n'
				return dao.TwoFour(map);
		}
		else if(status.equals("배송완료")) {
			if(payStatus.equals("카드결제")) //pay_type = '신용카드', delivery_end_yn = 'y'
				return dao.TwoFive(map);
			else if(payStatus.equals("무통장 입금완료")) //pay_type = '무통장입금', delivery_end_yn = 'y'
				return dao.TwoSix(map);
		}
		else{ //전체
			if(payStatus.equals("카드결제")) //pay_type = '신용카드'
				return dao.TwoSeven(map);
			else if(payStatus.equals("무통장 입금전")) //pay_type = '무통장입금', depodit_yn = 'n'
				return dao.TwoEight(map);
			else if(payStatus.equals("무통장 입금완료")) //pay_type = '무통장입금', depodit_yn = 'y'
				return dao.TwoNine(map);
			else if(payStatus.equals("취소")) //status = '주문취소'
				return dao.TwoTen(map);
			else 
				return dao.Two(map);
		}
		
		return null;
	}
	
	@Override
	public List<Map<String, Object>> getTypeThreeList(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		
		String payStatus = (String) map.get("payStatus");
		
		if(payStatus.equals("카드결제")) //pay_type = '신용카드'
			return dao.ThreeSeven(map);
		else if(payStatus.equals("무통장 입금전")) //pay_type = '무통장입금', depodit_yn = 'n'
			return dao.ThreeEight(map);
		else if(payStatus.equals("무통장 입금완료")) //pay_type = '무통장입금', depodit_yn = 'y'
			return dao.ThreeNine(map);
		else if(payStatus.equals("취소")) //status = '주문취소'
			return dao.ThreeTen(map);
		else 
			return dao.Three(map);
	}
	
	@Override
	public List<Map<String, Object>> getTypeFourList(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);

		String payStatus = (String) map.get("payStatus");
		
		if(payStatus.equals("카드결제")) //pay_type = '신용카드'
			return dao.FourSeven(map);
		else if(payStatus.equals("무통장 입금전")) //pay_type = '무통장입금', depodit_yn = 'n'
			return dao.FourEight(map);
		else if(payStatus.equals("무통장 입금완료")) //pay_type = '무통장입금', depodit_yn = 'y'
			return dao.FourNine(map);
		else if(payStatus.equals("취소")) //status = '주문취소'
			return dao.FourTen(map);
		else 
			return dao.Four(map);
	}
	
	@Override
	public Map<String, Object> getUpdateInfo(String cartKey) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getUpdateInfo(cartKey);
	}
	
	@Override
	public void updatePaymentInfo(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		
		String payStatus = (String) param.get("payStatus"); //입금변화
		String jNo = (String) param.get("jNo");
		int cartKey = (Integer)param.get("cartKey");
		
		if(payStatus.equals("무통장입금완료")) {
			payStatus = "결제완료";
			param.put("payStatus", payStatus);
			//test1
			if(!dao.checkWebbook(jNo)) { //다 web
				dao.updateWebbookPri(jNo);
				dao.updateWebbookBundle(jNo);
				dao.updateWebbookNoMem(jNo);
				String cartKeyList = dao.getKey(jNo);
				String[] arr = cartKeyList.split(",");
				for(int i=0; i<arr.length; i++) {
					dao.updateCart(Integer.parseInt(arr[i]));
				}
			}
			else {
				
				if(dao.getBookBoolean(param)) { // megazine일 경우 (직접구매만 가능하기에 하나의 jNo는 동일하게 megazine만 존재)
					Map<String, Object> map = new HashMap<>();
					
					Cart cart = dao.getMegazineCart(cartKey);
					OrderMegazine megazine = dao.getMegazineMegazine(jNo);
					Order pri = dao.getMegazinePri(jNo);
					OrderBundle bundle = dao.getMegazineBundle(jNo);
					NoMember noMem = dao.getMegazineNoMember(jNo);
					
					int cartKey2 = 0;
					
					String deliDate = megazine.getDeliDate();
					String comment = megazine.getStartYear().concat("-").concat(megazine.getStartMonth());
					map.put("jNo", jNo);
					map.put("deliDate", deliDate);
					
					if(cart!=null) {
						dao.updateMegazineCart(cartKey);
						cart.setComment(comment);
						dao.insertMegazineCart(cart);
						cartKey2 = dao.getCartKeyId3(cart);
					}
					if(bundle !=null) {
						bundle.setCartKeyList(String.valueOf(cartKey2));
						dao.updateMegazineBundle(bundle);
						//dao.insertMegazineBundle(bundle);
					}
					if(pri!=null) {
						dao.updateMegazinePri(jNo);
						pri.setCartKey(cartKey2);
						dao.insertMegazinePri(pri);
					}
					if(megazine !=null)
						dao.updateMegazineMegazine(megazine);
					if(noMem !=null) 
						dao.updateMegazineNoMember(jNo);
				}
				else {//일반 책인 경우
					param.put("seeYn", "Y");
					param.put("depositYn", "Y");
					dao.updateBundlePayment(param);
					dao.updatePriPayment1(param);
					dao.updateMegazinePayment(param);
					dao.updateNoMemberPayment(param);
					String cartKeyList = dao.getKey(jNo);
					String[] arr = cartKeyList.split(",");
					for(int i=0; i<arr.length; i++) {
						dao.updateCart(Integer.parseInt(arr[i]));
					}
				}				
			}
		}
		else if(payStatus.equals("취소")) {
			payStatus = "결제취소";
			param.put("seeYn", "N");
			param.put("depositYn", "N");
			param.put("payStatus", payStatus);
			
			dao.updateBundlePayment(param);
			dao.updatePriPayment2(param);
			dao.updateMegazinePayment(param);
			dao.updateNoMemberPayment(param);
			String cartKeyList = dao.getKey(jNo);
			String[] arr = cartKeyList.split(",");
			for(int i=0; i<arr.length; i++) {
				dao.updateCart(Integer.parseInt(arr[i]));
			}
		}
	}
	
	@Override
	public void updateDeliveryInfo(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		
		String deliStatus = (String) param.get("deliStatus"); //배송변화
		String deliNum = (String) param.get("deliNum"); //배송번호
		String jNo = (String) param.get("jNo");

		if(deliNum.equals("")) {
			if(deliStatus.equals("배송중"))  dao.updateInfo2(param);
			else dao.updateInfo3(param); //배송완료
		}
		else {
			if(deliStatus.equals("")) 
				dao.updateInfo4(param);
			else {
				if(deliStatus.equals("배송중"))  dao.updateInfo6(param);
				else dao.updateInfo7(param);
			}
		}
	}
	
	@Override
	public List<Member> getAllMember(String name) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		if(name.equals(""))
			return dao.getAllMemberNoName();
		else
			return dao.getAllMember(name);
	}
	
	@Override
	public List<Member> getMember(String name) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		if(name.equals(""))
			return dao.getMemberNoName();
		else
			return dao.getMember(name);
	}
	
	@Override
	public List<Member> getNOMember(String name) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		if(name.equals(""))
			return dao.getNOMemberNoName();
		else
			return dao.getNOMember(name);
	}
	
	@Override
	public void updateMemberInfo(Member member) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.updateMemberInfo(member);
	}
	
	@Override
	public Map<String, Object> getManageInfo() {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getManageInfo();
	}
	
	//order
	public Member getUserInfo(String id) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getUserInfo(id);
	}
	
	@Override
	public void setStandingOrderNoMember(StandingOrder order) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		
		Date dt = new Date();
		Calendar cal = new GregorianCalendar(Locale.KOREA);
		cal.setTime(dt);
		SimpleDateFormat newFromat = new SimpleDateFormat("MM");
		SimpleDateFormat newFromat2 = new SimpleDateFormat("yyyy");
		String monthStr = newFromat.format(cal.getTime());
		String yearStr = newFromat2.format(cal.getTime());
		
		OrderBundle bundle = new OrderBundle();
		
		Map<String, Object> param = new HashMap<>();
		param.put("bookId", order.getBookId());
		param.put("userId", order.getOrderName());
		
		bundle.setMEM_YN("N");
		bundle.setUserId(order.getOrderName());
		bundle.setName(order.getOrderName());
		bundle.setAddr1(order.getOrderAddr1());
		bundle.setAddr2(order.getOrderAddr2());
		bundle.setZipCd(order.getOrderZipCode());
		bundle.setTel(order.getOrderTel());
		bundle.setEmail(order.getOrderEmail());
		bundle.setjNo(order.getjNo());
		bundle.setPayType(order.getPayType());
		bundle.setDeliveryNm(order.getDeliveryName());
		bundle.setDeliveryAddr1(order.getDeliveryAddr1());
		bundle.setDeliveryAddr2(order.getDeliveryAddr2());
		bundle.setDeliveryZipCd(order.getDeliveryZipCode());
		bundle.setDeliveryTel2(order.getDeliveryTel());
		bundle.setDeliveryMemo(order.getDeliveryMemo());
		
		bundle.setBookCate(0);
		bundle.setPrice(order.getPrice()*order.getNumOfCopies());
		
		//insert 해주고(order_dt = now())
		dao.setStandingOrderCartNoMember(order);
		//cartKey가지고옴
		int cartKeyy = dao.getCartKeyId(param);
		order.setCartKey(cartKeyy);
		//order_pri에 넣음
		//book_type='GOSI'
		//order_megazine에 넣음.카드만
		if(order.getPayType().equals("card")) {
			order.setPayType("");
			order.setSeeYn("N");
			dao.insertStandingOrderPriNoMember(order);
			//order_bundle에 넣음
			//list가 하나임
			bundle.setPrice(order.getPrice());
			bundle.setTotalPrice(order.getPrice());
			bundle.setCartKeyList(String.valueOf(cartKeyy));
			dao.insertOrderBundleA(bundle);

			if(order.getBookId()==111) //_112 -> 6, _111 -> 12
				order.setMonths(12);
			else
				order.setMonths(6);
			order.setPayStatus("N");
			
			//여기에 startMonth가 오늘날짜로 같은지 아니면 다음달인지 확인해야함!
			String month = order.getStartMonth();
			String year = order.getStartYear();
			int curCnt = 0;

			String comment = "";
			String jNo = order.getjNo();
			String cartKey = dao.getCartKeyList(jNo);
			Map<String, Object> cartMap = dao.getCartMap(Integer.parseInt(cartKey));
			
			if(Integer.parseInt(monthStr) == Integer.parseInt(month)) {
				curCnt = 1;
				comment = year.concat("-").concat(month);
				cartMap.put("comment", comment);
				dao.insertCart(cartMap);
				int cartKey2 = dao.getCartKeyId2(cartMap);
				Map<String, Object> orderMap = dao.getOrderBunPri(jNo);
				cartKey = cartKey.concat(",").concat(String.valueOf(cartKey2)); 
				orderMap.put("cartKeyList", cartKey);
				orderMap.put("cartKey", cartKey2);
				//cartKey 물고와서
				dao.updateOrderBundleMegazine(orderMap);
				dao.insertOrderPriMegazine(orderMap);
			}
			else if(yearStr.equals(Input.getYear()) && Integer.parseInt(monthStr) > Integer.parseInt(month)){
				//이 전달부터 여러권 주문, curCnt 계산
				Map<String, Object> orderMap2 = new HashMap<>();
				for(int i=Integer.parseInt(month); i<=Integer.parseInt(monthStr); i++) {
					curCnt++;
					if(i<10)
						comment = year.concat("-0").concat("i");
					else
						comment = year.concat("-").concat("i");
					cartMap.put("comment", comment);
					dao.insertCart(cartMap);
					int cartKey2 = dao.getCartKeyId2(cartMap);
					Map<String, Object> orderMap = dao.getOrderBunPri(jNo);
					cartKey = cartKey.concat(",").concat(String.valueOf(cartKey2)); 
					orderMap.put("cartKey", cartKey2);
					orderMap2.put("jNo", jNo);
					orderMap2.put("cartKeyList", cartKey);
					//cartKey 물고와서
					dao.insertOrderPriMegazine(orderMap);
				}
				dao.updateOrderBundleMegazine(orderMap2);	
			}
			else if(Integer.parseInt(yearStr) > Integer.parseInt(year)){
				//이 전달부터 여러권 주문, curCnt 계산
				Map<String, Object> orderMap2 = new HashMap<>();
				for(int i=Integer.parseInt(month); i<=12; i++) {
					curCnt++;
					if(i<10)
						comment = year.concat("-0").concat("i");
					else
						comment = year.concat("-").concat("i");
					cartMap.put("comment", comment);
					dao.insertCart(cartMap);
					int cartKey2 = dao.getCartKeyId2(cartMap);
					Map<String, Object> orderMap = dao.getOrderBunPri(jNo);
					cartKey = cartKey.concat(",").concat(String.valueOf(cartKey2)); 
					orderMap.put("cartKey", cartKey2);
					orderMap2.put("jNo", jNo);
					orderMap2.put("cartKeyList", cartKey);
					//cartKey 물고와서
					dao.insertOrderPriMegazine(orderMap);
				}
				for(int i=1; i<=Integer.parseInt(monthStr); i++) {
					curCnt++;
					if(i<10)
						comment = year.concat("-0").concat("i");
					else
						comment = year.concat("-").concat("i");
					cartMap.put("comment", comment);
					dao.insertCart(cartMap);
					int cartKey2 = dao.getCartKeyId2(cartMap);
					Map<String, Object> orderMap = dao.getOrderBunPri(jNo);
					cartKey = cartKey.concat(",").concat(String.valueOf(cartKey2)); 
					orderMap.put("cartKey", cartKey2);
					orderMap2.put("jNo", jNo);
					orderMap2.put("cartKeyList", cartKey);
					//cartKey 물고와서
					dao.insertOrderPriMegazine(orderMap);
				}
				dao.updateOrderBundleMegazine(orderMap2);	
			}
		
			order.setCurCnt(curCnt);
			String deliDate = "01";
			order.setDeliDate(deliDate);
			dao.insertOrderMegazineNoMember(order);
		
		}
		else {
			order.setPayType("입금대기");
			order.setSeeYn("N");
			dao.insertStandingOrderPriNoMember(order);
			//order_bundle에 넣음
			//list가 하나임
			bundle.setPrice(order.getPrice());
			bundle.setTotalPrice(order.getPrice());
			bundle.setCartKeyList(String.valueOf(cartKeyy));
			dao.insertOrderBundle(bundle);
	
			if(order.getBookId()==111) //_112 -> 6, _111 -> 12
				order.setMonths(12);
			else
				order.setMonths(6);
			order.setPayStatus("N");
			order.setCurCnt(0);
			
			String deliDate = "01";
			order.setDeliDate(deliDate);
			dao.insertOrderMegazineNoMember(order);
		}
		//no_member에 넣음
		dao.insertStandingNoMember(order);
	}
	
	@Override
	public void setStandingOrder(StandingOrder order) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		
		Date dt = new Date();
		Calendar cal = new GregorianCalendar(Locale.KOREA);
		cal.setTime(dt);
		SimpleDateFormat newFromat = new SimpleDateFormat("MM");
		SimpleDateFormat newFromat2 = new SimpleDateFormat("yyyy");
		String monthStr = newFromat.format(cal.getTime());
		String yearStr = newFromat2.format(cal.getTime());
		
		OrderBundle bundle = new OrderBundle();
		
		bundle.setMEM_YN("Y");
		bundle.setUserId(order.getId());
		bundle.setName(order.getOrderName());
		bundle.setAddr1(order.getOrderAddr1());
		bundle.setAddr2(order.getOrderAddr2());
		bundle.setZipCd(order.getOrderZipCode());
		bundle.setTel(order.getOrderTel());
		bundle.setEmail(order.getOrderEmail());
		bundle.setjNo(order.getjNo());
		bundle.setPayType(order.getPayType());
		bundle.setDeliveryNm(order.getDeliveryName());
		bundle.setDeliveryAddr1(order.getDeliveryAddr1());
		bundle.setDeliveryAddr2(order.getDeliveryAddr2());
		bundle.setDeliveryZipCd(order.getDeliveryZipCode());
		bundle.setDeliveryTel2(order.getDeliveryTel());
		bundle.setDeliveryMemo(order.getDeliveryMemo());
		
		bundle.setBookCate(0);
		bundle.setPrice(order.getPrice()*order.getNumOfCopies());
		
		Map<String, Object> param = new HashMap<>();
		param.put("bookId", order.getBookId());
		param.put("userId", order.getId());
		
		//insert 해주고(order_dt = now())
		dao.setStandingOrderCart(order);
		//cartKey가지고옴
		int cartKeyy = dao.getCartKeyId(param);
		order.setCartKey(cartKeyy);
		//order_pri에 넣음
		//book_type='GOSI'

		//order_megazine에 넣음.카드만
		if(order.getPayType().equals("card")) {
			order.setPayType("");
			order.setSeeYn("N");
			dao.insertStandingOrderPri(order);
			//order_bundle에 넣음
			//list가 하나임
			bundle.setPrice(order.getPrice());
			bundle.setTotalPrice(order.getPrice());
			bundle.setCartKeyList(String.valueOf(cartKeyy));
			dao.insertOrderBundleA(bundle);
			
			if(order.getBookId()==111) //_112 -> 6, _111 -> 12
				order.setMonths(12);
			else
				order.setMonths(6);
			order.setPayStatus("N");
			
			//여기에 startMonth가 오늘날짜로 같은지 아니면 다음달인지 확인해야함!
			String month = order.getStartMonth();
			String year = order.getStartYear();
			int curCnt = 0;

			String comment = "";
			String jNo = order.getjNo();
			String cartKey = dao.getCartKeyList(jNo);
			Map<String, Object> cartMap = dao.getCartMap(Integer.parseInt(cartKey));
			
			if(Integer.parseInt(monthStr) == Integer.parseInt(month)) {
				curCnt = 1;
				comment = year.concat("-").concat(month);
				cartMap.put("comment", comment);
				dao.insertCart(cartMap);
				int cartKey2 = dao.getCartKeyId2(cartMap);
				Map<String, Object> orderMap = dao.getOrderBunPri(jNo);
				cartKey = cartKey.concat(",").concat(String.valueOf(cartKey2)); 
				orderMap.put("cartKeyList", cartKey);
				orderMap.put("cartKey", cartKey2);
				//cartKey 물고와서
				dao.updateOrderBundleMegazine(orderMap);
				dao.insertOrderPriMegazine(orderMap);
			}
			else if(yearStr.equals(Input.getYear()) && Integer.parseInt(monthStr) > Integer.parseInt(month)){
				//이 전달부터 여러권 주문, curCnt 계산
				Map<String, Object> orderMap2 = new HashMap<>();
				for(int i=Integer.parseInt(month); i<=Integer.parseInt(monthStr); i++) {
					curCnt++;
					if(i<10)
						comment = year.concat("-0").concat("i");
					else
						comment = year.concat("-").concat("i");
					cartMap.put("comment", comment);
					dao.insertCart(cartMap);
					int cartKey2 = dao.getCartKeyId2(cartMap);
					Map<String, Object> orderMap = dao.getOrderBunPri(jNo);
					cartKey = cartKey.concat(",").concat(String.valueOf(cartKey2)); 
					orderMap.put("cartKey", cartKey2);
					orderMap2.put("jNo", jNo);
					orderMap2.put("cartKeyList", cartKey);
					//cartKey 물고와서
					dao.insertOrderPriMegazine(orderMap);
				}
				dao.updateOrderBundleMegazine(orderMap2);	
			}
			else if(Integer.parseInt(yearStr) > Integer.parseInt(year)){
				//이 전달부터 여러권 주문, curCnt 계산
				Map<String, Object> orderMap2 = new HashMap<>();
				for(int i=Integer.parseInt(month); i<=12; i++) {
					curCnt++;
					if(i<10)
						comment = year.concat("-0").concat("i");
					else
						comment = year.concat("-").concat("i");
					cartMap.put("comment", comment);
					dao.insertCart(cartMap);
					int cartKey2 = dao.getCartKeyId2(cartMap);
					Map<String, Object> orderMap = dao.getOrderBunPri(jNo);
					cartKey = cartKey.concat(",").concat(String.valueOf(cartKey2)); 
					orderMap.put("cartKey", cartKey2);
					orderMap2.put("jNo", jNo);
					orderMap2.put("cartKeyList", cartKey);
					//cartKey 물고와서
					dao.insertOrderPriMegazine(orderMap);
				}
				for(int i=1; i<=Integer.parseInt(monthStr); i++) {
					curCnt++;
					if(i<10)
						comment = year.concat("-0").concat("i");
					else
						comment = year.concat("-").concat("i");
					cartMap.put("comment", comment);
					dao.insertCart(cartMap);
					int cartKey2 = dao.getCartKeyId2(cartMap);
					Map<String, Object> orderMap = dao.getOrderBunPri(jNo);
					cartKey = cartKey.concat(",").concat(String.valueOf(cartKey2)); 
					orderMap.put("cartKey", cartKey2);
					orderMap2.put("jNo", jNo);
					orderMap2.put("cartKeyList", cartKey);
					//cartKey 물고와서
					dao.insertOrderPriMegazine(orderMap);
				}
				dao.updateOrderBundleMegazine(orderMap2);	
			}
		
			order.setCurCnt(curCnt);
			String deliDate = "01";
			order.setDeliDate(deliDate);
			dao.insertOrderMegazine(order);
		}
		else {
			order.setPayType("입금대기");
			order.setSeeYn("N");
			dao.insertStandingOrderPri(order);
			//order_bundle에 넣음
			//list가 하나임
			bundle.setPrice(order.getPrice());
			bundle.setTotalPrice(order.getPrice());
			bundle.setCartKeyList(String.valueOf(cartKeyy));
			dao.insertOrderBundle(bundle);
	
			if(order.getBookId()==111) //_112 -> 6, _111 -> 12
				order.setMonths(12);
			else
				order.setMonths(6);
			order.setPayStatus("N");
			order.setCurCnt(0);
			
			String deliDate = "";
			if(Integer.parseInt(Input.getDay()) > 20) 
				deliDate = "01";
			else 
				deliDate = Input.getDay();
			order.setDeliDate(deliDate);
			dao.insertOrderMegazine(order);
		}
	}
	
	
	@Override
	public void setOrderNoMember(List<Order> orderList) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		
		OrderBundle bundle = new OrderBundle();
		Order mainOrder = orderList.get(0);
		//모든게 web이고 현재 신용카드 일 경우에
		boolean checkAllWeb = true; 
		
		bundle.setMEM_YN("N");
		bundle.setUserId(mainOrder.getOrderName());
		bundle.setName(mainOrder.getOrderName());
		bundle.setAddr1(mainOrder.getOrderAddr1());
		bundle.setAddr2(mainOrder.getOrderAddr2());
		bundle.setZipCd(mainOrder.getOrderZipCode());
		bundle.setTel(mainOrder.getOrderTel());
		bundle.setEmail(mainOrder.getOrderEmail());
		bundle.setjNo(mainOrder.getjNo());
		bundle.setPayType(mainOrder.getPayType());
		bundle.setDeliveryNm(mainOrder.getDeliveryName());
		bundle.setDeliveryAddr1(mainOrder.getDeliveryAddr1());
		bundle.setDeliveryAddr2(mainOrder.getDeliveryAddr2());
		bundle.setDeliveryZipCd(mainOrder.getDeliveryZipCode());
		bundle.setDeliveryTel2(mainOrder.getDeliveryTel());
		bundle.setDeliveryMemo(mainOrder.getDeliveryMemo());
		
		for(int i=0; i<orderList.size(); i++) {
			
			Order order = orderList.get(i);
			order.setMemYn("N");
			Map<String, Object> param = new HashMap<>();
			param.put("bookId", order.getBookId());
			param.put("userId", order.getOrderName());
			param.put("count", order.getCount());
			
			bundle.setBookCate(order.getBookCate());
			bundle.setPrice(order.getSalePrice()*order.getCount());
			
			if(order.getCartKey()!=0) { //있다면? 존재한다면
				//cartKey를 가지고옴
				int cartKey = order.getCartKey();
				order.setCartKey(cartKey);
				bundle.setCartKeyList(String.valueOf(cartKey));
				//cart에서 count, order_dt를 수정
				param.put("cartKey", cartKey);
				dao.updateCartKey(param);

				if(order.getBookType().equals("BOOK")) {
					checkAllWeb = false;
					//order_pri에 넣음
					if(order.getPayType().equals("card")) order.setPayType("");
					else order.setPayType("입금대기");
					//book_type='BOOK' || 'GOSI'
					if(order.getBookCate() == 0) order.setBookCateStr("GOSI");
					else order.setBookCateStr("BOOK");
					dao.insertOrderPriNoMember(order);
				}
				else {
					//order_pri에 넣음
					//book_type='WEBBOOK'
					if(order.getPayType().equals("card")) order.setPayType("");
					else order.setPayType("입금대기");
					dao.insertOrderPriNoMemberWebbook(order);
				}
			}
			else {
				//insert 해주고(order_dt = now()) -> book, webbook 인지 구분해야함
				if(order.getBookType().equals("BOOK")) {
					checkAllWeb = false;
					dao.setOrderCartNoMember(order);
					//cartKey가지고옴
					int cartKey = dao.getCartKeyId(param);
					order.setCartKey(cartKey);
					bundle.setCartKeyList(String.valueOf(cartKey));
					//order_pri에 넣음
					if(order.getPayType().equals("card")) order.setPayType("");
					else order.setPayType("입금대기");
					//book_type='BOOK' || 'GOSI'
					if(order.getBookCate() == 0) order.setBookCateStr("GOSI");
					else order.setBookCateStr("BOOK");
					dao.insertOrderPriNoMember(order);
				}
				else {
					dao.setOrderCartNoMemberWebbook(order);
					//cartKey가지고옴
					int cartKey = dao.getCartWebKeyId(param);
					order.setCartKey(cartKey);
					bundle.setCartKeyList(String.valueOf(cartKey));	
					//order_pri에 넣음
					//book_type='WEBBOOK'
					if(order.getPayType().equals("card")) order.setPayType("");
					else order.setPayType("입금대기");
					dao.insertOrderPriNoMemberWebbook(order);
				}	
			}
		}
		
		//no_member에 넣음
		//order_bundle에 넣음
		if(mainOrder.getPayType().equals("card")) {
			mainOrder.setPayType("");
			if(checkAllWeb) {
				bundle.setStatus("");
				dao.insertOrderBundleCardWeb(bundle);
			}
//			else 
//				dao.insertOrderBundle(bundle);
		}
		else { 
			mainOrder.setPayType("입금대기");
			dao.insertOrderBundle(bundle);
		}
		dao.insertNoMember(mainOrder);
		
	}
	
	@Override
	public void setOrder(List<Order> orderList) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		
		OrderBundle bundle = new OrderBundle();
		Order mainOrder = orderList.get(0);
		//모든게 web이고 현재 신용카드 일 경우에
		boolean checkAllWeb = true; 
		
		bundle.setMEM_YN("Y");
		bundle.setUserId(mainOrder.getId());
		bundle.setName(mainOrder.getOrderName());
		bundle.setAddr1(mainOrder.getOrderAddr1());
		bundle.setAddr2(mainOrder.getOrderAddr2());
		bundle.setZipCd(mainOrder.getOrderZipCode());
		bundle.setTel(mainOrder.getOrderTel());
		bundle.setEmail(mainOrder.getOrderEmail());
		bundle.setjNo(mainOrder.getjNo());
		bundle.setPayType(mainOrder.getPayType());
		bundle.setDeliveryNm(mainOrder.getDeliveryName());
		bundle.setDeliveryAddr1(mainOrder.getDeliveryAddr1());
		bundle.setDeliveryAddr2(mainOrder.getDeliveryAddr2());
		bundle.setDeliveryZipCd(mainOrder.getDeliveryZipCode());
		bundle.setDeliveryTel2(mainOrder.getDeliveryTel());
		bundle.setDeliveryMemo(mainOrder.getDeliveryMemo());
		
		for(int i=0; i<orderList.size(); i++) {
			Order order = orderList.get(i);
			order.setMemYn("Y");
			Map<String, Object> param = new HashMap<>();
			param.put("bookId", order.getBookId());
			param.put("userId", order.getId());
			param.put("count", order.getCount());
			
			bundle.setBookCate(order.getBookCate());
			bundle.setPrice(order.getSalePrice()*order.getCount());
			
			if(order.getCartKey()!=0) { //존재한다면
				//cartKey를 가지고옴
				int cartKey = order.getCartKey();
				order.setCartKey(cartKey);
				//cart에서 count, order_dt를 수정
				param.put("cartKey", cartKey);
				dao.updateCartKey(param);
				bundle.setCartKeyList(String.valueOf(cartKey));
				
				if(order.getBookType().equals("BOOK")) {
					checkAllWeb = false;
					//order_pri에 넣음
					if(order.getPayType().equals("card")) order.setPayType("");
					else order.setPayType("입금대기");
					//book_type='BOOK' || 'GOSI'
					if(order.getBookCate() == 0) order.setBookCateStr("GOSI");
					else order.setBookCateStr("BOOK");
					dao.insertOrderPri(order);
				}
				else {
					//order_pri에 넣음
					//book_type='WEBBOOK'
					if(order.getPayType().equals("card")) order.setPayType("");
					else order.setPayType("입금대기");
					dao.insertOrderPriWebbook(order);
				}
			}
			else {
				//insert 해주고(order_dt = now()) -> book, webbook 인지 구분해야함
				if(order.getBookType().equals("BOOK")) {
					checkAllWeb = false;
					dao.setOrderCart(order);
					//cartKey가지고옴
					int cartKey = dao.getCartKeyId(param);
					order.setCartKey(cartKey);
					bundle.setCartKeyList(String.valueOf(cartKey));
					//order_pri에 넣음
					if(order.getPayType().equals("card")) order.setPayType("");
					else order.setPayType("입금대기");
					//book_type='BOOK' || 'GOSI'
					if(order.getBookCate() == 0) order.setBookCateStr("GOSI");
					else order.setBookCateStr("BOOK");
					dao.insertOrderPri(order);
				}
				else {
					dao.setOrderCartWebbook(order);
					//cartKey가지고옴
					int cartKey = dao.getCartWebKeyId(param);
					order.setCartKey(cartKey);
					bundle.setCartKeyList(String.valueOf(cartKey));
					//order_pri에 넣음
					//book_type='WEBBOOK'
					if(order.getPayType().equals("card")) order.setPayType("");
					else order.setPayType("입금대기");
					dao.insertOrderPriWebbook(order);
				}
			}
		}	
		//order_bundle에 넣음
		if(mainOrder.getPayType().equals("card")) {
			if(checkAllWeb) 
				dao.insertOrderBundleCardWeb(bundle);
			else 
				dao.insertOrderBundle(bundle);
		}
		else 
			dao.insertOrderBundle(bundle);
	}
	
	
	@Override
	public Map<String, Object> sendMegazineInfo(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.sendMegazineInfo(map);
	}
	
	@Override
	public void updateOrder(Map<String, Object> param) {
		
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		
		String payType = (String) param.get("payType");
		String jNo = (String) param.get("jNo");
		String payStatus = (String) param.get("payStatus");
		
		if(payStatus.equals("success")) {
			
			if(payType.equals("card")) {
				param.put("payStatus", "결제완료");
				dao.updateOrderPri(param);
				String cartKeyList = dao.getKey(jNo);
				String[] arr = cartKeyList.split(",");
				for(int i=0; i<arr.length; i++) {
					dao.updateCart(Integer.parseInt(arr[i]));
				}
				dao.updateOrderBundle(param);
				dao.updateNoMember(jNo);
				dao.updateOrderMegazine(jNo);
				//updatemegazine
//				
//				Map<String, Object> map = new HashMap<>();
//				String deliDate = "";
//				if(Integer.parseInt(Input.getDay()) > 20) 
//					deliDate = "01";
//				else 
//					deliDate = Input.getDay();
//				map.put("jNo", jNo);
//				map.put("deliDate", deliDate);
//				
//				dao.updateOrderMegazine(map);
				
				//기존 ORDER_BUNDLE UPDATE
				//cart, order_pri, bundle update
//				String cartKey = dao.getCartKeyList(jNo);
//				String[] arr = cartKey.split(",");
//				for(int i=0; i<arr.length; i++) {
//					cartKey = arr[i];
//					
//					
//				}
				
//				Map<String, Object> cartMap = dao.getCartMap(Integer.parseInt(cartKey));
//				String comment = "";
//				//comment
//				if(Integer.parseInt(Input.getDay()) > 20) {
//					if(Input.getMonth().equals("12")) {
//						int year = Integer.parseInt(Input.getYear())+1;
//						comment = String.valueOf(year).concat("-01");
//					}
//					else {
//						int month = Integer.parseInt(Input.getMonth())+1;
//						comment = Input.getYear().concat("-").concat(String.valueOf(month));
//					}
//				}
//				else
//					 comment = Input.getYear().concat("-").concat(Input.getMonth());
//				
//				cartMap.put("comment", comment);
//				dao.insertCart(cartMap);
//				int cartKey2 = dao.getCartKeyId2(cartMap);
//				Map<String, Object> orderMap = dao.getOrderBunPri(jNo);
//				cartKey = cartKey.concat(",").concat(String.valueOf(cartKey2)); 
//				orderMap.put("cartKeyList", cartKey);
//				orderMap.put("cartKey", cartKey2);
//				//cartKey 물고와서
//				dao.updateOrderBundleMegazine(orderMap);
//			//	dao.insertOrderBundleMegazine(orderMap);
//				dao.insertOrderPriMegazine(orderMap);
			}
			else { //무통장입금
				param.put("payStatus", "입금대기");
				dao.updateOrderBundleBank(param);
				dao.updateOrderPriBank(param);
				dao.updateNoMemberBank(jNo);
				dao.updateOrderMegazineBank(jNo);
			}
			
//			String cartKeys = dao.getKey(jNo);
//			String[] arr = cartKeys.split(",");
//			for(int i=0; i<arr.length; i++) {
//				int key = Integer.parseInt(arr[i]);
//				dao.updateCart(key);
//			}
		}
		else if(payStatus.equals("fail")) {
			//해야 하는게 있나? 모르겠당 -> 결제 실패를 만들어야 겠당
			//SUCCESS 보면서 같은 루트로 하면 될듯
			//취소처럼 -> 결제취소로 우선은
			param.put("payStatus", "결제취소");
 			dao.cancleOrderPri(param);
			dao.cancleOrderBundle(param);
			dao.cancleNoMember(jNo);
			dao.cancleOrderMegazine(jNo);
		}
		else { //취소
			param.put("payStatus", "결제취소");
			dao.cancleOrderPri(param);
			dao.cancleOrderBundle(param);
			dao.cancleNoMember(jNo);
			dao.cancleOrderMegazine(jNo);
		}
	}
	
	@Override
	public List<Map<String, Object>> getSubOrderList(String jNo) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getSubOrderList(jNo); 
	}
	
	@Override
	public void insertCartMem(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.insertCartMem(param);
	}
	
	@Override
	public void insertCartNoMem(Map<String, Object> param) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.insertCartNoMem(param);
	}
	
	@Override
	public Map<String, Object> getOrderBundleInfo(String jNo) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getOrderBundleInfo(jNo);
	}
	
	@Override
	public List<Map<String, Object>> getOrderPriInfo(String jNo) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getOrderPriInfo(jNo);
	}

	@Override
	public Map<String, Object> getPayInfo(String id) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		return dao.getPayInfo(id);
	}
	
	
	
	
	@Override
	public void setBook(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.setBook(map);
	}
	

	@Override
	public void setBook2(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.setBook2(map);
	}
	

	@Override
	public void setBook3(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.setBook3(map);
	}
	
	@Override
	public void addNewBook(Map<String, Object> map) {
		GosiDao dao = (GosiDao) this.sqlSession.getMapper(GosiDao.class);
		dao.addNewBook(map);
	}
}