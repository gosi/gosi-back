package com.gosi.db.dto;

public class Book {
	
	private int bookId;
	
	//cate 0:잡지, 1: 수험도서, 2:전문도서, 3:미디어북
	//현재 db에는 int로 되어 있음
	//private String cate;
	private int cate;
	
	private String title;
	private String author;
	//private String coAuthor;
	private String isbn;
	//private String isbn13;
	private String sellStatus;
	private String dspYn;
	private int price;
	private int salePrice;
	private String panNo;
	private String pubsEDt;
	//private String comment;
	//private String addInfoS;
	//private int pubsId;
	private String pubsNm;
	//private int goodsId;
	private String regDt;
	private String uptDt;
	
	//sale Percent
	private int sales;
	//db에서 사용중
	private String type;
	
	private int bookCate;
	private String bookType;
	
	
	//db에 추가
	//private String pageNum;
	private int pageNum;
	private String image;
	private String authorComment;
	private String bookComment;
	private String indexComment;
	
	
	
	public String getPubsNm() {
		return pubsNm;
	}

	public void setPubsNm(String pubsNm) {
		this.pubsNm = pubsNm;
	}

	public int getBookCate() {
		return bookCate;
	}

	public void setBookCate(int bookCate) {
		this.bookCate = bookCate;
	}

	public String getBookType() {
		return bookType;
	}

	public void setBookType(String bookType) {
		this.bookType = bookType;
	}

	public int getCate() {
		return cate;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getAuthorComment() {
		return authorComment;
	}

	public void setAuthorComment(String authorComment) {
		this.authorComment = authorComment;
	}

	public String getBookComment() {
		return bookComment;
	}

	public void setBookComment(String bookComment) {
		this.bookComment = bookComment;
	}

	public String getIndexComment() {
		return indexComment;
	}

	public void setIndexComment(String indexComment) {
		this.indexComment = indexComment;
	}

	public void setCate(int cate) {
		this.cate = cate;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSales() {
		return this.sales;
	}

	public void setSales(int sales) {
		this.sales = sales;
	}

	public int getBookId() {
		return this.bookId;
	}

	public void setBookId(int id) {
		this.bookId = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getIsbn() {
		return this.isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getSellStatus() {
		return this.sellStatus;
	}

	public void setSellStatus(String sellStatus) {
		this.sellStatus = sellStatus;
	}

	public String getDspYn() {
		return this.dspYn;
	}

	public void setDspYn(String dspYn) {
		this.dspYn = dspYn;
	}

	public int getPrice() {
		return this.price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getSalePrice() {
		return this.salePrice;
	}

	public void setSalePrice(int salePrice) {
		this.salePrice = salePrice;
	}

	public String getPanNo() {
		return this.panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getPubsEDt() {
		return this.pubsEDt;
	}

	public void setPubsEDt(String pubsEDt) {
		this.pubsEDt = pubsEDt;
	}

	public String getRegDt() {
		return this.regDt;
	}

	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}

	public String getUptDt() {
		return this.uptDt;
	}

	public void setUptDt(String uptDt) {
		this.uptDt = uptDt;
	}
}