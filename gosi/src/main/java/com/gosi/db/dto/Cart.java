package com.gosi.db.dto;

public class Cart {
	private int id;
	private String memYn;
	private String userId;
	private int bookId;
	private int webBookId;
	private String orderDt;
	private int cartKey;
	private int goodsId;
	private int count;
	private String regDt;
	private String uptDt;
	private String title;
	private String comment;

	public static Cart makeBook(String userId, String title, int bookId, int count) {
		Cart cart = new Cart();
		cart.setUserId(userId);
		cart.setTitle(title);
		cart.setBookId(bookId);
		cart.setCount(count);
		return cart;
	}

	public static Cart makeWeb(String userId, String title, int bookId, String comment, int count) {
		Cart cart = new Cart();
		cart.setUserId(userId);
		cart.setTitle(title);
		cart.setWebBookId(bookId);
		cart.setComment(comment);
		cart.setCount(count);
		return cart;
	}
	
	public static Cart makeMegazine(String userId, String title, String comment, int count) {
		Cart cart = new Cart();
		cart.setUserId(userId);
		cart.setTitle(title);
		cart.setComment(comment);
		cart.setCount(count);
		return cart;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMemYn() {
		return this.memYn;
	}

	public void setMemYn(String memYn) {
		this.memYn = memYn;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getBookId() {
		return this.bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public int getWebBookId() {
		return this.webBookId;
	}

	public void setWebBookId(int webBookId) {
		this.webBookId = webBookId;
	}

	public String getOrderDt() {
		return this.orderDt;
	}

	public void setOrderDt(String orderDt) {
		this.orderDt = orderDt;
	}

	public int getCartKey() {
		return this.cartKey;
	}

	public void setCartKey(int cartKey) {
		this.cartKey = cartKey;
	}

	public int getGoodsId() {
		return this.goodsId;
	}

	public void setGoodsId(int goodsId) {
		this.goodsId = goodsId;
	}

	public int getCount() {
		return this.count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getRegDt() {
		return this.regDt;
	}

	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}

	public String getUptDt() {
		return this.uptDt;
	}

	public void setUptDt(String uptDt) {
		this.uptDt = uptDt;
	}
}