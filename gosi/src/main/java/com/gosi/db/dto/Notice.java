package com.gosi.db.dto;

import java.util.ArrayList;
import java.util.List;

public class Notice {
	private int id;
	private String cate;
	private String title;
	private String text;
	private String add_info;
	private int readNum;
	private String dspYn;
	private String regDt;
	private String uptDt;
	private int bbsKey;
	private String uId;
	private int detailNum;
	List<String> fileList = new ArrayList<>();

	String local = "tomcat/webapps/";
	
	
	public List<String> getFileList() {
		return fileList;
	}

	public void setFileList(List<String> fileList) {
		this.fileList = fileList;
	}

	public String getuId() {
		return this.uId;
	}

	public void setuId(String uId) {
		this.uId = uId;
	}

	public int getDetailNum() {
		return this.detailNum;
	}

	public void setDetailNum(int detailNum) {
		this.detailNum = detailNum;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCate() {
		return this.cate;
	}

	public void setCate(String cate) {
		this.cate = cate;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getAdd_info() {
		return this.add_info;
	}

	public void setAdd_info(String add_info) {
		this.add_info = add_info;
		if(!this.add_info.equals("")) {
			String[] arr = this.add_info.split(",");
			for(int i=0; i<arr.length; i++) {
				fileList.add(local.concat(arr[i]));
			}
		}
	}

	public int getReadNum() {
		return this.readNum;
	}

	public void setReadNum(int readNum) {
		this.readNum = readNum;
	}

	public String getDspYn() {
		return this.dspYn;
	}

	public void setDspYn(String dspYn) {
		this.dspYn = dspYn;
	}

	public String getRegDt() {
		return this.regDt;
	}

	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}

	public String getUptDt() {
		return this.uptDt;
	}

	public void setUptDt(String uptDt) {
		this.uptDt = uptDt;
	}

	public int getBbsKey() {
		return this.bbsKey;
	}

	public void setBbsKey(int bbsKey) {
		this.bbsKey = bbsKey;
	}
}