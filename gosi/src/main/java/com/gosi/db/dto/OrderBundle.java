package com.gosi.db.dto;

public class OrderBundle {
	
	private int id;
	private String MEM_YN;
	private String userId;
	private String name;
	private String addr1;
	private String addr2;
	private String zipCd;
	private String tel;
	private String email;
	private String jNo;
	private String cartKeyList="";
	private int price=0;
	private int deliveryFee=3000; //3000? 2500? 
	private int deliveryPriceSum = 0;
	private int totalPrice=0;
	private String orderDt;
	private String payType;
	private String depositYn;
	private String depositDt;
	private String refundReason;
	private String deliveryNm;
	private String deliveryAddr1;
	private String deliveryAddr2;
	private String deliveryZipCd;
	private String deliveryTel2;
	private String deliveryMemo;
	private String deliveryStartYn;
	private String deliveryEndYn;
	private String deliveryNo;
	private String deliveryRegDt;
	private String deliveryUptDt;
	private String status;
	private String regDt;
	private String uptDt;
	
	private int bookCate;
	private String deliDate;
	
	int checkCount = 0;
	
	
	
	public String getDeliDate() {
		return deliDate;
	}
	public void setDeliDate(String deliDate) {
		this.deliDate = deliDate;
	}
	public int getDeliveryPriceSum() {
		return deliveryPriceSum;
	}
	public void setDeliveryPriceSum(int deliveryPriceSum) {
		this.deliveryPriceSum = deliveryPriceSum;
	}
	public int getBookCate() {
		return bookCate;
	}
	public void setBookCate(int bookCate) {
		this.bookCate = bookCate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMEM_YN() {
		return MEM_YN;
	}
	public void setMEM_YN(String mEM_YN) {
		MEM_YN = mEM_YN;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	public String getAddr2() {
		return addr2;
	}
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	public String getZipCd() {
		return zipCd;
	}
	public void setZipCd(String zipCd) {
		this.zipCd = zipCd;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getjNo() {
		return jNo;
	}
	public void setjNo(String jNo) {
		this.jNo = jNo;
	}
	public String getCartKeyList() {
		return cartKeyList;
	}
	public void setCartKeyList(String cartKeyList) {
		if(this.cartKeyList.equals(""))
			this.cartKeyList = cartKeyList;
		else
			this.cartKeyList = this.cartKeyList.concat(",").concat(cartKeyList);
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		
		if(this.bookCate !=0 && this.bookCate !=100) {
			this.deliveryPriceSum = this.deliveryPriceSum + price;
			checkCount++;
		}
		this.price = this.price + price;
		
		if(this.deliveryPriceSum >= 30000)
			this.deliveryFee=0;
		
		if(checkCount==0)
			this.deliveryFee=0;
		else {
			if(this.deliveryPriceSum >= 30000)
				this.deliveryFee=0;
			else
				this.deliveryFee=3000;
		}
		this.totalPrice = this.price + this.deliveryFee;
	}
	public int getDeliveryFee() {
		return deliveryFee;
	}
	public void setDeliveryFee(int deliveryFee) {
		this.deliveryFee = deliveryFee;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getOrderDt() {
		return orderDt;
	}
	public void setOrderDt(String orderDt) {
		this.orderDt = orderDt;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		if(payType.equals("card")) {
			this.payType = "신용카드";
			this.status = "결제완료";
		}
		else {
			this.payType = "무통장입금";
			this.status = "입금대기";
		}
	}
	public String getDepositYn() {
		return depositYn;
	}
	public void setDepositYn(String depositYn) {
		this.depositYn = depositYn;
	}
	public String getDepositDt() {
		return depositDt;
	}
	public void setDepositDt(String depositDt) {
		this.depositDt = depositDt;
	}
	public String getRefundReason() {
		return refundReason;
	}
	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}
	public String getDeliveryNm() {
		return deliveryNm;
	}
	public void setDeliveryNm(String deliveryNm) {
		this.deliveryNm = deliveryNm;
	}
	public String getDeliveryAddr1() {
		return deliveryAddr1;
	}
	public void setDeliveryAddr1(String deliveryAddr1) {
		this.deliveryAddr1 = deliveryAddr1;
	}
	public String getDeliveryAddr2() {
		return deliveryAddr2;
	}
	public void setDeliveryAddr2(String deliveryAddr2) {
		this.deliveryAddr2 = deliveryAddr2;
	}
	public String getDeliveryZipCd() {
		return deliveryZipCd;
	}
	public void setDeliveryZipCd(String deliveryZipCd) {
		this.deliveryZipCd = deliveryZipCd;
	}
	public String getDeliveryTel2() {
		return deliveryTel2;
	}
	public void setDeliveryTel2(String deliveryTel2) {
		this.deliveryTel2 = deliveryTel2;
	}
	public String getDeliveryMemo() {
		return deliveryMemo;
	}
	public void setDeliveryMemo(String deliveryMemo) {
		this.deliveryMemo = deliveryMemo;
	}
	public String getDeliveryStartYn() {
		return deliveryStartYn;
	}
	public void setDeliveryStartYn(String deliveryStartYn) {
		this.deliveryStartYn = deliveryStartYn;
	}
	public String getDeliveryEndYn() {
		return deliveryEndYn;
	}
	public void setDeliveryEndYn(String deliveryEndYn) {
		this.deliveryEndYn = deliveryEndYn;
	}
	public String getDeliveryNo() {
		return deliveryNo;
	}
	public void setDeliveryNo(String deliveryNo) {
		this.deliveryNo = deliveryNo;
	}
	public String getDeliveryRegDt() {
		return deliveryRegDt;
	}
	public void setDeliveryRegDt(String deliveryRegDt) {
		this.deliveryRegDt = deliveryRegDt;
	}
	public String getDeliveryUptDt() {
		return deliveryUptDt;
	}
	public void setDeliveryUptDt(String deliveryUptDt) {
		this.deliveryUptDt = deliveryUptDt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getUptDt() {
		return uptDt;
	}
	public void setUptDt(String uptDt) {
		this.uptDt = uptDt;
	}
	
	
}