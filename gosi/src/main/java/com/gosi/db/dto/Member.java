package com.gosi.db.dto;

public class Member {
	private int id;
	private String memId;
	private String passwd;
	private String passHash;
	private String name;
	private String adminYn;
	private String addr1;
	private String addr2;
	private String zipCd;
	private String tel;
	private String email;
	private String outYn;
	private String outDt;
	private String regDt;
	private String uptDt;
	private String memYn;
	

	
	public String getMemYn() {
		return memYn;
	}

	public void setMemYn(String memYn) {
		this.memYn = memYn;
	}

	public static Member make(String memId, String passwd, String passHash, String name, String addr1, String addr2,
			String zipCd, String tel, String email) {
		Member mem = new Member();
		mem.setMemId(memId);
		mem.setPasswd(passwd);
		mem.setPassHash(passHash);
		mem.setName(name);
		mem.setAddr1(addr1);
		mem.setAddr2(addr2);
		mem.setZipCd(zipCd);
		mem.setTel(tel);
		mem.setEmail(email);
		return mem;
	}

	public static Member make(String memId, String addr1, String addr2, String zipCd, String tel, String email) {
		Member mem = new Member();
		mem.setMemId(memId);
		mem.setAddr1(addr1);
		mem.setAddr2(addr2);
		mem.setZipCd(zipCd);
		mem.setTel(tel);
		mem.setEmail(email);
		return mem;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMemId() {
		return this.memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public String getPasswd() {
		return this.passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getPassHash() {
		return this.passHash;
	}

	public void setPassHash(String passHash) {
		this.passHash = passHash;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdminYn() {
		return this.adminYn;
	}

	public void setAdminYn(String adminYn) {
		this.adminYn = adminYn;
	}

	public String getAddr1() {
		return this.addr1;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public String getAddr2() {
		return this.addr2;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public String getZipCd() {
		return this.zipCd;
	}

	public void setZipCd(String zipCd) {
		this.zipCd = zipCd;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOutYn() {
		return this.outYn;
	}

	public void setOutYn(String outYn) {
		this.outYn = outYn;
	}

	public String getOutDt() {
		return this.outDt;
	}

	public void setOutDt(String outDt) {
		this.outDt = outDt;
	}

	public String getRegDt() {
		return this.regDt;
	}

	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}

	public String getUptDt() {
		return this.uptDt;
	}

	public void setUptDt(String uptDt) {
		this.uptDt = uptDt;
	}
}