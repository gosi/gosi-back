package com.gosi.db;

import com.gosi.db.dto.Book;
import com.gosi.db.dto.Cart;
import com.gosi.db.dto.Member;
import com.gosi.db.dto.Notice;
import com.gosi.db.dto.Order;
import com.gosi.db.dto.OrderMegazine;
import com.gosi.db.dto.StandingOrder;
import com.gosi.db.dto.WebBook;
import java.util.List;
import java.util.Map;

public interface DbHandler {
	void update(Map<String, Object> map);
	List<Map<String, Object>> getList();
	//자동 프로젝트
	List<Map<String, Object>> getMonitorList();
	void updateMonitorList(String jNo);
	List<Map<String, Object>> getMonitorList2();
	List<Map<String, Object>> getMonitorList3();
	List<Map<String, Object>> getMonitorList4();
	void updateMonitorList2(String jNo);
	void updateMonitorList3(String jNo);
	void updateMonitorList4(String jNo, int cnt, String deliDate);
	
	void updateDeli1(Map<String, Object> map);
	void updateDeli2(Map<String, Object> map);
	void updateDeli3(Map<String, Object> map);
	void updateDeli4(Map<String, Object> map);
	
	//common
	List<Notice> getBbsList(Map<String, Object> var1, int var2);
	List<Book> getBookList(Map<String, Object> var1);
	public Integer getPageNum(Map<String, Object> param);
	List<WebBook> getWebBookList(String var1);
	Notice getDetailInfo(int var1);
	Book getBookDetail(int var1);
	Integer getBbsPageNum(String subId);
	
	//login
	boolean checkMember(Map<String, Object> var1);
	Integer addInfo(Member var1);
	boolean checkId(Map<String, Object> var1);
	Map<String, Object> getHash(String var1);
	String getOnlyHash(String var1);
	String findId(Map<String, Object> var1);
	String findPassWd(Map<String, Object> var1);
	void changePassWd(Map<String, Object> var1);
	String getNonHash(String var1);

	//cart
	List<Map<String, Object>> getCartBookList(String var1);
	List<Map<String, Object>> getCartMagazineList(String var1);
	List<Map<String, Object>> getCartWebList(String var1);
	void addMegazineCart(Cart var1);
	void addCart(Cart var1);
	void deleteCart(int var1);
	void addOrderCart(List<Order> orderList, String id, String pwd, String hash);
	void addOrderCart(List<Order> orderList, String id);
	boolean check(Map<String, Object> map);
	List<Map<String, Object>> getOrderCart(Map<String, Object> map);
	void deleteOrderCart(Map<String, Object> map);
	
	//mypage
	List<Map<String, Object>> getNonmemberOrderList(String var1);
	List<Map<String, Object>> getOrderList(Map<String, Object> var1);
	List<Map<String, Object>> getWebOrderList(String id);
	void changeInfo(Member var1);
	void changePassWd2(Map<String, Object> var1);
	boolean deleteInfo(Map<String, Object> var1);
	Map<String, Object> getMemberInfo(String id);
	void deleteOrder(String jNo);
	boolean checkOrder(String jNo);
	
	//admin
	void addBook(Book var1);
	void deleteBook(int bookId);
	void changeBook(Book var1);
	void addMegazine(Book var1);
	void changeMegazine(Book var1);
	void addWebBook(WebBook var1);
	void changeWebBook(WebBook var1);
	List<Book> getAdminBookList(Map<String, Object> var1);
	List<Book> getMegazineList(Map<String, Object> var1);
	List<WebBook> getWebList(Map<String, Object> var1);
	List<Map<String, Object>> getTypeOneList(Map<String, Object> map);
	List<Map<String, Object>> getTypeTwoList(Map<String, Object> map);
	List<Map<String, Object>> getTypeThreeList(Map<String, Object> map);
	List<Map<String, Object>> getTypeFourList(Map<String, Object> map);
	Map<String, Object> getUpdateInfo(String jNo);
	void updateDeliveryInfo(Map<String, Object> map);
	void updatePaymentInfo(Map<String, Object> map);
	List<Member> getNOMember(String name);
	List<Member> getMember(String name);
	List<Member> getAllMember(String name);
	void updateMemberInfo(Member member);
	Map<String, Object> getManageInfo();
	List<Map<String, Object>> getSubOrderList(String jNo);
	
	//order
	Member getUserInfo(String var1);
	void setStandingOrder(StandingOrder order);
	void setStandingOrderNoMember(StandingOrder order);
	void setOrderNoMember(List<Order> orderList);
	void setOrder(List<Order> orderList);
	Map<String, Object> sendMegazineInfo(Map<String, Object> map);
	void updateOrder(Map<String, Object> param);
	void insertCartNoMem(Map<String, Object> param);
	void insertCartMem(Map<String, Object> param);
	Map<String, Object> getOrderBundleInfo(String jNo);
	List<Map<String, Object>> getOrderPriInfo(String jNo);
	Map<String, Object> getPayInfo(String id);
	
	
	
	void setBook(Map<String, Object> map);
	void setBook2(Map<String, Object> map);
	void setBook3(Map<String, Object> map);
	void addNewBook(Map<String, Object> map);
	
}