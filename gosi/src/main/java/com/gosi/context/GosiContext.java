package com.gosi.context;

import ch.qos.logback.classic.LoggerContext;
import com.gosi.db.DbHandler;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class GosiContext implements ApplicationContextAware {
	private static ApplicationContext CONTEXT;
	private String logDir;
	@Resource
	DbHandler dbHandler;
	@Resource
	private GosiContext context;

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		CONTEXT = context;
	}

	@PostConstruct
	public void init() {
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
		this.logDir = context.getProperty("app.log.dir");
	}

	public String getLogDir() {
		return this.logDir;
	}

	public void setLogDir(String logDir) {
		this.logDir = logDir;
	}

	private void saveServerInfo(String serverName, String strServerInfo, String ip, int port) {
	}
}