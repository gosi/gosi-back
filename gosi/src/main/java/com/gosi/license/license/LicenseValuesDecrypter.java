package com.gosi.license.license;

import com.gosi.exception.PException;
import com.gosi.license.encrypter.AESEncrypter;
import com.gosi.license.encrypter.Encrypter;

public class LicenseValuesDecrypter {
	private Encrypter encrypter;

	public LicenseValuesDecrypter(String salt) throws PException {
		try {
			this.encrypter = new AESEncrypter(salt);
		} catch (Exception var3) {
			throw PException.buildException((short) 8001, var3);
		}
	}

	public String decrypt(String encryptString) throws PException {
		String decryptedString = "";

		try {
			decryptedString = this.encrypter.decrypt(encryptString);
			return decryptedString;
		} catch (Exception var4) {
			throw PException.buildException((short) 8003, var4);
		}
	}
}