package com.gosi.license.license;

import com.gosi.exception.PException;
import com.gosi.license.encode.Encode;
import com.gosi.license.encode.EncodeFactory;
import com.gosi.license.encode.EncodeAlgorithm;
import com.gosi.license.encrypter.AESPassword;
import com.gosi.utils.date.DateUtils;
import java.util.Date;

public class LicenseGenerator {
	private static String seperator = "||";
	private static String deseperator = "\\|\\|";

	public static String generate(String licenseKey, String serviceCode, String password, String deviceCnt,
			String expiredDate, String sendingCntPerMin, String salt) throws PException {
		LicenseValuesEncrypter encrypt = null;

		try {
			encrypt = new LicenseValuesEncrypter(AESPassword.getPassword());
		} catch (Exception var19) {
			throw PException.buildException((short) 8000, var19);
		}

		Encode encode = EncodeFactory.getEncode(EncodeAlgorithm.SHA256);
		licenseKey = licenseKey.replaceAll("-", "");
		String encryptedLicense = encode.encode(licenseKey, salt);
		String encrytedServiceCode = encode.encode(serviceCode);
		String authToken = encode.encode(password);
		String encrytedDeviceCnt = encode.encode(deviceCnt);
		String encrytedExpiredDate = encode.encode(expiredDate.replaceAll("-", "").replaceAll("/", ""));
		String encrytedSendingCntPerMin = encode.encode(sendingCntPerMin);
		StringBuilder sb = new StringBuilder();
		sb.append(encrytedServiceCode).append(seperator);
		sb.append(authToken).append(seperator);
		sb.append(encrytedDeviceCnt).append(seperator);
		sb.append(encrytedSendingCntPerMin).append(seperator);
		sb.append(encrytedExpiredDate);
		encode = EncodeFactory.getEncode(EncodeAlgorithm.SHA512);
		String authValues = encode.encode(sb.toString(), licenseKey);
		sb.delete(0, sb.length());

		try {
			sb.append(encryptedLicense).append(seperator).append(authValues).append(seperator)
					.append(encrypt.encrypt(serviceCode)).append(seperator).append(encrypt.encrypt(password))
					.append(seperator).append(encrypt.encrypt(deviceCnt)).append(seperator)
					.append(encrypt.encrypt(sendingCntPerMin)).append(seperator)
					.append(encrypt.encrypt(expiredDate.replaceAll("-", "").replaceAll("/", "")));
		} catch (Exception var18) {
			var18.printStackTrace();
		}

		authValues = sb.toString();
		return authValues;
	}

	public static LicenseValues getLicenseValuesInfo(LicenseValuesDecrypter decrypter, String license,
			String licenseKey, String enSalt) throws PException {
		String salt;
		try {
			salt = decrypter.decrypt(enSalt);
		} catch (Exception var22) {
			throw PException.buildException((short) 8004, var22);
		}

		Encode encode = EncodeFactory.getEncode(EncodeAlgorithm.SHA256);
		licenseKey = licenseKey.replaceAll("-", "");
		String[] authValues = license.split(deseperator);
		if (authValues.length == 7) {
			String plainServiceCode = "";
			String plainPassword = "";
			String plainDeviceCnt = "";
			String plainSendingCntPerMin = "";
			String plainExpiredDate = "";
			String encryptedLicense = "";
			String encrytedServiceCode = "";
			String authToken = "";
			String encrytedDeviceCnt = "";
			String encrytedSendingCntPerMin = "";
			String encrytedExpiredDate = "";

			try {
				plainServiceCode = decrypter.decrypt(authValues[2]);
				plainPassword = decrypter.decrypt(authValues[3]);
				plainDeviceCnt = decrypter.decrypt(authValues[4]);
				plainSendingCntPerMin = decrypter.decrypt(authValues[5]);
				plainExpiredDate = decrypter.decrypt(authValues[6]);
				encryptedLicense = encode.encode(licenseKey, salt);
				encrytedServiceCode = encode.encode(plainServiceCode);
				authToken = encode.encode(plainPassword);
				encrytedDeviceCnt = encode.encode(plainDeviceCnt);
				encrytedSendingCntPerMin = encode.encode(plainSendingCntPerMin);
				encrytedExpiredDate = encode.encode(plainExpiredDate);
			} catch (Exception var21) {
				throw PException.buildException((short) 8005, var21);
			}

			StringBuilder sb = new StringBuilder();
			sb.append(encrytedServiceCode).append(seperator);
			sb.append(authToken).append(seperator);
			sb.append(encrytedDeviceCnt).append(seperator);
			sb.append(encrytedSendingCntPerMin).append(seperator);
			sb.append(encrytedExpiredDate);
			encode = EncodeFactory.getEncode(EncodeAlgorithm.SHA512);
			String authData = encode.encode(sb.toString(), licenseKey);
			LicenseValues authObj = null;
			if (authValues[0].equals(encryptedLicense) && authValues[1].equals(authData)) {
				authObj = getAuthValue(licenseKey, plainServiceCode, plainPassword, authToken, plainDeviceCnt,
						plainSendingCntPerMin, plainExpiredDate, license);
				return authObj;
			} else {
				throw PException.buildException((short) 8005, "License Infomation for EUMS's service is wrong");
			}
		} else {
			throw PException.buildException((short) 8005, "License Infomation for EUMS's service is wrong");
		}
	}

	public static boolean isValid(LicenseValuesDecrypter decrypter, String license, String licenseKey)
			throws PException {
		try {
			LicenseValues auth = getLicenseValuesInfo(decrypter, license, licenseKey, "voFyLL/KUDNmAL8YiqO0Ig==");
			if (!DateUtils.isCompareToDate(new Date(), DateUtils.parseDate(auth.getExpiredDate(), "yyyyMMdd"))) {
				return false;
			} else if (auth.getMaxDeviceCnt() <= 0L) {
				return false;
			} else if (!auth.getLicenseKey().equals(licenseKey)) {
				return false;
			} else {
				return auth.getSendingCntPerMin() > 0L;
			}
		} catch (Exception var4) {
			var4.printStackTrace();
			return false;
		}
	}

	public static LicenseValues getAuthValue(String licenseKey, String serviceCode, String password, String authToken,
			String deviceCnt, String sendingCntPerMin, String expireDate) throws PException {
		return new LicenseValues(licenseKey, serviceCode, password, authToken, deviceCnt, sendingCntPerMin, expireDate);
	}

	public static LicenseValues getAuthValue(String licenseKey, String serviceCode, String password, String authToken,
			String deviceCnt, String sendingCntPerMin, String expireDate, String encryptedAuth) throws PException {
		return new LicenseValues(licenseKey, serviceCode, password, authToken, deviceCnt, sendingCntPerMin, expireDate,
				encryptedAuth);
	}
}