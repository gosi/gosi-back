package com.gosi.license.license;

import com.gosi.exception.PException;
import com.gosi.license.encrypter.AESEncrypter;
import com.gosi.license.encrypter.Encrypter;

public class LicenseValuesEncrypter {
	private Encrypter encrypter;

	public LicenseValuesEncrypter(String salt) throws PException {
		try {
			this.encrypter = new AESEncrypter(salt);
		} catch (Exception var3) {
			throw PException.buildException((short) 8001, var3);
		}
	}

	public String encrypt(String data) throws PException {
		String encryptedString = "";

		try {
			encryptedString = this.encrypter.encrypt(data);
			return encryptedString;
		} catch (Exception var4) {
			throw PException.buildException((short) 8002, var4);
		}
	}
}