package com.gosi.license.license;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

public class LicenseValueTest implements Serializable {
	private static final long serialVersionUID = 7706202896230704762L;
	private String licenseKey;
	private String serviceName;
	private String password;
	private String enPassword;
	private long maxDeviceCnt;
	private long connectedDeviceCnt;
	private String expiredDate;
	private long sendingCntPerMin;
	private AtomicLong sentCntPerMin = new AtomicLong(0L);
	private String encryptedAuth;

	public String getLicenseKey() {
		return this.licenseKey;
	}

	public void setLicenseKey(String licenseKey) {
		this.licenseKey = licenseKey;
	}

	public long getMaxDeviceCnt() {
		return this.maxDeviceCnt;
	}

	public void setMaxDeviceCnt(long deviceCnt) {
		this.maxDeviceCnt = deviceCnt;
	}

	public String getExpiredDate() {
		return this.expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEnPassword() {
		return this.enPassword;
	}

	public void setEnPassword(String enPassword) {
		this.enPassword = enPassword;
	}

	public long getSendingCntPerMin() {
		return this.sendingCntPerMin;
	}

	public void setSendingCntPerMin(long sendingCntPerMin) {
		this.sendingCntPerMin = sendingCntPerMin;
	}

	public String getEncryptedAuth() {
		return this.encryptedAuth;
	}

	public void setEncryptedAuth(String encryptedAuth) {
		this.encryptedAuth = encryptedAuth;
	}

	public void increaseSentCntPerMin() {
		this.sentCntPerMin.incrementAndGet();
	}

	public void addSentCntPerMin(int cnt) {
		this.sentCntPerMin.addAndGet((long) cnt);
	}

	public void initSentCntPerMin() {
		this.sentCntPerMin.set(0L);
	}

	public long getSentCntPerMin() {
		return this.sentCntPerMin.get();
	}

	public long getConnectedDeviceCnt() {
		return this.connectedDeviceCnt;
	}

	public void setConnectedDeviceCnt(long connectedDeviceCnt) {
		this.connectedDeviceCnt = connectedDeviceCnt;
	}

	public void addAddConnectedDeviceCnt(int cnt) {
		this.connectedDeviceCnt += (long) cnt;
	}

	public boolean valid() {
		return this.sentCntPerMin.get() < this.sendingCntPerMin && this.connectedDeviceCnt < this.maxDeviceCnt;
	}

	public boolean validInDeviceCnt() {
		return this.connectedDeviceCnt < this.maxDeviceCnt;
	}

	public boolean validInSendingMessage() {
		return this.sentCntPerMin.get() < this.sendingCntPerMin;
	}
}