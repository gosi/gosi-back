package com.gosi.license.license;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

public class LicenseValues implements Cloneable, Serializable {
	private static final long serialVersionUID = 7706202896230704762L;
	private String licenseKey;
	private String license;
	private String serviceCode;
	private String password;
	private String authToken;
	private long maxDeviceCnt;
	private long connectedDeviceCnt;
	private String expiredDate;
	private long sendingCntPerMin;
	private AtomicLong sentCntPerMin = new AtomicLong(0L);
	private boolean isDeleted = false;

	public LicenseValues() {
	}

	public LicenseValues(String licenseKey, String serviceCode, String password, String authToken, String deviceCnt,
			String sendingCntPerMin, String expiredDate) {
		this.licenseKey = licenseKey;
		this.serviceCode = serviceCode;
		this.password = password;
		this.authToken = authToken;
		this.maxDeviceCnt = (long) Integer.parseInt(deviceCnt);
		this.sendingCntPerMin = (long) Integer.parseInt(sendingCntPerMin);
		this.expiredDate = expiredDate;
	}

	public LicenseValues(String licenseKey, String serviceCode, String password, String authToken, String deviceCnt,
			String sendingCntPerMin, String expiredDate, String license) {
		this.licenseKey = licenseKey;
		this.serviceCode = serviceCode;
		this.password = password;
		this.authToken = authToken;
		this.maxDeviceCnt = (long) Integer.parseInt(deviceCnt);
		this.sendingCntPerMin = (long) Integer.parseInt(sendingCntPerMin);
		this.expiredDate = expiredDate;
		this.license = license;
	}

	public String getLicenseKey() {
		return this.licenseKey;
	}

	public void setLicenseKey(String licenseKey) {
		this.licenseKey = licenseKey;
	}

	public long getMaxDeviceCnt() {
		return this.maxDeviceCnt;
	}

	public void setMaxDeviceCnt(long deviceCnt) {
		this.maxDeviceCnt = deviceCnt;
	}

	public String getExpiredDate() {
		return this.expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getServiceCode() {
		return this.serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuthToken() {
		return this.authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public long getSendingCntPerMin() {
		return this.sendingCntPerMin;
	}

	public void setSendingCntPerMin(long sendingCntPerMin) {
		this.sendingCntPerMin = sendingCntPerMin;
	}

	public String getLicense() {
		return this.license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public void increaseSentCntPerMin() {
		this.sentCntPerMin.incrementAndGet();
	}

	public void addSentCntPerMin(int cnt) {
		this.sentCntPerMin.addAndGet((long) cnt);
	}

	public void initSentCntPerMin() {
		this.sentCntPerMin.set(0L);
	}

	public long getSentCntPerMin() {
		return this.sentCntPerMin.get();
	}

	public long getConnectedDeviceCnt() {
		return this.connectedDeviceCnt;
	}

	public void setConnectedDeviceCnt(long connectedDeviceCnt) {
		this.connectedDeviceCnt = connectedDeviceCnt;
	}

	public void addAddConnectedDeviceCnt(int cnt) {
		this.connectedDeviceCnt += (long) cnt;
	}

	public boolean valid(long sentCntPerMinFromOtherServer) {
		return this.sentCntPerMin.get() + sentCntPerMinFromOtherServer < this.sendingCntPerMin
				&& this.connectedDeviceCnt < this.maxDeviceCnt;
	}

	public boolean validInDeviceCnt() {
		return this.connectedDeviceCnt < this.maxDeviceCnt;
	}

	public boolean validInSendingMessage() {
		return this.sentCntPerMin.get() < this.sendingCntPerMin;
	}

	public boolean isDeleted() {
		return this.isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}