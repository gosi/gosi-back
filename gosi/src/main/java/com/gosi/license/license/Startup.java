package com.gosi.license.license;

import com.gosi.exception.PException;
import com.gosi.license.encrypter.AESEncrypter;
import com.gosi.license.encrypter.AESPassword;
import com.gosi.utils.date.DateUtils;
import com.gosi.utils.file.FileUtils;
import java.io.Console;
import java.util.HashMap;
import java.util.Map;

public class Startup {
	public static void main(String[] args) {
		String licenseKey = "";
		String serviceCode = "";
		String deviceCnt = "";
		String expiredDate = "";
		String genDate = "";
		String sendingCntPerMin = "";
		String salt = "EPOPCON!!";
		HashMap serviceCodeMap = new HashMap();

		try {
			Console console = System.console();

			while (!validateLicensekey(licenseKey, serviceCodeMap)) {
				if (licenseKey.equals("")) {
					licenseKey = console
							.readLine("Please enter a license key which is provided by EMUS Server Admin : ");
				} else {
					licenseKey = console
							.readLine("Please enter a license key which is provided by push server admin : ");
				}
			}

			serviceCode = (String) serviceCodeMap.get("serviceCode");

			while (!validateDate(genDate, "yyyyMMdd")) {
				if (genDate.equals("")) {
					genDate = console.readLine(
							"Please enter an date of license of EMUS System published if a license have, (ex:20130101): ");
				} else {
					genDate = console.readLine(
							"Please enter an date of license of EMUS System published if a license have, (ex:20130101): ");
				}

				if (genDate.equals("")) {
					genDate = DateUtils.getCurrentDate();
				}
			}

			String authToken = generateAuthToken((String) serviceCodeMap.get("serviceCode"),
					(String) serviceCodeMap.get("serviceId"), genDate);

			while (!validateNumber(deviceCnt)) {
				if (deviceCnt.equals("")) {
					deviceCnt = console.readLine("Please enter a total number of device using a EMUS System : ");
				} else {
					deviceCnt = console.readLine("Please enter a total number of device using a EMUS System : ");
				}
			}

			while (!validateNumber(sendingCntPerMin)) {
				if (sendingCntPerMin.equals("")) {
					sendingCntPerMin = console
							.readLine("Please enter a count of sending message per a minute in the EMUS System: ");
				} else {
					sendingCntPerMin = console
							.readLine("Please enter a count of sending message per a minute in the EMUS System: ");
				}
			}

			while (!validateDate(expiredDate, "yyyy-MM-dd")) {
				if (expiredDate.equals("")) {
					expiredDate = console
							.readLine("Please enter an expired date of license of EMUS System , (ex:2013-01-01): ");
				} else {
					expiredDate = console
							.readLine("Please enter an expired date of license of EMUS System , (ex:2013-01-01): ");
				}
			}

			String license = LicenseGenerator.generate(licenseKey, serviceCode, authToken, deviceCnt,
					expiredDate.replaceAll("-", ""), sendingCntPerMin, salt);
			String fileAbsPath = FileUtils.getFullPathFromAbsFile("./license/license.eums");
			System.out.println("license file path ::" + fileAbsPath);
			FileUtils.makeNewFile(license, fileAbsPath);
		} catch (Exception var13) {
			PException.buildException((short) 8006, var13);
		}

	}

	private static boolean validateNumber(String number) {
		try {
			Integer.parseInt(number);
			return true;
		} catch (Exception var2) {
			return false;
		}
	}

	public static String generateAuthToken(String serviceCode, String serviceId, String genDate) throws Exception {
		new AESEncrypter(AESPassword.getPassword());
		StringBuffer sb = new StringBuffer(serviceCode);
		sb.append("||").append(genDate).append("||").append(serviceId);
		return sb.toString();
	}

	public static String generateAuthToken(String serviceCode, String serviceId) throws Exception {
		new AESEncrypter(AESPassword.getPassword());
		StringBuffer sb = new StringBuffer(serviceCode);
		sb.append("||").append(DateUtils.getCurrentDate()).append("||").append(serviceId);
		return sb.toString();
	}

	private static boolean validateDate(String date, String strFormat) {
		try {
			return DateUtils.isDate(date, strFormat);
		} catch (Exception var3) {
			return false;
		}
	}

	public static boolean validateLicensekey(String key, Map<String, String> serviceCode) {
		try {
			AESEncrypter encrypter = new AESEncrypter(AESPassword.getPassword());
			String serviceInfo = encrypter.decrypt(key);
			String[] serviceCodes = serviceInfo.split("\\|\\|");
			serviceCode.put("serviceCode", serviceCodes[0]);
			serviceCode.put("serviceId", serviceCodes[1]);
			if (serviceCodes.length != 2) {
				return false;
			} else {
				return !"".equals(key) && key != null;
			}
		} catch (Exception var5) {
			return false;
		}
	}

	private static boolean validateServiceName(String serviceName) {
		try {
			if (!"".equals(serviceName) && serviceName != null) {
				return serviceName.length() <= 20;
			} else {
				return false;
			}
		} catch (Exception var2) {
			return false;
		}
	}
}