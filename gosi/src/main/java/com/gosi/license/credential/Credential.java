package com.gosi.license.credential;

import com.gosi.license.encode.Encode;
import com.gosi.license.encode.EncodeFactory;
import com.gosi.license.encode.EncodeAlgorithm;
import com.gosi.license.encrypter.AESEncrypter;
import com.gosi.license.encrypter.AESPassword;
import com.gosi.license.encrypter.Encrypter;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;

@Component
public class Credential {
	Encode en;
	Encrypter encrypter;

	@PostConstruct
	public void init() {
		this.en = EncodeFactory.getEncode(EncodeAlgorithm.SHA512);

		try {
			this.encrypter = new AESEncrypter(AESPassword.getPassword());
		} catch (Exception var2) {
			var2.printStackTrace();
		}

	}

	public String toCredential(String e, String k, String r) {
		k = k.replaceAll(r, "");
		return this.en.encode(e, k);
	}

	public String encrypt(String key) {
		String enKey = "";

		try {
			enKey = this.encrypter.encrypt(key);
		} catch (Exception var4) {
			var4.printStackTrace();
			enKey = key;
		}

		return enKey;
	}

	public String decrypt(String key) {
		String plain = "";

		try {
			plain = this.encrypter.decrypt(key);
		} catch (Exception var4) {
			var4.printStackTrace();
		}

		return plain;
	}
}