package com.gosi.license.encrypter;

public interface Encrypter {
	String encrypt(String var1) throws Exception;

	byte[] encrypt(byte[] var1) throws Exception;

	String decrypt(String var1) throws Exception;

	byte[] decrypt(byte[] var1) throws Exception;
}