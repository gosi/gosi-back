package com.gosi.license.encrypter;

import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public class AESEncrypter implements Encrypter {
	private static final byte[] SALT = new byte[]{69, 80, 79, 80, 67, 79, 78, 33, 33};
	private static final int ITERATION_COUNT = 65536;
	private static final int KEY_LENGTH = 128;
	private Cipher ecipher;
	private Cipher dcipher;

	public AESEncrypter(String passPhrase) throws Exception {
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(passPhrase.toCharArray(), SALT, 65536, 128);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		this.ecipher = Cipher.getInstance("AES");
		this.ecipher.init(1, secret);
		this.dcipher = Cipher.getInstance("AES");
		this.dcipher.init(2, secret);
	}

	public String encrypt(String encrypt) throws Exception {
		byte[] bytes = encrypt.getBytes("UTF8");
		byte[] encrypted = this.encrypt(bytes);
		return Base64.encodeBase64String(encrypted);
	}

	public byte[] encrypt(byte[] plain) throws Exception {
		return this.ecipher.doFinal(plain);
	}

	public String decrypt(String encrypt) throws Exception {
		byte[] bytes = Base64.decodeBase64(encrypt);
		byte[] decrypted = this.decrypt(bytes);
		return new String(decrypted, "UTF8");
	}

	public byte[] decrypt(byte[] encrypt) throws Exception {
		return this.dcipher.doFinal(encrypt);
	}
}