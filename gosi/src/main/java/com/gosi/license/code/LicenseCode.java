package com.gosi.license.code;

public class LicenseCode extends MessageCode {
	public static final short LICENSE_AES_ENCRYPT = 8000;
	public static final short AUTH_AES_SALT = 8001;
	public static final short AUTH_AES_ENCRYPT = 8002;
	public static final short AUTH_AES_DECRYPT = 8003;
	public static final short AUTH_SALT_DECRYPT = 8004;
	public static final short AUTH_DECRYPT = 8005;
	public static final short LICENSE_PROVIDE = 8006;
}