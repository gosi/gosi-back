package com.gosi.license.key;

import com.gosi.license.encrypter.AESEncrypter;
import com.gosi.license.encrypter.AESPassword;
import com.gosi.license.encrypter.Encrypter;

public class LicenseKeyGenerator {
	public static String generate(String serviceCode, String serviceId) throws Exception {
		String licenseKey = "";
		Encrypter encrypter = new AESEncrypter(AESPassword.getPassword());
		StringBuffer sb = (new StringBuffer(serviceCode)).append("||").append(serviceId);
		licenseKey = encrypter.encrypt(sb.toString());
		return licenseKey;
	}
}