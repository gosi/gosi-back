package com.gosi.license.generate;

import com.gosi.utils.date.DateUtils;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class GosiPriPubKey {
	String epopconId;
	String privateKey;
	String publicKey;
	Date uptDt;
	int refreshGap = 5;

	public String getPrivateKey() {
		return this.privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getPublicKey() {
		return this.publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getUptDt() {
		return DateUtils.formatDate(this.uptDt, "yyyy-MM-dd HH:mm:ss");
	}

	public void setUptDt(Date uptDt) {
		this.uptDt = uptDt;
	}

	public long getPassedPeriod() {
		return DateUtils.getDateDiff(this.uptDt, DateUtils.rightNow(), TimeUnit.MINUTES);
	}

	public String getEpopconId() {
		return this.epopconId;
	}

	public void setEpopconId(String epopconId) {
		this.epopconId = epopconId;
	}

	public int getRefreshGap() {
		return this.refreshGap;
	}

	public void setRefreshGap(int refreshGap) {
		this.refreshGap = refreshGap;
	}

	public static GosiPriPubKey make(String epopconId, int refreshGap) {
		GosiPriPubKey tt = new GosiPriPubKey();
		tt.setEpopconId(epopconId);
		tt.setUptDt(DateUtils.rightNow());
		tt.setRefreshGap(refreshGap);
		return tt;
	}

	public boolean isRefreshRequired() {
		if (this.uptDt == null) {
			return true;
		} else {
			long diffMinute = this.getPassedPeriod();
			return diffMinute >= (long) this.refreshGap;
		}
	}

	public String toString() {
		return "EpopKey [privateKey=" + this.privateKey + ", publicKey=" + this.publicKey + ", uptDt=" + this.getUptDt()
				+ "]";
	}
}