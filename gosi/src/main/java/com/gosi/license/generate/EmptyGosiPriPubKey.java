package com.gosi.license.generate;

public class EmptyGosiPriPubKey extends GosiPriPubKey {
	public static EmptyGosiPriPubKey make(String epopconId) {
		EmptyGosiPriPubKey tt = new EmptyGosiPriPubKey();
		tt.setEpopconId(epopconId);
		tt.setPrivateKey("");
		tt.setPublicKey("");
		return tt;
	}
}