package com.gosi.license.generate;

public class GosiPriPubKeyFactory {
	private static GosiPriPubKeyFactory instance;
	int refreshGap = 5;

	private GosiPriPubKeyFactory(int refreshGap) {
		this.refreshGap = refreshGap;
	}

	public static synchronized GosiPriPubKeyFactory getInstance(int refreshGap) {
		if (instance == null) {
			instance = new GosiPriPubKeyFactory(refreshGap);
		}

		return instance;
	}

	public GosiPriPubKey make(String epopconId) {
		GosiPriPubKey tt = GosiPriPubKey.make(epopconId, this.refreshGap);
		return tt;
	}
}