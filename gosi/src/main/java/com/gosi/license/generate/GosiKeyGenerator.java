package com.gosi.license.generate;

import java.util.concurrent.ThreadLocalRandom;

public class GosiKeyGenerator implements KeyGenerator {
	public String genPublicKey(String epopconId) {
		Long randdom = ThreadLocalRandom.current().nextLong(10000000000000L, 99999999999999L);
		StringBuffer sb = (new StringBuffer("pc-")).append(epopconId);
		sb.append("-");
		sb.append(randdom);
		return sb.toString();
	}

	public String genPrivateKey(String epopconId) {
		Long randdom = ThreadLocalRandom.current().nextLong(10000000000000L, 99999999999999L);
		StringBuffer sb = (new StringBuffer("pk-")).append(epopconId);
		sb.append("-");
		sb.append(randdom);
		return sb.toString();
	}
}