package com.gosi.license.generate;

public interface KeyGenerator {
	String genPublicKey(String var1);

	String genPrivateKey(String var1);
}