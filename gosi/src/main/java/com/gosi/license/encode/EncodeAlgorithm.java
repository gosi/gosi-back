package com.gosi.license.encode;

public enum EncodeAlgorithm {
	SHA256, SHA512, MD5;
}