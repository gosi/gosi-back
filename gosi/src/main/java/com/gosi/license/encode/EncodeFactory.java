package com.gosi.license.encode;

import com.gosi.license.encode.EncodeAlgorithm;

public class EncodeFactory {
	public static Encode getEncode(EncodeAlgorithm algorithm) {
		if (algorithm.equals(EncodeAlgorithm.SHA256)) {
			return makeSha256();
		} else if (algorithm.equals(EncodeAlgorithm.SHA512)) {
			return makeSha512();
		} else {
			return algorithm.equals(EncodeAlgorithm.MD5) ? makeMD5() : null;
		}
	}

	private static Encode makeSha256() {
		return new Sha256();
	}

	private static Encode makeSha512() {
		return new Sha512();
	}

	private static Encode makeMD5() {
		return new MD5();
	}
}