package com.gosi.license.encode;

public interface Encode {
	String encode(String var1);

	String encode(String var1, String var2);
}