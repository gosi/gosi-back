package com.gosi.license.encode;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

public class Sha256 implements Encode {
	ShaPasswordEncoder passEncoder = new ShaPasswordEncoder(256);

	public String encode(String data) {
		return this.passEncoder.encodePassword(data, (Object) null);
	}

	public String encode(String data, String salt) {
		return this.passEncoder.encodePassword(data, salt);
	}
}