package com.gosi.license.encode;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

public class Sha512 implements Encode {
	ShaPasswordEncoder passEncoder = new ShaPasswordEncoder(512);

	public String encode(String data) {
		return this.passEncoder.encodePassword(data, (Object) null);
	}

	public String encode(String data, String salt) {
		return this.passEncoder.encodePassword(data, salt);
	}
}