package com.gosi.license.encode;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 implements Encode {
	public String encode(String pass) {
		String md5 = "";

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(pass.getBytes());
			byte[] temp = md.digest();
			StringBuffer sb = new StringBuffer();

			for (int i = 0; i < temp.length; ++i) {
				sb.append(Integer.toString((temp[i] & 255) + 256, 16).substring(1));
			}

			md5 = sb.toString();
		} catch (NoSuchAlgorithmException var7) {
			var7.printStackTrace();
		}

		return md5.toString();
	}

	public String encode(String pass, String salt) {
		return this.encode(pass);
	}
}