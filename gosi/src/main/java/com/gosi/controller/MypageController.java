package com.gosi.controller;

import com.gosi.db.DbHandler;
import com.gosi.db.dto.Member;
import com.gosi.exception.MessageCode;
import com.gosi.exception.PException;
import com.gosi.license.encrypter.AESEncrypter;
import com.gosi.license.encrypter.AESPassword;
import com.gosi.license.key.LicenseKeyGenerator;
import com.gosi.mail.SmtpMailSender;
import com.gosi.utils.input.Input;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({"/mypage"})
public class MypageController {
	
	@Resource
	SmtpMailSender smtpMailSender;
	@Resource
	DbHandler dbHandler;

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getNonmemberOrderList"})
	public Map<String, Object> getNonmemberOrderList(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String name = "", pwd = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			pwd = (String)job.get("pws");
			name = (String)job.get("name");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		String hash = "";

		try {
			hash = LicenseKeyGenerator.generate(name, pwd);
		} catch (Exception var7) {
			var7.printStackTrace();
		}

		List<Map<String, Object>> list = this.dbHandler.getNonmemberOrderList(hash);
		response.put("orderList", list);
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getOrderList"})
	public Map<String, Object> getOrderList(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "", startDt = "", endDt = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			startDt = (String)job.get("startDt");
			endDt = (String)job.get("endDt");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		param.put("id", id);
		param.put("startDt", startDt);
		param.put("endDt", endDt);
		List<Map<String, Object>> list = this.dbHandler.getOrderList(param);
		response.put("orderList", list);
		return response;
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getWebList"})
	public Map<String, Object> getWebList(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		List<Map<String, Object>> list = this.dbHandler.getWebOrderList(id);
		response.put("webList", list);
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/changeInfo"})
	public Map<String, Object> changeInfo(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "";
		String addr1 = "", addr2 = "", zipCd = "", tel = "", email = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			addr1 = (String)job.get("addr1");
			addr2 = (String)job.get("addr2");
			zipCd = (String)job.get("zipCd");
			tel = (String)job.get("tel");
			email = (String)job.get("email");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		Member mem = Member.make(id, addr1, addr2, zipCd, tel, email);
		this.dbHandler.changeInfo(mem);
		response.put("change", "success");
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/changePassWd"})
	public Map<String, Object> changePassWd(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "", passWd = "", newPassWd = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			passWd = (String)job.get("passwd");
			newPassWd = (String)job.get("newPasswd");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		String hash = this.dbHandler.getOnlyHash(id);
		String passwd = "";
		String str = "";

		try {
			AESEncrypter encrypter = new AESEncrypter(AESPassword.getPassword());
			str = encrypter.decrypt(hash);
			String[] arr = str.split("\\|\\|");
			if (arr.length > 0) {
				passwd = arr[1];
				if (!passWd.equals(passwd)) {
					throw PException.buildException((short) 9999, "passwd is not match");
				}
			}
		} catch (Exception var14) {
			var14.printStackTrace();
			throw PException.buildException((short) 9999, "passwd is strange");
		}

		if (passWd.equals(passwd)) {
			response.put("change", "success");
			String newHash = "";

			try {
				newHash = LicenseKeyGenerator.generate(id, newPassWd);
			} catch (Exception var13) {
				var13.printStackTrace();
				throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
			}

			param.put("newPassHash", newHash);
			param.put("id", id);
			param.put("newPassWd", newPassWd);
		} else {
			response.put("change", "fail");
		}

		this.dbHandler.changePassWd2(param);
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/deleteInfo"})
	public Map<String, Object> deleteInfo(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "", passwd = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			passwd = (String)job.get("passwd");
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		param.put("id", id);
		param.put("passWd", passwd);
		
		String hash = "";
		try {
			hash = LicenseKeyGenerator.generate(id, passwd);
		} catch (Exception var7) {
			var7.printStackTrace();
		}

		param.put("passHash", hash);
		if(this.dbHandler.deleteInfo(param)) {
			response.put("delete", "success");
			Map<String, Object> map = dbHandler.getMemberInfo(id);
			map.put("type", "delete");
			smtpMailSender.sendEmailMessage((String)map.get("email"), (String)map.get("name"), "고시계사 홈페이지 회원가입에 감사드립니다.", map);
		}
		else
			response.put("delete", "fail");
		return response;
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/deleteOrder"})
	public Map<String, Object> deleteOrder(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String jNo = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			jNo = (String)job.get("jNo");
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		
		if(dbHandler.checkOrder(jNo)) {
			dbHandler.deleteOrder(jNo);
			response.put("delete", "success");
		}
		else {
			response.put("delete", "fail");
		}
		
		return response;
	}
}