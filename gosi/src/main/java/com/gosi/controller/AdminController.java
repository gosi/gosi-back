package com.gosi.controller;

import com.gosi.db.DbHandler;
import com.gosi.db.dto.Book;
import com.gosi.db.dto.Member;
import com.gosi.db.dto.WebBook;
import com.gosi.exception.PException;
import com.gosi.license.encrypter.AESEncrypter;
import com.gosi.license.encrypter.AESPassword;
import com.gosi.mail.SmtpMailSender;
import com.gosi.utils.input.Input;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping({"/admin"})
public class AdminController {
	
	
	
	@Resource
	SmtpMailSender smtpMailSender;
	@Resource
	DbHandler dbHandler;
	
	String local = "tomcat/webapps/file_1003/";
	//String local = "C:\\Users\\jung\\Pictures\\2017-07\\";
	
	
	@ResponseBody
	@PostMapping({"/setLogin"})
	public Map<String, Object> setLogin(HttpServletRequest request) {
		Map<String, Object> response = new HashMap<String, Object>();
		String id = request.getParameter("id");
		String responsePasswd = request.getParameter("passwd");
		Map<String, Object> map = this.dbHandler.getHash(id);
		if(map!=null) {
			String tel = (String)map.get("tel");
			map.put("tel", tel);
			
			String hash = (String)map.get("passHash");
			String passwd = "";
			String str = "";

			try {
				AESEncrypter encrypter = new AESEncrypter(AESPassword.getPassword());
				str = encrypter.decrypt(hash);
				String[] arr = str.split("\\|\\|");
				if (arr.length > 0) {
					passwd = arr[1];
					if (!responsePasswd.equals(passwd)) {
						throw PException.buildException((short) 9999, "passwd is not match");
					}
				}
			} catch (Exception var10) {
				var10.printStackTrace();
				throw PException.buildException((short) 9999, "passwd is strange");
			}
			if (responsePasswd.equals(passwd)) 
				response.put("memberInfo", map);
			 else 
				response.put("memberInfo", null);
		}
		else
			response.put("memberInfo", null);

		return response;
	}

	@ResponseBody
	@PostMapping({"/addBook"})
	public void addBook(MultipartHttpServletRequest request) {
	
		Book book = Input.getBook(request);
		MultipartFile multipartFile = request.getFile("image");
		
		if(multipartFile !=null) {
			String[] arr = multipartFile.getOriginalFilename().split("\\\\");
			String str = "";
			if(arr.length>0)
				str = arr[arr.length-1];

			File file = new File(local + str);
			if(file.exists() == false) {
				file.mkdirs();
			}
			
			if(multipartFile.isEmpty() == false) {
				
				file = new File(local + str);
				try {
					multipartFile.transferTo(file);
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}
		else 
			book.setImage("file_1003/defaultImage.jpg");
		
		//update일 경우 -> 표지 제대로 적용되는지 확인하기
		String status = request.getParameter("status");
		if(status.equals("insert"))
			dbHandler.addBook(book);
		else
			dbHandler.changeBook(book);	
	}
	
	@ResponseBody
	@PostMapping({"/getBookList"})
	public Map<String, Object> getBookList(HttpServletRequest request) {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<>();
		
		int pageNum = Input.getIntValue("pageNum", request, 1);
		String title = Input.getValue("title", request);
		String cateStr = Input.getValue("cate", request); //모든도서
		int cate = Integer.parseInt(cateStr);
		
		String endDt = Input.getValue("endDt", request);
		String startDt = Input.getValue("startDt", request);
		String sellStatus = Input.getValue("sellStatus", request); 
		
		param.put("pageNum", pageNum*10);
		param.put("title", title);
		param.put("cate", cate);
		param.put("startDt", startDt);
		param.put("endDt", endDt);
		param.put("sellStatus", sellStatus);
		
		List<Book> list = dbHandler.getAdminBookList(param);
		if(pageNum>1) {
			List<Book> result = new ArrayList<>();
			for(int i=(pageNum-1)*10; i<pageNum*10; i++) {
				result.add(list.get(i));
			}
			response.put("bookList", result);
		}
		else
			response.put("bookList", list);
		return response;
	}

	@ResponseBody
	@PostMapping({"/deleteBook"})
	public void deleteBook(HttpServletRequest request) {
		dbHandler.deleteBook(Integer.parseInt(request.getParameter("bookId")));
	}

	@ResponseBody
	@PostMapping({"/addMegazine"})
	public void addMegazine(MultipartHttpServletRequest request) {
	
		Book book = Input.getMegazine(request);
		MultipartFile multipartFile = request.getFile("image");
		
		if(multipartFile !=null) {
			String[] arr = multipartFile.getOriginalFilename().split("\\\\");
			String str = "";
			if(arr.length>0)
				str = arr[arr.length-1];

			File file = new File(local + str);
			if(file.exists() == false) {
				file.mkdirs();
			}
			
			if(multipartFile.isEmpty() == false) {
				
				file = new File(local + str);
				try {
					multipartFile.transferTo(file);
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}
		else 
			book.setImage("file_1003/defaultImage.jpg");
		
		//update일 경우 -> 표지 제대로 적용되는지 확인하기
		String status = request.getParameter("status");
		if(status.equals("insert"))
			dbHandler.addMegazine(book);
		else
			dbHandler.changeMegazine(book);
		
	}
	
	@ResponseBody
	@PostMapping({"/addWebbook"})
	public void addWebbook(MultipartHttpServletRequest request) {
	
		WebBook book = Input.getWebbook(request);
		
		MultipartFile multipartFile = request.getFile("pdf");
		
		if(multipartFile !=null) {
			String[] arr = multipartFile.getOriginalFilename().split("\\\\");
			String str = "";
			if(arr.length>0)
				str = arr[arr.length-1];

			File file = new File(local + str);
			if(file.exists() == false) {
				file.mkdirs();
			}
			
			if(multipartFile.isEmpty() == false) {
				
				file = new File(local + str);
				try {
					multipartFile.transferTo(file);
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}
		else {
			//pdf 없음 -> 안됨!
		}
		
		//update일 경우 -> 표지 제대로 적용되는지 확인하기
		String status = request.getParameter("status");
		if(status.equals("insert"))
			dbHandler.addWebBook(book);
		else
			dbHandler.changeWebBook(book);	
	}
	
	@ResponseBody
	@PostMapping({"/getMegazineList"})
	public Map<String, Object> getMegazineList(HttpServletRequest request) {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<>();
		
		int pageNum = Input.getIntValue("pageNum", request, 1);
		String year = Input.getValue("year", request);
		String month = Input.getValue("month", request);
		String type = Input.getValue("type", request); //잡지, 웹
		
		param.put("pageNum", pageNum*10);
		param.put("type", type);
		
		if(type.equals("잡지")) {
			param.put("year", year.concat("년"));
			List<Book> list = dbHandler.getMegazineList(param);
			if(pageNum>1) {
				List<Book> result = new ArrayList<>();
				for(int i=(pageNum-1)*10; i<pageNum*10; i++) {
					result.add(list.get(i));
				}
				response.put("megazineList", result);
			}
			else
				response.put("megazineList", list);
		}
		else {
			
			if(!year.equals("")) {
				if(!month.equals("")) 
					param.put("yMon", year.concat("-").concat(month));
				else 
					param.put("yMon", year);
			}
			else
				param.put("yMon", "");
			
			//list -> year, month 잘 잘렸는지 확인하기
			List<WebBook> list = dbHandler.getWebList(param);
			if(pageNum>1) {
				List<WebBook> result = new ArrayList<>();
				for(int i=(pageNum-1)*10; i<pageNum*10; i++) {
					result.add(list.get(i));
				}
				response.put("webList", result);
			}
			else
				response.put("webList", list);
		}
		
		return response;
	}

	
	@ResponseBody
	@PostMapping({"/getOrderList"})
	public Map<String, Object> getOrderList(HttpServletRequest request) {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<>();
		
		String name = Input.getValue("name", request);
		String startDt = Input.getValue("startDt", request);
		String endDt = Input.getValue("endDt", request);
		String status = Input.getValue("status", request);
		String payStatus = Input.getValue("payStatus", request);
		String memYn = Input.getValue("memYn", request);
		int type = Input.getIntValue("type", request, 0);
		
		param.put("name", name);
		param.put("status", status);
		param.put("payStatus", payStatus);
		param.put("startDt", startDt);
		param.put("endDt", endDt);
		param.put("memYn", memYn);
		
		if(type==1) {
			List<Map<String,Object>> list = dbHandler.getTypeOneList(param);
			response.put("bookList", list);
		}
		else if(type==2) {
			List<Map<String,Object>> list = dbHandler.getTypeTwoList(param);
			response.put("megazineList", list);
		}
		else if(type==3) {
			List<Map<String,Object>> list = dbHandler.getTypeThreeList(param);
			response.put("webList", list);
		}
		else if(type==4) { //정기구독
			List<Map<String,Object>> list = dbHandler.getTypeFourList(param);
			response.put("standingBookList", list);
		}
		
		return response;
	}
	
	@ResponseBody
	@PostMapping({"/getUpdateInfo"})
	public Map<String, Object> getUpdateInfo(HttpServletRequest request) {
		Map<String, Object> response = new HashMap<String, Object>();
		
		String cartKey = Input.getValue("cartKey", request);
		String jNo = Input.getValue("jNo", request);
		
		Map<String, Object> map = dbHandler.getUpdateInfo(cartKey);
		List<Map<String, Object>> list = dbHandler.getSubOrderList(jNo);
		response.put("mainInfo", map);
		response.put("subOrderList", list);
		return response;
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/updateDeliveryInfo"})
	public void updateDeliveryInfo(HttpServletRequest request) {
		Map<String, Object> param = new HashMap<>();
		
		int cartKey = Input.getIntValue("cartKey", request, 0);
		String deliNum = Input.getValue("deliNum", request, "");
		String deliStatus = Input.getValue("deliStatus", request, "");
		String jNo = Input.getValue("jNo", request);
		param.put("jNo", jNo);
		param.put("cartKey", cartKey);
		param.put("deliNum", deliNum);
		param.put("deliStatus", deliStatus);
		
		dbHandler.updateDeliveryInfo(param);
	}
	
	//j_no에 해당되는 모든 책이 web인경우 -> 입금완료가 된다면 -> 배달정보 변경하기
		@ResponseBody
		@PostMapping({"/updatePaymentInfo"})
		public void updatePaymentInfo(HttpServletRequest request) {
			Map<String, Object> param = new HashMap<>();
			
			int cartKey = Input.getIntValue("cartKey", request, 0);
			String payStatus = Input.getValue("payStatus", request);
			String jNo = Input.getValue("jNo", request);
			param.put("jNo", jNo);
			param.put("cartKey", cartKey);
			param.put("payStatus", payStatus);
			
			dbHandler.updatePaymentInfo(param);
		}


	@ResponseBody
	@PostMapping({"/getMemberInfo"})
	public Map<String, Object> getMemberInfo(HttpServletRequest request) {
		Map<String, Object> response = new HashMap<String, Object>();
		List<Member> result = new ArrayList<>();
		
		String name = Input.getValue("name", request);
		String memYn = Input.getValue("memYn", request);
		if(memYn.equals("N")) 
			result = dbHandler.getNOMember(name);
		else if(memYn.equals("Y")) 
			result = dbHandler.getMember(name);
		else 
			result = dbHandler.getAllMember(name);
		
		response.put("memberList", result);
		return response;
	}


	@ResponseBody
	@PostMapping({"/updateMemberInfo"})
	public void updateMemberInfo(HttpServletRequest request) {
		Member mem = new Member();
		
		String name = Input.getValue("name", request, "");
		String id = Input.getValue("id", request, "");
		String tel = Input.getValue("tel", request, "");
		String addr1 = Input.getValue("addr1", request, "");
		String addr2 = Input.getValue("addr2", request, "");
		String zipCd = Input.getValue("zipCd", request, "");
		String email = Input.getValue("email", request, "");
		
		mem.setName(name);
		mem.setMemId(id);
		mem.setTel(tel);
		mem.setAddr1(addr1);
		mem.setAddr2(addr2);
		mem.setZipCd(zipCd);
		mem.setEmail(email);
		
		dbHandler.updateMemberInfo(mem);
	}


	@ResponseBody
	@PostMapping({"/postManageInfo"})
	public Map<String, Object> postManageInfo(HttpServletRequest request) {
		
		return dbHandler.getManageInfo();
	}
	

}