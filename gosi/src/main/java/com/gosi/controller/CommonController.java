package com.gosi.controller;

import com.gosi.db.DbHandler;
import com.gosi.db.dto.Book;
import com.gosi.db.dto.Notice;
import com.gosi.db.dto.WebBook;
import com.gosi.utils.input.Input;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({"/common"})
public class CommonController {
	
	@Resource
	DbHandler dbHandler;

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getCategoryList"})
	public Map<String, Object> getMainSubList(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String mainId = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			mainId = (String)job.get("mainId");
		} catch (IOException e) {
			e.printStackTrace();
		}

		List<Map<String, Object>> list = Input.getCategory(mainId);
		response.put("subList", list);
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getNoticeList"})
	public Map<String, Object> getNotice(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		List<Notice> list = new ArrayList<Notice>();
		
		JSONParser jsonParser = new JSONParser();
		String subId = "0"; int pageNum = 1;
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			subId = (String)job.get("subId");
			if(job.get("pageNum")!=null)
				pageNum = (int)(long)job.get("pageNum");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (subId.equals("0")) {
			List<Notice> info = this.dbHandler.getBbsList(param, 0);

			for (int i = 0; i < 8; ++i) {
				Notice bbs = (Notice) info.get(i);
				bbs.setDetailNum(i + 1);
				list.add(bbs);
			}
		} else {			
			param.put("id", subId);
			param.put("pageNum", pageNum);
			List<Notice> info = this.dbHandler.getBbsList(param, 1);
			int startPageNum = pageNum - 1;

			for (int i = startPageNum * 10; i < info.size(); ++i) {
				Notice bbs = (Notice) info.get(i);
				bbs.setDetailNum(i + 1);
				list.add(bbs);
			}
		}
		response.put("responseList", list);
		return response;
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getNoticePageNum"})
	public Map<String, Object> getNoticePageNum(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String subId = "0";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			subId = (String)job.get("subId");
		} catch (IOException e) {
			e.printStackTrace();
		}
		int num = 1, page = 1;
		
		num = this.dbHandler.getBbsPageNum(subId);
		if(num%10>0) page = num/10+1;
		else page = num/10;
		response.put("num", page);
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getDetailInfo"})
	public Map<String, Object> getDetailInfo(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		int id = 0;
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (int)(long)job.get("id");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Notice detail = this.dbHandler.getDetailInfo(id);
		response.put("detail", detail);
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getWebBookList"})
	public Map<String, Object> getWebBookList(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String year = "", month = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			year = (String)job.get("year");
			month = (String)job.get("month");
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(month.length()==1)
			month = "0".concat(month);
		List<WebBook> list = this.dbHandler.getWebBookList(year.concat("-").concat(month));
		response.put("webBookList", list);
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getBookList"})
	public Map<String, Object> getBookList(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String mainId = "", subId = ""; int pageNum = 1;
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			mainId = (String)job.get("mainId");
			subId = (String)job.get("subId");
			pageNum = (int)(long)job.get("pageNum");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int num = pageNum;
		int cate = Input.getPubsId(mainId, subId);
		param.put("cateId", cate);
		param.put("totalLimit", num * 16);
		List<Book> result = new ArrayList<Book>();
		List<Book> info = this.dbHandler.getBookList(param);
		int i;
		int price;
		int rPrice;
		int sales;
		if (info.size() < 16) {
			for (i = (num - 1) * 16; i < info.size(); ++i) {
				price = ((Book) info.get(i)).getPrice();
				rPrice = ((Book) info.get(i)).getSalePrice();
				sales = 100 - (rPrice * 100 / price);
				((Book) info.get(i)).setSales(sales);
				result.add(info.get(i));
			}
		} else {
			for (i = (num - 1) * 16; i < num * 16; ++i) {
				price = ((Book) info.get(i)).getPrice();
				rPrice = ((Book) info.get(i)).getSalePrice();
				sales = 100 - (rPrice * 100 / price);
				((Book) info.get(i)).setSales(sales);
				result.add(info.get(i));
			}
		}

		response.put("bookList", result);
		return response;
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getPageNum"})
	public Map<String, Object> getPageNum(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String mainId = "", subId = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			mainId = (String)job.get("mainId");
			subId = (String)job.get("subId");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int cate = Input.getPubsId(mainId, subId);
		param.put("cateId", cate);
		
		int num = this.dbHandler.getPageNum(param);
		
		int page = 1;
		if(num%16>0) page = num/16+1;
		else page = num/16;
		
		response.put("num", page);
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getBookDetail"})
	public Map<String, Object> getBookDetail(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		int bookId = 0;
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			bookId = (int)(long)job.get("bookId");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Book info = this.dbHandler.getBookDetail(bookId);
		int price = info.getPrice();
		int rPrice = info.getSalePrice();
		int sales = 100 - (rPrice * 100 / price);
		info.setSales(sales);

		response.put("detail", info);
		return response;
	}
	
	
}