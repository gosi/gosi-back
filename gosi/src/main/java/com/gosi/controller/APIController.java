package com.gosi.controller;

import com.gosi.db.DbHandler;
import com.gosi.mail.SmtpMailSender;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping({"/api"})
public class APIController {
	
	@Resource
	DbHandler dbHandler;
	
	@Resource
	SmtpMailSender smtpMailSender;
	
	String path = "tomcat/webapps/file_1003/";
	String local = "D:\\daun\\";

	@ResponseBody
	@PostMapping({"/ping"})
	public void ping(MultipartHttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		
	
	}
	
	@ResponseBody
	@PostMapping({"/addBook"})
	public void addBook(MultipartHttpServletRequest request) {
		
		Map<String, Object> map = new HashMap<>();
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String isbn = request.getParameter("isbn");
		String pageNum = request.getParameter("pageNum");
		int pageNumInt = Integer.parseInt(pageNum);
		String pan = request.getParameter("pan");
		String price = request.getParameter("price");
		int priceInt = Integer.parseInt(price);
		String salePrice = request.getParameter("salePrice");
		int salePriceInt = Integer.parseInt(salePrice);
		String dt = request.getParameter("dt");
		String bookType = request.getParameter("bookType");
		int bookCate = Integer.parseInt(bookType);
	
		MultipartFile file = request.getFile("image");
		String fileName = "";

		try {
			InputStream input = file.getInputStream();
			String[] arr = file.getOriginalFilename().split("\\\\");
			String str = "";
			if(arr.length>0)
				str = arr[arr.length-1];
			fileName="file_1003/";
			FileOutputStream output = new FileOutputStream(local.concat(str));
			fileName= fileName.concat(str);
			int readBuffer = 0;
	        byte [] buffer = new byte[512];
	        while((readBuffer = input.read(buffer)) != -1) {
	            output.write(buffer, 0, readBuffer);
	        }

	        input.close();
            output.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		String comment = request.getParameter("comment");
		map.put("title", title);
		map.put("author", author);
		map.put("isbn", isbn);
		map.put("pageNum", pageNumInt);
		map.put("pan", pan);
		map.put("price", priceInt);
		map.put("salePrice", salePriceInt);
		map.put("dt", dt);
		map.put("bookCate", bookCate);
		map.put("fileName", fileName);
		map.put("comment", comment);
		
		dbHandler.addNewBook(map);
		
		
	
	}

	@ResponseBody
	@PostMapping({"/book"})
	public void book(MultipartHttpServletRequest request) {
			Map<String, Object> map = new HashMap<>();
			String bookId = request.getParameter("bookId");
			int bookIdInt = Integer.parseInt(bookId);
			
			String content1 = request.getParameter("content1");
			
			map.put("bookId", bookIdInt);
			map.put("content1", content1);
			dbHandler.setBook(map);
	}
	
	@ResponseBody
	@PostMapping({"/book2"})
	public void book2(MultipartHttpServletRequest request) {
			Map<String, Object> map = new HashMap<>();
			String bookId = request.getParameter("bookId");
			int bookIdInt = Integer.parseInt(bookId);
			
			String content1 = request.getParameter("content1");
			
			map.put("bookId", bookIdInt);
			map.put("content1", content1);
			dbHandler.setBook2(map);
	}
	
	@ResponseBody
	@PostMapping({"/book3"})
	public void book3(MultipartHttpServletRequest request) {
			Map<String, Object> map = new HashMap<>();
			String bookId = request.getParameter("bookId");
			int bookIdInt = Integer.parseInt(bookId);
			
			String content1 = request.getParameter("content1");
			
			map.put("bookId", bookIdInt);
			map.put("content1", content1);
			dbHandler.setBook3(map);
	}
}