package com.gosi.controller;

import com.gosi.db.DbHandler;
import com.gosi.db.dto.Member;
import com.gosi.exception.MessageCode;
import com.gosi.exception.PException;
import com.gosi.license.encrypter.AESEncrypter;
import com.gosi.license.encrypter.AESPassword;
import com.gosi.license.key.LicenseKeyGenerator;
import com.gosi.mail.SmtpMailSender;
import com.gosi.utils.input.Input;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({"/login"})
public class LoginController {
	
	@Resource
	SmtpMailSender smtpMailSender;
	@Resource
	DbHandler dbHandler;
	

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/checkMember"})
	public Map<String, Object> checkMember(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String name = "", email = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			name = (String)job.get("name");
			email = (String)job.get("email");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		param.put("name", name);
		param.put("email", email);
		if (this.dbHandler.checkMember(param)) {
			response.put("check", "Y");
			response.put("name", name);
		}
		else {
			response.put("check", "N");
			response.put("name", name);
		}
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/addInfo"})
	public Map<String, Object> addInfo(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "", passwd = "";
		String name = "", addr1 = "", addr2 = "", zipCd = "", tel = "", email = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			passwd = (String)job.get("passwd");
			name = (String)job.get("name");
			addr1 = (String)job.get("addr1");
			addr2 = (String)job.get("addr2");
			zipCd = (String)job.get("zipCd");
			tel = (String)job.get("tel");
			email = (String)job.get("email");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		String hash = "";
		try {
			hash = LicenseKeyGenerator.generate(id, passwd);
		} catch (Exception var7) {
			var7.printStackTrace();
		}

		Member mem = Member.make(id, passwd, hash, name, addr1,addr2, zipCd, tel, email);
		
		if(this.dbHandler.addInfo(mem)==0)
			response.put("signIn", "fail");
		else {
			Map<String, Object> map = new HashMap<>();
			map.put("type", "join");
			map.put("name", mem.getName());
			map.put("tel", mem.getTel());
			
			smtpMailSender.sendEmailMessage(mem.getEmail(), mem.getName(), "고시계사 홈페이지 회원가입에 감사드립니다.", map);
			response.put("signIn", "success");
		}
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/checkId"})
	public Map<String, Object> checkId(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		param.put("id", id);
		
		if (this.dbHandler.checkId(param)) 
			response.put("check", "fail");
		else
			response.put("check", "success");

		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/setLogin"})
	public Map<String, Object> setLogin(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "", responsePasswd = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			responsePasswd = (String)job.get("passwd");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		Map<String, Object> map = this.dbHandler.getHash(id);
		if(map!=null) {
			String tel = (String)map.get("tel");
			map.put("tel", tel);
			map.put("id", id);
			
			String hash = (String)map.get("passHash");
			String passwd = "";
			String str = "";

			try {
				AESEncrypter encrypter = new AESEncrypter(AESPassword.getPassword());
				str = encrypter.decrypt(hash);
				String[] arr = str.split("\\|\\|");
				if (arr.length > 0) {
					passwd = arr[1];
					if (!responsePasswd.equals(passwd)) {
						throw PException.buildException((short) 9999, "passwd is not match");
					}
				}
			} catch (Exception var10) {
				var10.printStackTrace();
				throw PException.buildException((short) 9999, "passwd is strange");
			}
			if (responsePasswd.equals(passwd)) 
				response.put("memberInfo", map);
			 else 
				response.put("memberInfo", null);
		}
		else
			response.put("memberInfo", null);

		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/findId"})
	public Map<String, Object> findId(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String name = "", email = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			name = (String)job.get("name");
			email = (String)job.get("email");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		param.put("name", name);
		param.put("email", email);
		
		String id = this.dbHandler.findId(param);
		response.put("id", id);
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/findPassWd"})
	public Map<String, Object> findPassWd(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "", name = "", email = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			name = (String)job.get("name");
			email = (String)job.get("email");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		param.put("id", id);
		param.put("name", name);
		param.put("email", email);
		
		String hash = this.dbHandler.findPassWd(param);
		if (hash == null) 
			response.put("passwd", hash);
		 else {
			String passwd = "";
			String str = "";

			try {
				AESEncrypter encrypter = new AESEncrypter(AESPassword.getPassword());
				str = encrypter.decrypt(hash);
				String[] arr = str.split("\\|\\|");
				if (arr.length > 0) {
					passwd = arr[1];
				}
			} catch (Exception var12) {
				var12.printStackTrace();
				throw PException.buildException((short) 9999, "Info is strange");
			}
			response.put("passwd", passwd);
		}

		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/changePassWd"})
	public Map<String, Object> changePassWd(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "", passWd = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			passWd = (String)job.get("passwd");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		param.put("id", id);
		param.put("passwd", passWd);
		
		String hash = "";
		try {
			hash = LicenseKeyGenerator.generate(id, passWd);
		} catch (Exception var8) {
			var8.printStackTrace();
			throw PException.buildException((short) 9999, "Info is strange");
		}

		param.put("passHash", hash);
		this.dbHandler.changePassWd(param);
		response.put("change", "success");
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/loginNonmember"})
	public Map<String, Object> loginNonmember(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String name = "", pwd = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			pwd = (String)job.get("pwd");
			name = (String)job.get("name");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		String hash = this.dbHandler.getNonHash(name);
		if(hash!=null) {
			String passwd = "", confirmName = "";
			String str = "";

			try {
				AESEncrypter encrypter = new AESEncrypter(AESPassword.getPassword());
				str = encrypter.decrypt(hash);
				String[] arr = str.split("\\|\\|");
				if (arr.length > 0) {
					
					confirmName = arr[1];
					if (!name.equals(confirmName)) {
						throw PException.buildException((short) 9999, "name is not match");
					}
					passwd = arr[1];
					if (!pwd.equals(passwd)) {
						throw PException.buildException((short) 9999, "passwd is not match");
					}
				}
			} catch (Exception var10) {
				var10.printStackTrace();
				throw PException.buildException((short) 9999, "passwd is strange");
			}
			if (pwd.equals(passwd)) {
				if (name.equals(confirmName)) 
					response.put("login", "success");
				else 
					response.put("login", "fail");
			} else 
				response.put("login", "fail");
		}
		else 
			response.put("login", "fail");

		return response;
	}

}