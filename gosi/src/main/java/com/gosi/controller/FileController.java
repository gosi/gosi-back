package com.gosi.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import java.io.File;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.gosi.db.DbHandler;

@Controller
@RequestMapping({"/file"})
public class FileController {
	
	@Resource
	DbHandler dbHandler;
	
	String path = "tomcat/webapps/file_1003/";
	String local = "D:\\daun\\";

	//test 확인 완료
	@ResponseBody
	@PostMapping({"/upload"})
	public void uplode(MultipartHttpServletRequest request) {
		
		MultipartFile file = request.getFile("file01");

		try {
			InputStream input = file.getInputStream();
			String[] arr = file.getOriginalFilename().split("\\\\");
			String str = "";
			if(arr.length>0)
				str = arr[arr.length-1];
			FileOutputStream output = new FileOutputStream(local.concat("new_").concat(str));
			int readBuffer = 0;
	        byte [] buffer = new byte[512];
	        while((readBuffer = input.read(buffer)) != -1) {
	            output.write(buffer, 0, readBuffer);
	        }

	        input.close();
            output.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}
	
	//뭐든됨
	@ResponseBody
	@PostMapping({"/uplode2"})
	public void uplode2(MultipartHttpServletRequest request) {
		
		MultipartFile multipartFile = request.getFile("file01");
		
		String[] arr = multipartFile.getOriginalFilename().split("\\\\");
		String str = "";
		if(arr.length>0)
			str = arr[arr.length-1];

		File file = new File(local + str);
		if(file.exists() == false) {
			file.mkdirs();
		}
		
		if(multipartFile.isEmpty() == false) {
			
			file = new File(local + str);
			try {
				multipartFile.transferTo(file);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}

		
	}
	

}
