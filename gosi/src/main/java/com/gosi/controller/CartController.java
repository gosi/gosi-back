package com.gosi.controller;

import com.gosi.db.DbHandler;
import com.gosi.db.dto.Cart;
import com.gosi.db.dto.Order;
import com.gosi.exception.MessageCode;
import com.gosi.exception.PException;
import com.gosi.license.key.LicenseKeyGenerator;
import com.gosi.utils.input.Input;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({"/cart"})
public class CartController {
	@Resource
	DbHandler dbHandler;

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getMyCart"})
	public Map<String, Object> getMyCart(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		List<Map<String, Object>> bookList = this.dbHandler.getCartBookList(id);
		List<Map<String, Object>> megazineList = this.dbHandler.getCartMagazineList(id);
		List<Map<String, Object>> webList = this.dbHandler.getCartWebList(id);
		
		int tt = 0;
		if(bookList !=null) {
			for(int i=0; i<bookList.size(); i++) {
				Map<String, Object> map = bookList.get(i);
				int totalPrice = (int) map.get("totalPrice");
				tt = tt + totalPrice;
			}
		}
		if(megazineList!=null) {
			for(int i=0; i<megazineList.size(); i++) {
				Map<String, Object> map = megazineList.get(i);
				int totalPrice = (int) map.get("totalPrice");
				tt = tt + totalPrice;
			}
		}
		if(webList!=null) {
			for(int i=0; i<webList.size(); i++) {
				Map<String, Object> map = webList.get(i);
				int totalPrice = (int) map.get("totalPrice");
				tt = tt + totalPrice;
			}
		}
		
	
		//확인
		int deli = 0;
		if(tt<30000) deli = 3000;
		
		response.put("bookList", bookList);
		response.put("magazineList", megazineList);
		response.put("webList", webList);
		response.put("totalPrice", tt);
		response.put("deliveryFee", deli);
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/addCart"})
	public Map<String, Object> addCart(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "", title = "", comment = "";
		int bookId = 0, count = 1;
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			if(job.get("title")!=null)
				title = (String)job.get("title");
			if(job.get("comment")!=null)
				comment = (String)job.get("comment");
			if(job.get("bookId")!=null)
				bookId = (int)(long) job.get("bookId");
			count = (int)(long) job.get("count");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		if(bookId==0) {
			Cart cart3 = Cart.makeMegazine(id, title, comment, count);
			if (!comment.equals("")) //megazine이 이쪽으로 빠져야 함 
				this.dbHandler.addMegazineCart(cart3);
			else {
				//실패
				response.put("result","fail");
			}
		}
		else {
			Cart cart = Cart.makeBook(id, title, bookId, count);
			Cart cart2 = Cart.makeWeb(id, title, bookId, comment, count);
			
			if (!comment.equals("")) //web이 이쪽으로 빠져야 함 
				this.dbHandler.addMegazineCart(cart2);
			 else 
				this.dbHandler.addCart(cart);
		}

		response.put("result","success");
		return response;
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/deleteCart"})
	public Map<String, Object> deleteCart(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		int cartId = 0;
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			JSONArray jsonArray =  (JSONArray) job.get("data");
			for(int i=0; i<jsonArray.size(); i++) {
				JSONObject job2 = (JSONObject) jsonArray.get(i);
				cartId = (int)(long)job2.get("cartId");
				this.dbHandler.deleteCart(cartId);
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		//fail 줄 이유가...
		response.put("result","success");
		return response;
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/addOrderCart"})
	public Map<String, Object> addOrderCart(HttpServletRequest request) {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<>();
		List<Order> orderList = new ArrayList<>();
	
		JSONParser jsonParser = new JSONParser();
		String id = "", pwd = "", hash = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			if(job.get("pwd")!=null)
				pwd = (String)job.get("pwd");
			hash = LicenseKeyGenerator.generate(id, pwd);
			
			JSONArray jsonArray =  (JSONArray) job.get("data");
			for(int i=0; i<jsonArray.size(); i++) {
				JSONObject job2 = (JSONObject) jsonArray.get(i);
				Order order = Input.getOrderCart(request, job2, dbHandler);
				orderList.add(order);
			}
		} catch (Exception var7) {
			var7.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		param.put("id", id);
		dbHandler.deleteOrderCart(param);
		
		//비회원
		if(!pwd.equals("")) 
			dbHandler.addOrderCart(orderList, id, pwd, hash);
		else 
			dbHandler.addOrderCart(orderList, id);
		
		response.put("result","success");
		return response;
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getOrderCart"})
	public Map<String, Object> getOrderCart(HttpServletRequest request) {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<>();
	
		JSONParser jsonParser = new JSONParser();
		String id = "", pwd = "", hash = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			if(job.get("pwd")!=null) {
				pwd = (String)job.get("pwd");
				hash = LicenseKeyGenerator.generate(id, pwd);
				param.put("hash", hash);
			}
		} catch (Exception var7) {
			var7.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		} 
		
		param.put("id", id);
		
		
		if(dbHandler.check(param)) {
			List<Map<String, Object>> list = dbHandler.getOrderCart(param);
			response.put("resultList",list);
			response.put("totalPrice", list.get(0).get("totalPrice"));
			response.put("deliveryFee", list.get(0).get("deliveryFee"));
			response.put("result","success");
		}
		else {
			response.put("result","fail");
		}
		
		return response;
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/deleteOrderCart"})
	public Map<String, Object> deleteOrderCart(HttpServletRequest request) {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<>();
	
		JSONParser jsonParser = new JSONParser();
		String id = "", pwd = "", hash = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			if(job.get("pwd")!=null) {
				pwd = (String)job.get("pwd");
				hash = LicenseKeyGenerator.generate(id, pwd);
				param.put("hash", hash);
			}
		} catch (Exception var7) {
			var7.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		} 
		
		param.put("id", id);
		
		if(dbHandler.check(param)) {
			dbHandler.deleteOrderCart(param);
			response.put("result","success");
		}
		else {
			response.put("result","fail");
		}
		
		return response;
	}
}