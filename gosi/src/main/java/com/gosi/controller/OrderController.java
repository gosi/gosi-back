package com.gosi.controller;

import com.gosi.db.DbHandler;
import com.gosi.db.dto.Member;
import com.gosi.db.dto.Order;
import com.gosi.db.dto.StandingOrder;
import com.gosi.exception.MessageCode;
import com.gosi.exception.PException;
import com.gosi.mail.SmtpMailSender;
import com.gosi.utils.input.Input;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({"/order"})
public class OrderController {
	
	@Resource
	SmtpMailSender smtpMailSender;
	@Resource
	DbHandler dbHandler;
	
	//String requestURL = "https://api.iamport.kr/users/getToken";
	//String imp_key 		=	URLEncoder.encode("1789294470134223", "UTF-8");
	//String imp_secret	=	URLEncoder.encode("Luir9cxiZndhjDM0GUla7Czs209JVw7G0q89CFsC2FTmjFnZAIaHkJanKj6FQemQEu0hasTd7NzqtWq3", "UTF-8");

	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/setStandingOrder"})
	public Map<String, Object> setStandingOrder(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		
		JSONParser jsonParser = new JSONParser();
		String id = "", tel = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			tel = (String)job.get("orderTel");

			tel = tel.substring(3); //핸드폰번호는 01012345678 형식으로
			StandingOrder order = Input.getStandingOrder(job);
			String jNo = Input.getDate().concat("_").concat(tel);
			order.setjNo(jNo);
			//비회원
			if(id==null || id.equals("")) 
				dbHandler.setStandingOrderNoMember(order);
			else {
				order.setId(id);
				dbHandler.setStandingOrder(order);
			}
			response.put("jNo", jNo);
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		
		return response;
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getUserInfo"})
	public Map<String, Object> getUserInfo(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		Member mem = this.dbHandler.getUserInfo(id);
		response.put("userInfo", mem);
		return response;
	}

	
	//배송비는 받은대로 처리함
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/requestOrder"})
	public Map<String, Object> requestOrder(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		List<Order> orderList = new ArrayList<>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "", tel = "", jNo = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			tel = (String)job.get("orderTel");
			tel = tel.substring(3); //핸드폰번호는 01012345678 형식으로
			jNo = Input.getDate().concat("_").concat(tel);
			
			JSONArray jsonArray = (JSONArray) job.get("data");
			for(int i=0; i<jsonArray.size(); i++) {
				JSONObject job2 = (JSONObject) jsonArray.get(i);
				Order order = Input.getOrder(job, job2);
				order.setjNo(jNo);
				orderList.add(order);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		//비회원
		if(id.equals("")) 
			dbHandler.setOrderNoMember(orderList);
		else 
			dbHandler.setOrder(orderList);
		
		Map<String, Object> map = dbHandler.getPayInfo(jNo);
		response.put("result", map);
		return response;
	}
	
	//결제 성공, 실패, 취소
	//실패는 어떻게 할지 아직 결정 못함
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/getRequestOrderResponse"})
	public void getRequestOrderResponse(HttpServletRequest request) throws ParseException {
		Map<String, Object> param = new HashMap<>();
		
		JSONParser jsonParser = new JSONParser();
		String payStatus = "", payType = "", jNo = "";
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			payStatus = (String)job.get("payStatus");
			payType = (String)job.get("payType");
			jNo = (String)job.get("jNo");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		param.put("payType", payType);
		param.put("jNo", jNo);
		param.put("payStatus", payStatus);
		
		dbHandler.updateOrder(param);
		
		Map<String, Object> map = dbHandler.getOrderBundleInfo(jNo);
		List<Map<String, Object>> list = dbHandler.getOrderPriInfo(jNo);
		map.put("orderList", list);
		map.put("type", "order");
		smtpMailSender.sendEmailMessage((String)map.get("email"), (String)map.get("name"), "고시계사 홈페이지 회원가입에 감사드립니다.", map);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@ResponseBody
	@PostMapping({"/sendMegazineInfo"})
	public Map<String, Object> sendMegazineInfo(HttpServletRequest request) throws ParseException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<>();
		
		JSONParser jsonParser = new JSONParser();
		String id = "", comment = "", jNo = ""; int count = 1;
		try {
			JSONObject job = (JSONObject) jsonParser.parse(Input.getBody(request));
			id = (String)job.get("id");
			comment = (String)job.get("comment");
			count = (int)(long)job.get("count");
		} catch (IOException e) {
			e.printStackTrace();
			throw PException.buildException(MessageCode.JSON_PARSING_ERROR, "JSON_PARSING_ERROR");
		}
		
		param.put("id", id);
		String[] arr = comment.split("-");
		String searchWord = arr[0];
		param.put("search", searchWord);
		
		
		Map<String, Object> map = dbHandler.sendMegazineInfo(param);
		param.put("bookId", map.get("bookId"));
		param.put("title", map.get("title"));
		param.put("comment", comment);
		param.put("count", count);
		
		if(id.equals(""))  //비회원
			dbHandler.insertCartNoMem(param); //-> 후에 guest 이름 update
		else 
			dbHandler.insertCartMem(param);
		
		
		int deliveryFee = 0, totalPrice = 0;
		totalPrice = (int) map.get("salePrice")*count;
		if(totalPrice < 30000) {
			deliveryFee = 3000; //2500?
			totalPrice = totalPrice + deliveryFee;
		}
		map.put("comment", comment);
		map.put("count", count);
		map.put("totalPrice", totalPrice);
		map.put("deliveryFee", deliveryFee);
		
		response.put("megazineInfo", map);
		return response;
	}
	
}