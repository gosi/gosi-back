package com.gosi.utils;

public class NumericUtils {
	
	
	
	public static Integer defaultIntIfNull(Integer i)
	{
		if(i==null){
			return 0;
		}
		
		return i;
	}
	
	public static Double defaultDoubleIfNull(Double i)
	{
		if(i==null){
			return 0D;
		}
		
		return i;
	}

}
