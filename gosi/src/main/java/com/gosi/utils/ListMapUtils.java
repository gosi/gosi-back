
package com.gosi.utils;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


public class ListMapUtils {

	public static List<Map<String, Object>> getListMap(List<Map<String, Object>> datas, String keyName, Long key) {
		return datas.stream().filter(p -> ((Long) p.get(keyName)).longValue() == key.longValue())
				.collect(Collectors.toList());
	}

	public static List<Map<String, Object>> getListMap(List<Map<String, Object>> datas, String keyName, Integer key) {
		return datas.stream().filter(p -> ((Integer) p.get(keyName)).intValue() == key.intValue()).collect(Collectors.toList());
	}

	public static List<Map<String, Object>> getListMap(List<Map<String, Object>> datas, String key1, String key2,
			Integer keyValue1, Integer keyValue2) {
		return datas.stream().filter(p -> p.get(key1) == keyValue1 && p.get(key2) == keyValue2)
				.collect(Collectors.toList());
	}

	public static List<Map<String, Object>> getListMap(List<Map<String, Object>> datas, String key1, String key2,
			String keyValue1, String keyValue2) {
		return datas.stream().filter(p -> keyValue1.equalsIgnoreCase((String) p.get(key1))
				&& keyValue2.equalsIgnoreCase((String) p.get(key2))).collect(Collectors.toList());
	}

	public static List<Map<String, Object>> getListMap(List<Map<String, Object>> datas, String keyName, String key) {
		return datas.stream().filter(p -> key.equalsIgnoreCase((String) p.get(keyName))).collect(Collectors.toList());
	}

	public static Optional<Map<String, Object>> getMap(List<Map<String, Object>> datas, String keyName, Long key) {
		return datas.stream().filter(b -> b.get(keyName) == key).findFirst();
	}

	public static Optional<Map<String, Object>> getMap(List<Map<String, Object>> datas, String keyName, Integer key) {
		return datas.stream().filter(b -> b.get(keyName) == key).findFirst();
	}

	public static Optional<Map<String, Object>> getMap(List<Map<String, Object>> datas, String keyName, String key) {
		return datas.stream().filter(b -> key.equalsIgnoreCase((String) b.get(keyName))).findFirst();
	}

}
