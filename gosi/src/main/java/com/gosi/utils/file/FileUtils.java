package com.gosi.utils.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.util.ResourceUtils;

public class FileUtils {
	private static final char CH_DOT = '.';

	public void copyResourceInJarTo(String fileName, String absFile) throws IOException {
		InputStream is = this.getClass().getResourceAsStream(fileName);
		makeDirectory(getPathFromAbsFile(absFile));
		OutputStream output = new FileOutputStream(absFile);
		IOUtils.copy(is, output);
		is.close();
		output.close();
	}

	public String getWorkingCurrentPath() {
		return this.getClass().getClassLoader().getResource("").getPath().substring(0);
	}

	public void copyResourceInClassPathTo(String fileName, String absFile) throws IOException {
		InputStream is = new FileInputStream(ResourceUtils.getFile(fileName));
		makeDirectory(getPathFromAbsFile(absFile));
		OutputStream output = new FileOutputStream(absFile);
		IOUtils.copy(is, output);
		is.close();
		output.close();
	}

	public void copyResourceInClassPathTo(String fileName, OutputStream output) throws IOException {
		InputStream is = new FileInputStream(ResourceUtils.getFile(fileName));
		IOUtils.copy(is, output);
		is.close();
		output.flush();
		output.close();
	}

	public static String getFileFromURL(String directory, String downloadUrl, String fileName) throws Exception {
		String fileFullPath = "";
		InputStream fin = null;

		try {
			URL url = new URL(downloadUrl);
			fin = url.openStream();
			makeDirectory(directory);
			fileFullPath = directory + File.separatorChar + fileName;

			for (int index = 0; fileExist(fileFullPath); ++index) {
				fileFullPath = directory + File.separatorChar + getFileName(fileName) + "(" + index + ")."
						+ getFileType(fileName);
			}

			makeNewFile(fin, fileFullPath);
			return fileFullPath;
		} catch (Exception var10) {
			throw var10;
		} finally {
			fin.close();
		}
	}

	public static String getFileFromInputStream(String directory, InputStream io, String fileName) throws Exception {
		String fileFullPath = "";
		InputStream fin = null;

		try {
			fin = io;
			makeDirectory(directory);
			fileFullPath = directory + File.separatorChar + fileName;

			for (int index = 0; fileExist(fileFullPath); ++index) {
				fileFullPath = directory + File.separatorChar + getFileName(fileName) + "(" + index + ")."
						+ getFileType(fileName);
			}

			makeNewFile(io, fileFullPath);
			return fileFullPath;
		} catch (Exception var9) {
			throw var9;
		} finally {
			fin.close();
		}
	}

	public static File getFileObjectFromInputStream(String directory, InputStream io, String fileName)
			throws Exception {
		String fileFullPath = "";
		InputStream fin = null;

		try {
			fin = io;
			makeDirectory(directory);
			fileFullPath = directory + File.separatorChar + fileName;

			for (int index = 0; fileExist(fileFullPath); ++index) {
				fileFullPath = directory + File.separatorChar + getFileName(fileName) + "(" + index + ")."
						+ getFileType(fileName);
			}

			makeNewFile(io, fileFullPath);
			File f = new File(fileFullPath);
			return f;
		} catch (Exception var10) {
			throw var10;
		} finally {
			fin.close();
		}
	}

	public static void makeDirectory(String path) {
		File f = new File(path);
		if (!f.exists()) {
			f.mkdirs();
			f.setExecutable(true, false);
			f.setReadable(true, false);
			f.setWritable(true, true);
		}

	}

	public static String getPathFromAbsFile(String absFile) {
		File f = new File(absFile);
		absFile = f.getAbsolutePath();
		if (absFile.lastIndexOf(47) > 0) {
			return absFile.substring(0, absFile.lastIndexOf(47));
		} else {
			return absFile.lastIndexOf(92) > 0 ? absFile.substring(0, absFile.lastIndexOf(92)) : absFile;
		}
	}

	public static File getFullPathFromAbsFileAndFileName(String absFile, String fileName) {
		StringBuffer sb = new StringBuffer(absFile);
		sb.append(File.separatorChar).append(fileName);
		return new File(sb.toString());
	}

	public static String getFullPathFromAbsFile(String absFile) {
		File f = new File(absFile);
		absFile = f.getAbsolutePath();
		return absFile;
	}

	public static String changeFileNameWithSmartWay(File source, String dir, String fileName) {
		StringBuilder filePath = new StringBuilder();
		filePath.append(dir);
		filePath.append(File.separator);
		filePath.append(System.nanoTime());
		filePath.append(".");
		filePath.append(getFileType(fileName));
		if (fileExist(filePath.toString())) {
			return changeFileNameWithSmartWay(source, dir, fileName);
		} else {
			renameTo(source, filePath.toString());
			return filePath.toString();
		}
	}

	public static boolean renameTo(File f, String absFileName) {
		return f.renameTo(new File(absFileName));
	}

	public static boolean renameTo(String sourceFileName, String absFileName) {
		File f = new File(sourceFileName);
		return f.renameTo(new File(absFileName));
	}

	public static void deleteFilesInTemporaryDir(String tempDir) {
		try {
			File f = new File(tempDir);
			File[] files = f.listFiles();
			File[] var3 = files;
			int var4 = files.length;

			for (int var5 = 0; var5 < var4; ++var5) {
				File cf = var3[var5];
				cf.delete();
			}

			f.delete();
		} catch (Exception var7) {
			var7.printStackTrace();
		}

	}

	public static synchronized String createTemporaryDir(String baseTempDir) throws ParseException {
		StringBuffer tempDir = new StringBuffer(baseTempDir);
		tempDir.append("/");
		tempDir.append(getCurrentSecond());
		return tempDir.toString();
	}

	public static synchronized String createTodayDirectory(String parentDir, String msisdn) throws ParseException {
		StringBuffer tempDir = new StringBuffer(parentDir);
		tempDir.append(File.separatorChar);
		tempDir.append(getCurrentDate());
		tempDir.append(File.separatorChar);
		tempDir.append(msisdn);
		makeDirectory(tempDir.toString());
		return tempDir.toString();
	}

	public static synchronized String createUserDirectory(String baseTempDir, String msisdn) throws ParseException {
		StringBuffer tempDir = new StringBuffer(baseTempDir);
		tempDir.append(File.separatorChar);
		tempDir.append("users");
		tempDir.append(File.separatorChar);
		tempDir.append(msisdn.substring(msisdn.length() - 1, msisdn.length()));
		tempDir.append(File.separatorChar);
		tempDir.append(msisdn.substring(msisdn.length() - 2, msisdn.length() - 1));
		makeDirectory(tempDir.toString());
		return tempDir.toString();
	}

	public static synchronized String createDirectory(String baseTempDir, String userId) throws ParseException {
		StringBuffer tempDir = new StringBuffer(baseTempDir);
		tempDir.append(File.separatorChar);
		tempDir.append(userId);
		tempDir.append(File.separatorChar);
		tempDir.append(getCurrentDate());
		makeDirectory(tempDir.toString());
		return tempDir.toString();
	}

	public static String deleteFileFromFileSystem(String strFullFileName) throws Exception {
		String strResult = "success";

		try {
			File f = new File(strFullFileName);
			f.delete();
			strResult = "success";
		} catch (Exception var3) {
			strResult = "fail";
			var3.printStackTrace();
		}

		return strResult;
	}

	public static byte[] getByteArrayFromFile(File file) throws IOException {
		byte[] byteArray = new byte[(int) file.length()];
		byteArray = org.apache.commons.io.FileUtils.readFileToByteArray(file);
		return byteArray;
	}

	public static String getOnlyFileName(String fileName) {
		if (fileName != null) {
			fileName = FilenameUtils.getName(fileName);
		}

		return fileName;
	}

	private static String getCurrentSecond() throws ParseException {
		return (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS")).format(new Date());
	}

	private static String getCurrentDate() throws ParseException {
		return (new SimpleDateFormat("yyyyMMdd")).format(new Date());
	}

	public static String getPhysicalFileName(String fileUploadPath, String userId) throws ParseException {
		StringBuilder filePath = new StringBuilder();
		filePath.append(fileUploadPath);
		filePath.append(File.separator);
		filePath.append(userId);
		String userfilePath = filePath.toString();
		if (!directoryExist(userfilePath)) {
			makeDirectory(userfilePath);
		}

		String systemTime = getCurrentSecond();
		filePath.append(File.separator);
		filePath.append(systemTime);
		return fileExist(filePath.toString()) ? getPhysicalFileName(fileUploadPath, userId) : filePath.toString();
	}

	public static String getFileType(String fileName) {
		int index = fileName.lastIndexOf(".");
		return fileName.substring(index + 1);
	}

	public static String getFileName(String fileName) {
		fileName = getOnlyFileName(fileName);
		int index = fileName.lastIndexOf(".");
		return fileName.substring(0, index);
	}

	public static String getFilePath(String fileFullName) {
		return FilenameUtils.getFullPathNoEndSeparator(fileFullName);
	}

	public static String getFileNameFromFullFilePath(String fileFullName) {
		return FilenameUtils.getName(fileFullName);
	}

	public static boolean directoryExist(String file_path) {
		boolean exists = (new File(file_path)).exists();
		return exists;
	}

	public static boolean fileExist(String file_path) {
		boolean exists = (new File(file_path)).exists();
		return exists;
	}

	public static long getFileSize(String fullFilePath) {
		File f = new File(fullFilePath);
		return !f.exists() ? -1L : f.length();
	}

	public static boolean makeNewFile(String content, String fileName) throws Exception {
		boolean rtn = false;
		FileOutputStream fos = null;
		ByteArrayInputStream is = null;

		try {
			is = new ByteArrayInputStream(content.getBytes());
			fos = new FileOutputStream(fileName);
			byte[] buff = new byte[512000];
			boolean var6 = false;

			int s1;
			while ((s1 = is.read(buff)) != -1) {
				fos.write(buff, 0, s1);
			}

			rtn = true;
			return rtn;
		} catch (FileNotFoundException var11) {
			throw var11;
		} catch (IOException var12) {
			throw var12;
		} finally {
			if (is != null) {
				is.close();
			}

			if (fos != null) {
				fos.close();
				fos.flush();
			}

		}
	}

	public static void addContent(String fileFullName, String content) throws Exception {
		File file = new File(fileFullName);
		FileWriter fileWriter = new FileWriter(file, true);
		BufferedWriter bufferFileWriter = new BufferedWriter(fileWriter);
		fileWriter.append(content);
		fileWriter.append(System.lineSeparator());
		bufferFileWriter.close();
	}

	public static boolean makeNewFile(InputStream is, String fileName) throws Exception {
		boolean rtn = false;

		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			byte[] buff = new byte[512000];
			boolean var5 = false;

			int s1;
			while ((s1 = is.read(buff)) != -1) {
				fos.write(buff, 0, s1);
			}

			fos.close();
			rtn = true;
			return rtn;
		} catch (FileNotFoundException var6) {
			throw var6;
		} catch (IOException var7) {
			throw var7;
		}
	}

	public static void removeFile(File fileObj) {
		if (fileObj.exists()) {
			fileObj.delete();
		}

	}

	public static void removeFile(String filePath) {
		removeFile(new File(filePath));
	}

	public static void removeAllFile(String[] filePathArray) {
		for (int i = 0; i < filePathArray.length; ++i) {
			removeFile(filePathArray[i]);
		}

	}

	public static int getFileUploadTotalMemorySize(String uploadMaxSize) {
		String maxFileSize = uploadMaxSize;
		boolean var2 = false;

		int nMaxFileSize;
		try {
			nMaxFileSize = Integer.parseInt(maxFileSize);
		} catch (Exception var4) {
			nMaxFileSize = Integer.MAX_VALUE;
		}

		return nMaxFileSize;
	}

	public static long getFileUploadTotalSize(String uploadMaxSize) {
		String maxFileSize = uploadMaxSize;
		long mega = 1048576L;
		long nMaxFileSize = 0L;

		try {
			nMaxFileSize = Long.parseLong(maxFileSize);
			nMaxFileSize *= mega;
		} catch (Exception var7) {
			nMaxFileSize = Long.MAX_VALUE;
		}

		return nMaxFileSize;
	}

	public static String getFileSize(long filesize) {
		DecimalFormat df = new DecimalFormat(".#");
		String fSize = "";
		if (filesize > 1024L && filesize < 1048576L) {
			fSize = df.format((double) ((float) filesize / 1024.0F)).toString() + " KB";
		} else if (filesize >= 1048576L) {
			fSize = df.format((double) ((float) filesize / 1048576.0F)).toString() + " MB";
		} else if (filesize < 1L) {
			fSize = "0 KB";
		} else {
			fSize = "1 KB";
		}

		return fSize;
	}

	public static void moveFile(String sourceFilePath, String targetFilePath) {
		try {
			File fs = new File(sourceFilePath);
			File ft = new File(targetFilePath);
			fs.renameTo(ft);
		} catch (Exception var4) {
			var4.printStackTrace();
		}

	}

	public static void copyFile(String sourceFilePath, String targetFilePath) throws IOException {
		try {
			DataInputStream in = new DataInputStream(new FileInputStream(sourceFilePath));
			DataOutputStream out = new DataOutputStream(new FileOutputStream(targetFilePath));
			byte[] buff = new byte[8192];

			int size;
			while ((size = in.read(buff)) > -1) {
				out.write(buff, 0, size);
			}

			out.close();
			in.close();
		} catch (IOException var6) {
			throw var6;
		}
	}

	public static void deleteAllFile(String[] filePathList) {
		try {
			for (int i = 0; i < filePathList.length; ++i) {
				File fs = new File(filePathList[i]);
				fs.delete();
			}
		} catch (Exception var3) {
			var3.printStackTrace();
		}

	}

	public static void ensureDirectory(File dir) {
		if (!dir.exists()) {
			dir.mkdirs();
		} else if (dir.isFile()) {
			dir.mkdirs();
		}

	}

	public static void ensureDirectory(String dirPath) {
		ensureDirectory(new File(dirPath));
	}

	public static void deleteAll(File dir, boolean deleteAllDir) {
		if (dir.exists() && dir.isDirectory()) {
			File[] fileList = dir.listFiles();

			for (int i = 0; i < fileList.length; ++i) {
				File file = fileList[i];
				if (file.isFile()) {
					file.delete();
				} else if (file.isDirectory() && deleteAllDir) {
					deleteAll(file, true);
					file.delete();
				}
			}
		}

	}

	public static void deleteAll(String dirPath, boolean deleteAllDir) {
		deleteAll(new File(dirPath), deleteAllDir);
	}

	public static void deleteDirectory(File dir) {
		deleteAll(dir, true);
		if (dir.exists() && dir.isDirectory()) {
			dir.delete();
		}

	}

	public static String composeFileName(String fileName, String ext) {
		StringBuilder builder = new StringBuilder();
		builder.append(fileName).append('.').append(ext);
		return builder.toString();
	}

	public static void deleteDirectory(String dirPath) {
		deleteDirectory(new File(dirPath));
	}

	public static boolean checkExtension(File file, String ext) {
		boolean ret = false;
		if (file.exists()) {
			String fileName = file.getName();
			int extIndex = fileName.lastIndexOf(46);
			if (extIndex != -1) {
				String fileExt = fileName.substring(extIndex + 1);
				if (ext.equalsIgnoreCase(fileExt)) {
					ret = true;
				}
			}
		}

		return ret;
	}

	public static int decompress(String zipFile, String dstPath) throws IOException {
		ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
		ZipEntry entry = null;
		String file = null;
		FileOutputStream out = null;
		byte[] buffer = new byte[102400];
		int cnt = 0;

		try {
			while ((entry = zis.getNextEntry()) != null) {
				if (!entry.isDirectory()) {
					file = dstPath + entry.getName();
					out = new FileOutputStream(file);

					int bytes_read;
					while ((bytes_read = zis.read(buffer)) != -1) {
						out.write(buffer, 0, bytes_read);
					}

					out.close();
					++cnt;
				}
			}
		} catch (Exception var13) {
			var13.printStackTrace();
		} finally {
			out.close();
			zis.close();
		}

		return cnt;
	}

	public static void compressWithZip(String fileFullPath) {
		byte[] buffer = new byte[1024];
		FileInputStream in = null;
		ZipOutputStream zos = null;
		StringBuffer sb = new StringBuffer();
		sb.append(fileFullPath).append(".zip");
		String zipFileName = sb.toString();

		try {
			FileOutputStream fos = new FileOutputStream(zipFileName);
			zos = new ZipOutputStream(fos);
			File fileToZip = new File(fileFullPath);
			in = new FileInputStream(fileToZip);
			ZipEntry ze = new ZipEntry(fileToZip.getName());
			zos.putNextEntry(ze);

			int len;
			while ((len = in.read(buffer)) > 0) {
				zos.write(buffer, 0, len);
			}
		} catch (IOException var18) {
			var18.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}

				if (zos != null) {
					zos.closeEntry();
					zos.close();
				}
			} catch (IOException var17) {
				var17.printStackTrace();
			}

			removeFile(fileFullPath);
			renameTo(zipFileName, fileFullPath);
		}

	}

	public static String changeMD5(String fileName) {
		String fileID = fileName;
		StringBuffer sb = new StringBuffer();

		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(fileID.getBytes());
			byte[] md5Bytes = md5.digest();

			for (int i = 0; i < md5Bytes.length; ++i) {
				sb.append(md5Bytes[i]);
			}
		} catch (NoSuchAlgorithmException var6) {
			var6.printStackTrace();
		}

		return sb.toString();
	}

	public static String getFileContent(String filePath) throws IOException {
		String fileContent = "";
		BufferedReader br = new BufferedReader(new FileReader(filePath));

		try {
			StringBuilder sb = new StringBuilder();

			for (String line = br.readLine(); line != null; line = br.readLine()) {
				sb.append(line);
				sb.append("\n");
			}

			fileContent = sb.toString();
			return fileContent;
		} finally {
			br.close();
		}
	}

	public static String getFileContent(File file) throws IOException {
		String fileContent = "";
		BufferedReader br = new BufferedReader(new FileReader(file));

		try {
			StringBuilder sb = new StringBuilder();

			for (String line = br.readLine(); line != null; line = br.readLine()) {
				sb.append(line);
				sb.append("\n");
			}

			fileContent = sb.toString();
			return fileContent;
		} finally {
			br.close();
		}
	}

	public static String getFileContentWithKey(String filePath, String key) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filePath));

		try {
			String line = br.readLine();

			while (line != null) {
				line = br.readLine();
				if (line.contains(key)) {
					String var4 = line;
					return var4;
				}
			}
		} finally {
			br.close();
		}

		return "";
	}

	public static File[] getFilesFromDir(String dirPath) {
		File f = new File(dirPath);
		return directoryExist(dirPath) ? f.listFiles() : null;
	}
}