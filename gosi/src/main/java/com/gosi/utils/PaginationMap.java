package com.gosi.utils;

import java.util.HashMap;

public class PaginationMap<K, V> extends HashMap<String, Object> {
	private static final long serialVersionUID = 5162978139333700896L;

	private int page = 1;								// 현재 페이지
	private int pageSize = 10;						// 한 페이지당 목록 수
	private long totalCount = 0;						// 총 목록의 건수
	private int pageCount = 5;						// 화면에 나타날 페이지의 수
	private int totalPageCount = 0; 
	private int start = 0;
	private int end = 0;
	private int next = 0;
	private int last = 0;
	private int prev = 0;
	private int first = 1;
	private String javascriptFunctionName = "goPage";
	
	
	private boolean paginationEnabled = true;
	

	public String getPaginationHtml() {
		StringBuffer sb = new StringBuffer();
		
		
		if(totalCount <= 0) {
			getDefaultPaginationHtml(sb);
			return sb.toString();
		}
		
		totalPageCount = (int)(totalCount / (long)pageSize);
		
		if(totalCount % pageSize > 0)
			totalPageCount ++;
		
		if(page > totalPageCount) 
			page = totalPageCount;
		
		start = (page / pageCount) * pageCount + 1;
		
		if(start > page) 
			start = start - pageCount;
		
		end = start + pageCount - 1;
		next = start + pageCount;
		last = totalPageCount;
		prev = start - pageCount;
		first = 1;

		if(end > totalPageCount) end = totalPageCount;
		if(next > totalPageCount) next = totalPageCount;

		sb.append("<nav>");
		sb.append("	<ul class=\"pagination pull-right\">");
		
		
		if(start == 1) {		
			sb.append("		<li class=\"footable-page-arrow disabled\">");
			sb.append("<a data-page=\"prev\" href=\"#\"> < </a></li>");
		} else {			
			sb.append("		<li class=\"footable-page-arrow\">");
			sb.append("<a data-page=\"prev\" href=\"javascript:" + javascriptFunctionName + "("+ String.valueOf(prev) +");\"> < </a></li>");
		}
		
		
		for(int i = start; i <= end; i++) {
			if (i == page){
				sb.append("		<li class=\"active\">");
			}
			else {
				sb.append("		<li>");
			}
			sb.append("<a href=\"javascript:" + javascriptFunctionName + "(" + String.valueOf(i) + ");\">" + String.valueOf(i) + "</a></li>");
		}
		
		
		if(end < totalPageCount) {
			sb.append("		<li class=\"footable-page-arrow\">");
			sb.append("<a data-page=\"next\" href=\"javascript:" + javascriptFunctionName + "(" + String.valueOf(next) + ");\"> > </a></li>");
		} else {
			sb.append("		<li class=\"footable-page-arrow disabled\">");
			sb.append("<a data-page=\"next\" href=\"#\"> > </a></li>");
		}
		
		
		sb.append("	</ul>");
		sb.append("</nav>");
		
		return sb.toString();
	}

	private void getDefaultPaginationHtml(StringBuffer sb) {
		sb.append("<nav>");
		sb.append("	<ul class=\"pagination pull-right\">");
		sb.append("		<li class=\"footable-page-arrow disabled\">");
		sb.append("<a data-page=\"prev\" href=\"#\"> < </a></li>");
		sb.append("		<li class=\"active\">");
		sb.append("<a href=\"javascript:" + javascriptFunctionName + "(1);\">1</a></li>");
		sb.append("		<li class=\"footable-page-arrow disabled\">");
		sb.append("<a data-page=\"next\" href=\"#\"> > </a></li>");
		sb.append("	</ul>");
		sb.append("</nav>");
	}
	
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	
	public void setJavascriptFunction(String javascriptFunctionName) {
		this.javascriptFunctionName = javascriptFunctionName;
	}
	
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	

	public long getTotalCount() {
		return this.totalCount;
	}
	
	
	public void setPaginationEnabled(boolean enable) {
		this.paginationEnabled = enable;
	}
	
	public void setPage(int page) {
		if(!paginationEnabled)
			return;
		
		this.page = page;
		
		this.put("page", page);
	}
	
	public int getPage() {
		return this.page;  
	}
	
	

	public void setPageSize(int pageSize) {
		if(!paginationEnabled)
			return;
		
		if(pageSize > 0)
			this.pageSize = pageSize;
		
		this.put("pageSize", pageSize);
		this.put("offset", getOffset());
	}
	
	public int getPageSize() {
		return this.pageSize;
	}
	
	
	
	private int getOffset() {
		int offset = 0;
		offset = (this.page -1) * pageSize;
		return offset;
	}
	
}
