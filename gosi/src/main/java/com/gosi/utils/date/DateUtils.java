package com.gosi.utils.date;

import com.gosi.utils.CommonUtils;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateUtils {
	public static final String PATTERN_DATE = "yyyy-MM-dd";
	public static final String PATTERN_SDATE = "yyyyMMdd";
	public static final String PATTERN_SDATE2 = "yyMMdd";
	public static final String PATTERN_DATE_KOR = "yyyy년 MM월 dd일";
	public static final String PATTERN_DATE_TIME = "yyyy-MM-dd HH.mm.ss";
	public static final String PATTERN_DATE_YM = "yyyy-MM";
	public static final String PATTERN_SDATE_YM = "yyyyMM";
	public static final String PATTERN_TIME = "HH.mm.ss";
	public static final String PATTERN_TIME_COL = "HH:mm:ss";
	public static final String PATTERN_STIME = "HHmmss";
	public static final String PATTERN_REGIDB_DATETIME = "yyyy-MM-dd HH:mm:ss";
	public static final String PATTERN_DATETIME = "yyyy/MM/dd HH:mm:ss";
	public static final String PATTERN_YMD = "yyyy/MM/dd";
	public static final String PATTERN_YMD2 = "yy/MM/dd";
	public static final String PATTERN_YMD3 = "yyMMddHHmm";
	public static final String PATTERN_YMD2_KOR = "yy년 MM개월 dd일";
	public static final String THE_FIRST_DAY = "01";

	public static String formatDate(String yyyyMMddHHmmssSSS) throws ParseException {
		return format("yyyy-MM-dd", yyyyMMddHHmmssSSS);
	}

	public static String formatDateHistory(String yyyyMMddHHmmssSSS) throws ParseException {
		return format("yyyy-MM-dd HH:mm:ss", yyyyMMddHHmmssSSS);
	}

	public static String formatTime(String yyyyMMddHHmmssSSS) throws ParseException {
		return format("HH.mm.ss", yyyyMMddHHmmssSSS);
	}

	public static String formatDate(Date date, String pattern) {
		if (date == null) {
			throw new NullPointerException("날짜 date 가 null 입니다.");
		} else if (pattern == null) {
			throw new NullPointerException("날짜 패턴 string 이 null 입니다.");
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			return sdf.format(date);
		}
	}

	public static String format(String pattern, String inDate) throws ParseException {
		String strDate = "";
		if (!CommonUtils.isEmpty(inDate)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			if (inDate.trim().length() > 14) {
				inDate = inDate.substring(0, 14);
			}

			Date date = sdf.parse(inDate);
			strDate = format(pattern, date);
		}

		return strDate;
	}

	public static String format(String pattern, Date date) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		return formatter.format(date);
	}

	public static String format(String inDate) throws ParseException {
		String strDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = sdf.parse(inDate);
		strDate = format("yyyy-MM-dd HH.mm.ss", date);
		return strDate;
	}

	public static String format(String inPattern, String outPattern, String inDate) throws ParseException {
		String strDate = "";
		if (!CommonUtils.isEmpty(inDate)) {
			SimpleDateFormat sdf = new SimpleDateFormat(inPattern);
			inDate = inDate.substring(0, inPattern.length());
			Date date = sdf.parse(inDate);
			strDate = format(outPattern, date);
		}

		return strDate;
	}

	public static String getToday() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(new Date());
	}

	public static String getCurrentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		return formatter.format(new Date());
	}

	public static String getCurrentDate2() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");
		return formatter.format(new Date());
	}

	public static String getYesterDay() {
		Calendar cal = Calendar.getInstance();
		cal.add(5, -1);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		return formatter.format(cal.getTime());
	}

	public static String getCurrentTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
		return formatter.format(new Date());
	}

	public static String getCurrentTimeMilli() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return formatter.format(new Date());
	}

	public static String getCurrentTimeMilli(String pattern) {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		return formatter.format(new Date());
	}

	public static String getDatePatternString(String inDate) throws ParseException {
		String strDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		Date date = sdf.parse(inDate);
		strDate = format("yyyy/MM/dd", date);
		return strDate;
	}

	public static Date addDate(Date date, int amount) {
		return add(date, 5, amount);
	}

	public static Date addHour(Date date, int amount) {
		return add(date, 11, amount);
	}

	public static Date add(String str, int field, int amount, String pattern) throws ParseException {
		Date date = parseDate(str, pattern);
		return add(date, field, amount);
	}

	public static Date add(Date date, int field, int amount) {
		if (date == null) {
			throw new NullPointerException("날짜 date 가 null 입니다.");
		} else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(field, amount);
			return calendar.getTime();
		}
	}

	public static Date parseDate(String str) throws ParseException {
		return parseDate(str, "yyyyMMdd");
	}

	public static Date parseDate(String str, String pattern) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Date date = sdf.parse(str);
		return date;
	}

	public static boolean isDate(String str, String pattern) throws ParseException {
		if (str == null) {
			throw new NullPointerException("날짜 string 이 null 입니다.");
		} else if (pattern == null) {
			throw new NullPointerException("날짜 패턴 string 이 null 입니다.");
		} else {
			boolean isDate = false;
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			Date date = sdf.parse(str);
			if (str.equals(sdf.format(date))) {
				isDate = true;
			}

			return isDate;
		}
	}

	public static boolean isFuture(Date date) {
		Date rightNow = rightNow();
		boolean isFuture = isCompareToDate(rightNow, date);
		if (isFuture && formatDate(date).equals(formatDate(rightNow))) {
			isFuture = false;
		}

		return isFuture;
	}

	public static boolean isFuture(String str) throws ParseException {
		return isFuture(str, "yyyyMMdd");
	}

	public static boolean isFuture(String str, String pattern) throws ParseException {
		Date date = parseDate(str, pattern);
		return isFuture(date);
	}

	public static boolean isPast(Date date) {
		Date rightNow = rightNow();
		boolean isPast = isCompareToDate(date, rightNow);
		if (isPast && formatDate(date).equals(formatDate(rightNow))) {
			isPast = false;
		}

		return isPast;
	}

	public static boolean isPast(String str) throws ParseException {
		return isPast(str, "yyyyMMdd");
	}

	public static boolean isPast(String str, String pattern) throws ParseException {
		Date date = parseDate(str, pattern);
		return isPast(date);
	}

	public static boolean isCompareToDate(Date from, Date to) {
		if (from == null) {
			throw new NullPointerException("from날짜 date 가 null 입니다.");
		} else if (to == null) {
			throw new NullPointerException("to날짜 date 가 null 입니다.");
		} else {
			boolean isCompare = true;
			int diff = from.compareTo(to);
			if (diff >= 0) {
				isCompare = false;
			}

			return isCompare;
		}
	}

	public static boolean isCompareToDate(String date, String date1) throws ParseException {
		boolean isCompare = false;
		Date toDate = parseDate(date);
		Date fromDate = parseDate(date1);
		isCompare = isCompareToDate(toDate, fromDate);
		return isCompare;
	}

	public static boolean isCompareToDateHM(String date, String date1) throws ParseException {
		boolean isCompare = false;
		Date toDate = parseDate(date, "yyyyMMddHHmm");
		Date fromDate = parseDate(date1, "yyyyMMddHHmm");
		isCompare = isCompareToDate(toDate, fromDate);
		return isCompare;
	}

	public static boolean isCompareToDateYM(String dateYM, String date1YM) throws ParseException {
		return isCompareToDate(dateYM + "01", date1YM + "01");
	}

	public static int compareToDate(String from, String to) throws ParseException {
		Date fromDate = parseDate(from);
		Date toDate = parseDate(to);
		int diff = toDate.compareTo(fromDate);
		return diff;
	}

	public static int compareToDate(String from, String to, String pattern) throws ParseException {
		Date fromDate = parseDate(from, pattern);
		Date toDate = parseDate(to, pattern);
		int diff = toDate.compareTo(fromDate);
		return diff;
	}

	public static int compareToDateYM(String fromYM, String toYM) throws ParseException {
		return compareToDate(fromYM + "01", toYM + "01");
	}

	public static String formatDate(Date date) {
		return formatDate(date, "yyyyMMdd");
	}

	public static Date rightNow() {
		Calendar rightNow = Calendar.getInstance();
		return rightNow.getTime();
	}

	public static String getLastWeekDay() {
		Calendar cal = Calendar.getInstance();
		cal.add(5, -7);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		return formatter.format(cal.getTime());
	}

	public static String getGlobalDate() {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		TimeZone tz = TimeZone.getTimeZone("Europe/London");
		df.setTimeZone(tz);
		return df.format(date);
	}

	public static Long getGlobalDateWithLong() throws ParseException {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		TimeZone tz = TimeZone.getTimeZone("Europe/London");
		df.setTimeZone(tz);
		String strDate = df.format(date);
		date = parseDate(strDate, "yyyyMMddHHmmssSSS");
		return date.getTime();
	}

	public static Date convertFrom(Long date) {
		Date d = new Date(date);
		return d;
	}

	public static boolean truncatedEqualToday(Date dt) {
		if (dt == null) {
			return false;
		} else {
			return org.apache.commons.lang.time.DateUtils.truncatedEquals(dt, new Date(), 5);
		}
	}

	public static Date getDateFromTimeStamp(long timestamp) {
		Date d = new Date(timestamp);
		return d;
	}

	public static int getHourGap(long timestamp1, long timestamp2) {
		Date d1 = new Date(timestamp1);
		Date d2 = new Date(timestamp2);
		int hh1 = get24HourFromDate(d1);
		int hh2 = get24HourFromDate(d2);
		int gap = Math.abs(hh1 - hh2);
		return gap;
	}

	public static int getDayOfWeek(Date d) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		return calendar.get(7);
	}

	public static int get24HourFromDate(Date d) {
		String hh = (new SimpleDateFormat("HH")).format(d);
		int iHH = Integer.parseInt(hh);
		return iHH;
	}

	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

	public static boolean isMorning(int hour) {
		return hour >= 4 && hour < 9;
	}

	public static boolean isDay(int hour) {
		return hour >= 9 && hour < 19;
	}

	public static boolean isNight(int hour) {
		if (hour >= 19 && hour < 24) {
			return true;
		} else {
			return hour >= 0 && hour < 4;
		}
	}

	public static Date getPastDate(int pastDays) {
		Date past = addDate(new Date(), -pastDays);
		return past;
	}

	public static Date getPastDate(Date dd, int pastDays) {
		Date past = addDate(dd, -pastDays);
		return past;
	}

	public static Long getTimestamp(Date t) {
		return t.getTime();
	}
}