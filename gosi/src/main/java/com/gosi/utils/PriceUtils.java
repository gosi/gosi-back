package com.gosi.utils;

import java.util.ArrayList;
import java.util.List;


public class PriceUtils {
	
	
	public static List<Integer> getPriceRangeList(String startPrice,String endPrice,String interval)
	{
		Integer intSPrice = Integer.parseInt(startPrice);
		Integer intEPrice = Integer.parseInt(endPrice);
		Integer intInterval = Integer.parseInt(interval);
		
		Integer first = intSPrice + intInterval;
		
		List<Integer> priceList = new ArrayList<>();
		if(first > intEPrice )
		{
			intEPrice = first;
			for(int i=intSPrice; i <=intEPrice; i=i+intInterval )
			{
				priceList.add(i);
			}
			
		}else {
			
			for(int i=intSPrice; i <=intEPrice; i=i+intInterval )
			{
				priceList.add(i);
			}
		}
		
		
		
		
		Integer maxPrice = priceList.get(priceList.size()-1);
		
		if(maxPrice < intEPrice)
		{
			priceList.add(maxPrice+intInterval);
		}
		
		return priceList; 
	}

}
