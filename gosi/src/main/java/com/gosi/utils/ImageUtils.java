package com.gosi.utils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

import com.gosi.utils.StringUtils;

public class ImageUtils {
	
	public static String makePublicAccessUrl(String publicUrl, String otherUrl)
	{
		if(StringUtils.isNullOrEmpty(otherUrl))
		{
			return "";
		}
		
		if(otherUrl.trim().matches("^http://.*") || otherUrl.trim().matches("^HTTP://.*"))
		{
			return otherUrl;
		}
		
		StringBuffer sb = new StringBuffer();
		
		sb.append(publicUrl);
		sb.append(otherUrl);
		
		
		return sb.toString();
	}
	
	
	public static byte[] getImage(String url) {
		
		try{
			
			URL imageUrl = new URL(url);
			InputStream is = imageUrl.openStream();
			ByteArrayOutputStream os = new ByteArrayOutputStream();

			byte[] b = new byte[2048];
			int length;

			while ((length = is.read(b)) != -1) {
				os.write(b, 0, length);
			}

			is.close();
			os.close();
			
			return os.toByteArray();
			
		}catch(Exception e)
		{
			return null;
		}
		
	}

}
