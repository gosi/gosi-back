package com.gosi.utils.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.gosi.db.DbHandler;
import com.gosi.db.dto.Book;
import com.gosi.db.dto.Order;
import com.gosi.db.dto.OrderMegazine;
import com.gosi.db.dto.StandingOrder;
import com.gosi.db.dto.WebBook;
import com.gosi.license.key.LicenseKeyGenerator;

public class Input {
	
	
	public static String getDate() {
		
		SimpleDateFormat date = new SimpleDateFormat("yyyyMMddHHmm");
		Date dt = new Date();
		Calendar cal = new GregorianCalendar(Locale.KOREA);
		cal.setTime(dt);
		return date.format(cal.getTime());
	}
	
	public static String getDay() {
		
		SimpleDateFormat date = new SimpleDateFormat("dd");
		Date dt = new Date();
		Calendar cal = new GregorianCalendar(Locale.KOREA);
		cal.setTime(dt);
		return date.format(cal.getTime());
	}
	
	public static String getMonth() {
		
		SimpleDateFormat date = new SimpleDateFormat("MM");
		Date dt = new Date();
		Calendar cal = new GregorianCalendar(Locale.KOREA);
		cal.setTime(dt);
		return date.format(cal.getTime());
	}
	
	public static String getYear() {
		
		SimpleDateFormat date = new SimpleDateFormat("yyyy");
		Date dt = new Date();
		Calendar cal = new GregorianCalendar(Locale.KOREA);
		cal.setTime(dt);
		return date.format(cal.getTime());
	}
	
	public static OrderMegazine getOrderMegazine(HttpServletRequest request) {
		OrderMegazine order = new OrderMegazine();
		
		String payType = getValue("payType", request); //결제 방식
		if(payType.equals("card")) order.setPayStatus("결제완료");
		else order.setPayStatus("입금대기");
		
		String memYn = getValue("memYn", request);
		order.setMemYn(memYn);
		if(memYn.equals("Y"))
			order.setUserId(getValue("id", request));
		else 
			order.setUserId(getValue("name", request));
		
		String name = getValue("name", request);
		String addr1 = getValue("addr1", request);
		String addr2 = getValue("addr2", request);
		String zipCd = getValue("zipCd", request);
		String tel = getValue("tel", request);
		String email = getValue("email", request);
		String jNo = getValue("jNo", request);
		
		String startYear = getValue("startYear", request);
		String startMonth = getValue("startMonth", request);
		String endYear = getValue("endYear", request);
		String endMonth = getValue("endMonth", request);
		int totalCnt = 0;
		if(startYear.equals(endYear))
			totalCnt = Integer.parseInt(endMonth) - Integer.parseInt(startMonth)+1;
		else
			totalCnt = Integer.parseInt(endMonth) - Integer.parseInt(startMonth)+1+12;
		order.setTotalCnt(totalCnt);
		order.setCurCnt(1);
		order.setStartYear(startYear);
		order.setStartMonth(startMonth);
		order.setEndYear(endYear);
		order.setEndMonth(endMonth);
		order.setjNo(jNo);
		order.setEmail(email);
		order.setTel(tel);
		order.setZipCd(zipCd);
		order.setAddr2(addr2);
		order.setAddr1(addr1);
		order.setName(name);

		return order;
	}
	
	public static StandingOrder getStandingOrder(JSONObject job) throws ParseException {
		
		StandingOrder standingOrder = new StandingOrder();
		
		String title = "", startYear = "", startMonth="", endYear="", endMonth = "", payType = "", pwd = "";
		int bookId = 0, numOfCopies = 1, totalPrice = 0, price = 0;
		String orderName = "", orderAddr1="", orderAddr2="", orderZipCode = "", orderTel= "", orderEmail = "";
		String deliveryName = "", deliveryAddr1 = "", deliveryAddr2 = "", deliveryZipCode = "", deliveryTel = "", deliveryEmail = "", deliveryMemo = "";
		
			bookId = (int)(long)job.get("bookId");
			if(bookId==111) title = "월간고시계 12개월 구독";
			else title = "월간고시계 6개월 구독";
			numOfCopies = (int)(long)job.get("numOfCopies");
			startYear = (String)job.get("startYear");
			startMonth = (String)job.get("startMonth");
			endYear = (String)job.get("endYear");
			endMonth = (String)job.get("endMonth");
			totalPrice = (int)(long)job.get("totalPrice");
			price = (int)(long)job.get("price");
			payType = (String)job.get("payType");

			orderName = (String)job.get("orderName");
			orderAddr1 = (String)job.get("orderAddr1");
			orderAddr2 = (String)job.get("orderAddr2");
			orderZipCode = (String)job.get("orderZipCode");
			orderTel = (String)job.get("orderTel");
			orderEmail = (String)job.get("orderEmail");
			deliveryName = (String)job.get("deliveryName");
			deliveryAddr1 = (String)job.get("deliveryAddr1");
			deliveryAddr2 = (String)job.get("deliveryAddr2");
			deliveryZipCode =(String)job.get("deliveryZipCode");
			deliveryTel = (String)job.get("deliveryTel");
			deliveryEmail = (String)job.get("deliveryEmail");
			deliveryMemo = (String)job.get("deliveryMemo");
					
			if((String)job.get("orderPwd")!=null)
				pwd = (String)job.get("orderPwd");

		
		String hash = "";
		try {
			hash = LicenseKeyGenerator.generate(orderName, pwd);
		} catch (Exception var7) {
			var7.printStackTrace();
		}

		standingOrder.setPrice(price);
		standingOrder.setBookId(bookId);
		standingOrder.setTitle(title);
		standingOrder.setNumOfCopies(numOfCopies);
		standingOrder.setStartYear(startYear);
		standingOrder.setStartMonth(startMonth);
		standingOrder.setEndYear(endYear);
		standingOrder.setEndMonth(endMonth);
		standingOrder.setTotalPrice(totalPrice);
		standingOrder.setPayType(payType);
		standingOrder.setComment(startYear.concat("-").concat(startMonth));
		
		standingOrder.setOrderName(orderName);
		standingOrder.setOrderAddr1(orderAddr1);
		standingOrder.setOrderAddr2(orderAddr2);
		standingOrder.setOrderZipCode(orderZipCode);
		standingOrder.setOrderTel(orderTel);
		standingOrder.setOrderEmail(orderEmail);
		
		standingOrder.setDeliveryName(deliveryName);
		standingOrder.setDeliveryAddr1(deliveryAddr1);
		standingOrder.setDeliveryAddr2(deliveryAddr2);
		standingOrder.setDeliveryZipCode(deliveryZipCode);
		standingOrder.setDeliveryTel(deliveryTel);
		standingOrder.setDeliveryEmail(deliveryEmail);
		standingOrder.setDeliveryMemo(deliveryMemo);
		
		standingOrder.setOrderPwd(pwd);
		standingOrder.setOrderPwdHash(hash);
		
		return standingOrder;
	}
	
	public static Order getOrder(JSONObject job2, JSONObject job) throws ParseException {
		
		Order order = new Order();
		
		String payType = "", pwd = "", id="";
		int totalPrice = 0, deliveryFee = 0;
		String orderName = "", orderAddr1="", orderAddr2="", orderZipCode = "", orderTel= "", orderEmail = "";
		String deliveryName = "", deliveryAddr1 = "", deliveryAddr2 = "", deliveryZipCode = "", deliveryTel = "", deliveryEmail = "", deliveryMemo = "";
		
			id = (String)job2.get("id");
			deliveryFee = (int)(long)job2.get("deliveryFee");
			totalPrice = (int)(long)job2.get("totalPrice");
			payType = (String)job2.get("payType");

			orderName = (String)job2.get("orderName");
			orderAddr1 = (String)job2.get("orderAddr1");
			orderAddr2 = (String)job2.get("orderAddr2");
			orderZipCode = (String)job2.get("orderZipCode");
			orderTel = (String)job2.get("orderTel");
			orderEmail = (String)job2.get("orderEmail");
			deliveryName = (String)job2.get("deliveryName");
			deliveryAddr1 = (String)job2.get("deliveryAddr1");
			deliveryAddr2 = (String)job2.get("deliveryAddr2");
			deliveryZipCode =(String)job2.get("deliveryZipCode");
			deliveryTel = (String)job2.get("deliveryTel");
			deliveryEmail = (String)job2.get("deliveryEmail");
			deliveryMemo = (String)job2.get("deliveryMemo");
					
			if(job2.get("orderPwd")!=null)
				pwd = (String)job2.get("orderPwd");
			

		
		String hash = "";
		try {
			hash = LicenseKeyGenerator.generate(orderName, pwd);
		} catch (Exception var7) {
			var7.printStackTrace();
		}
		
		int bookId = (int) (long) job.get("bookId");
		int bookCate = (int)(long)  job.get("bookCate");
		String type = (String) job.get("bookType");
		int count = (int)(long)  job.get("count");
		int price = (int)(long)  job.get("price");
		int salePrice = (int)(long)  job.get("salePrice");
		int cartKeyId = 0;
		if(job.get("cartKeyId")!=null) 
			cartKeyId = (int)(long)  job.get("cartKeyId");
		String comment = (String) job.get("comment");
		String title = (String)job.get("title");
		
		order.setBookId(bookId);
		order.setBookCate(bookCate);
		order.setBookType(type);
		order.setCount(count);
		order.setPrice(price);
		order.setSalePrice(salePrice);
		order.setCartKey(cartKeyId);
		order.setComment(comment);
		order.setTitle(title);
		
		
		if(!id.equals(""))
			order.setId(id);
		order.setDeliveryFee(deliveryFee);
		order.setTotalPrice(totalPrice);
		order.setPayType(payType);
		order.setOrderName(orderName);
		order.setOrderAddr1(orderAddr1);
		order.setOrderAddr2(orderAddr2);
		order.setOrderZipCode(orderZipCode);
		order.setOrderTel(orderTel);
		order.setOrderEmail(orderEmail);
		order.setDeliveryName(deliveryName);
		order.setDeliveryAddr1(deliveryAddr1);
		order.setDeliveryAddr2(deliveryAddr2);
		order.setDeliveryZipCode(deliveryZipCode);
		order.setDeliveryTel(deliveryTel);
		order.setDeliveryEmail(deliveryEmail);
		order.setDeliveryMemo(deliveryMemo);
		
		order.setOrderPwd(pwd);
		order.setOrderPwdHash(hash);
		
		return order;
	}
	
	public static Order getOrderCart(HttpServletRequest request, JSONObject job, DbHandler dbHandler) {
		
		Order order = new Order();

		int bookId = 0; int price = 0; int salePrice = 0; int bookCate = 0;int cartKeyId = 0;
		String type = "", title = "";
		String comment = (String) job.get("comment"); 
		if(comment==null)
			comment = "";
		int count = (int)(long)  job.get("count"); 
		
		if(job.get("cartKeyId")!=null) 
			cartKeyId = (int)(long)  job.get("cartKeyId");
		
		if(job.get("bookId")==null) {
			if(comment.length()>3) {
				if(comment.charAt(4)=='-') {
					Map<String, Object> param = new HashMap<>();
					String[] arr = comment.split("-");
					String searchWord = arr[0];
					param.put("search", searchWord);
					Map<String, Object> map = dbHandler.sendMegazineInfo(param);
					bookId = (int) map.get("bookId");
					title = (String) map.get("title");
					bookCate = 0;
					type = "BOOK";
					price = (int) map.get("price");
					salePrice = (int) map.get("salePrice");
					
				}
			}
		}
		else {
			bookId = (int) (long) job.get("bookId"); 
			price = (int)(long)  job.get("price"); 
			salePrice = (int)(long)  job.get("salePrice");
			bookCate = (int)(long)  job.get("bookCate"); 
			type = (String) job.get("bookType"); 
			title = (String)job.get("title"); 
		}

		order.setBookId(bookId);
		order.setBookCate(bookCate);
		order.setBookType(type);
		order.setCount(count);
		order.setPrice(price);
		order.setSalePrice(salePrice);
		order.setCartKey(cartKeyId);
		order.setComment(comment);
		order.setTitle(title);
		
		return order;
	}
	
	public static Book getBook(MultipartHttpServletRequest request) {
		
		Book book = new Book();
		
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String isbn = getValue("isbn", request); 
		int pageNum = getIntValue("pageNum", request);
		String panNo =  getValue("panNo", request); 
		int price = getIntValue("price", request);
		int salePrice = getIntValue("salePrice", request);
		String pubsEDt = request.getParameter("pubsEDt");
		int cate = getIntValue("cate", request); // -> int 로 분류 해야함
		
		String sellStatus = request.getParameter("sellStatus");
		String authorComment =  getValue("authorComment", request); 
		String bookComment =  getValue("bookComment", request); 
		String indexComment =  getValue("indexComment", request); 
		String status = request.getParameter("status");
		int bookId = 0;
		if(status.equals("update"))
			bookId = getIntValue("bookId", request);
		
		book.setTitle(title);
		book.setAuthor(author);
		book.setIsbn(isbn);
		book.setPageNum(pageNum);
		book.setPanNo(panNo);
		book.setPrice(price);
		book.setSalePrice(salePrice);
		book.setPubsEDt(pubsEDt);
		book.setCate(cate);
		book.setSellStatus(sellStatus);
		book.setAuthorComment(authorComment);
		book.setBookComment(bookComment);
		book.setIndexComment(indexComment);
		book.setBookId(bookId);
		
		return book;
	}
	
	public static Book getMegazine(MultipartHttpServletRequest request) {
		
		Book book = new Book();
		
		String title = getValue("title", request);
		int price = getIntValue("price", request);
		int salePrice = getIntValue("salePrice", request);
		
		String sellStatus = getValue("sellStatus", request);
		String status = request.getParameter("status");
		int bookId = 0;
		if(status.equals("update"))
			bookId = getIntValue("bookId", request);
		
		book.setTitle(title);
		book.setPrice(price);
		book.setSalePrice(salePrice);
		book.setSellStatus(sellStatus);
		book.setBookId(bookId);
		
		return book;
	}

	public static WebBook getWebbook(MultipartHttpServletRequest request) {
		
		WebBook book = new WebBook();
		
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String year = request.getParameter("year");
		String month = request.getParameter("month");
		String totalPage = request.getParameter("totalPage");
		int price = getIntValue("price", request);
		String cate = getValue("cate", request); 
		
		String status = request.getParameter("status");
		int bookId = 0;
		if(status.equals("update"))
			bookId = getIntValue("bookId", request);
		
		book.setTitle(title);
		book.setAuthor(author);
		book.setTotalPage(totalPage);
		book.setPrice(price);
		book.setCate(cate);
		book.setBookId(bookId);
		book.setyMon(year.concat("-").concat(month));
		
		return book;
	}
	
	public static String getValue(String key, MultipartHttpServletRequest request) {

		if(request.getParameter(key)!=null) return request.getParameter(key);
		return "";
	}
	
	public static int getIntValue(String key, MultipartHttpServletRequest request) {
		
		if(request.getParameter(key)!=null) return Integer.parseInt(request.getParameter(key));
		return 0;
	}
	
	public static String getValue(String key, HttpServletRequest request) {

		if(request.getParameter(key)!=null) return request.getParameter(key);
		return "";
	}
	
	public static String getValue(String key, HttpServletRequest request, String defaultValue) {

		if(request.getParameter(key)!=null) return request.getParameter(key);
		return defaultValue;
	}
	
	public static int getIntValue(String key, HttpServletRequest request, int defaultValue) {
		
		if(request.getParameter(key)!=null) return Integer.parseInt(request.getParameter(key));
		return defaultValue;
	}
	
	public static List<Map<String, Object>> getCategory(String mainId) {
		List<Map<String, Object>> list = new ArrayList<>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		if (mainId.equals("1")) {
			map.put("subId", 1);
			map.put("categoryName", "웹으로 보기");
			list.add(map);
			map = new HashMap<String, Object>();
			map.put("subId", 2);
			map.put("categoryName", "정기구독");
			list.add(map);
			map = new HashMap<String, Object>();
			map.put("subId", 3);
			map.put("categoryName", "잡지구매");
			list.add(map);
		} else if (mainId.equals("2")) {
			map.put("subId", 1);
			map.put("categoryName", "수험도서");
			list.add(map);
			map = new HashMap<String, Object>();
			map.put("subId", 2);
			map.put("categoryName", "전문도서");
			list.add(map);
		} else if (mainId.equals("3")) {
			map.put("subId", 1);
			map.put("categoryName", "도서보기");
			list.add(map);
		} else if (mainId.equals("4")) {
			map.put("subId", 1);
			map.put("categoryName", "변호사시험");
			list.add(map);
		} else if (mainId.equals("0")) {
			map.put("subId", 1);
			map.put("categoryName", "공지사항");
			list.add(map);
			map = new HashMap<String, Object>();
			map.put("subId", 2);
			map.put("categoryName", "매거진");
			list.add(map);
		}

		return list;
	}

	public static int getPubsId(String mainId, String subId) {
		
		if (mainId.equals("2")) {
			if (subId.equals("1")) {
				return 1; //수험도서
			}
			else if(subId.equals("2")) {
				return 2; //전문도서
			}
		} else if (mainId.equals("3")) {
			return 3; //미디어북
		}
		//변호사 시험은 현재 운영하지 않고, 공지사항은 다른 request가 있음

		return 0;
	}
	
	public static String getBody(HttpServletRequest request) throws IOException {
		 
        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
 
        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw ex;
                }
            }
        }
 
        body = stringBuilder.toString();
        return body;
    }
}