package com.gosi.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import com.gosi.utils.StringUtils;
import com.gosi.utils.date.DateUtils;

public class Validation {

	public static boolean checkBrand(String brandCode) {
		if (StringUtils.isNullOrEmpty(brandCode)) {
			return false;
		}

		return true;
	}

	public static boolean checkBrands(String[] brandCodes) {
		if (brandCodes == null) {
			return false;
		}

		for (String brand : brandCodes) {
			if (!checkBrand(brand)) {
				return false;
			}
		}

		return true;
	}

	public static boolean checkDate(String date) {
		try {
			return DateUtils.isDate(date, "yyyy-MM-dd");
		} catch (ParseException e) {
			return false;
		}
	}

	public static boolean checkStartDateAndEndDate(String sdate, String edate) {

		try {
			if (checkDate(sdate) && checkDate(edate)) {

				if (DateUtils.isPast(edate)) {
					return false;
				}

				return true;

			}
		} catch (ParseException e) {
			e.printStackTrace();

		}

		return false;
	}
	
	/*
	 * 신장률 계산을 위해 사용됨.
	 */
	public static String[] checkDateDiff(String sdate, String edate) {
		String startDay, endDay;
		try {
			SimpleDateFormat transFormat = new SimpleDateFormat("yyyyMMdd");
			Date end = transFormat.parse(edate);
			Date start = transFormat.parse(sdate);
			
			long diff = end.getTime() - start.getTime();
			long diffDays = diff / (24 * 60 * 60 * 1000);
			
			Calendar startCal = new GregorianCalendar(Locale.KOREA);
			Calendar endCal = new GregorianCalendar(Locale.KOREA);
			startCal.setTime(start);
			endCal.setTime(start);
			
			if (diffDays == 0) {
				startCal.add(Calendar.DATE, -1);
				endCal.add(Calendar.DATE, -1);
			} else {
				// 종료일은 무조건 이전의 시작일에서 -1day 이다.
				endCal.add(Calendar.DATE, (int) -1);
				// 시작일은 새로 설정된 종료일을 기준으로, 기간의 차이일만큼 빼야한다.
				startCal.setTimeInMillis(endCal.getTimeInMillis());
				startCal.add(Calendar.DATE, (int) -diffDays);
			}
			startDay = transFormat.format(startCal.getTime());
			endDay = transFormat.format(endCal.getTime());

			String[] ret = { startDay, endDay };
			return ret;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * 현시간 기준 금주 일~토 날짜 구하기
	 */
	public static String[] getThisWeekDt() {
		try {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd"); 
			int iDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)-1; 
			Date dt = new Date();
			long unix_time = dt.getTime() / 1000;
			long startDate = unix_time - iDayOfWeek * 86400;
			long endDate = unix_time;
			
			for (int y=iDayOfWeek; y < 6; y++) {
				endDate = endDate + 86400;
			}
			Date dt2 = new Date(startDate*1000);
			Date dt3 = new Date(endDate*1000);
			String sStart = sdf.format(dt2);
			String sEnd = sdf.format(dt3);
			
			String[] ret = { sStart, sEnd };
			return ret;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*public static String[] checkDateDiff(String sdate, String edate) {
		String startDay, endDay;
		try {
			SimpleDateFormat transFormat = new SimpleDateFormat("yyyyMMdd");
			Date end = transFormat.parse(edate);
			Date start = transFormat.parse(sdate);
			
			long diff = end.getTime() - start.getTime();
			long diffDays = diff / (24 * 60 * 60 * 1000);

			Calendar startCal = new GregorianCalendar(Locale.KOREA);
			Calendar endCal = new GregorianCalendar(Locale.KOREA);
			startCal.setTime(start);
			endCal.setTime(end);
			if (diffDays == 0) {
				startCal.add(Calendar.DATE, -1);
				endCal.add(Calendar.DATE, -1);
			} else {
				startCal.add(Calendar.DATE, (int) -diffDays);
				endCal.add(Calendar.DATE, (int) -diffDays);
			}
			startDay = transFormat.format(startCal.getTime());
			endDay = transFormat.format(endCal.getTime());

			String[] ret = { startDay, endDay };
			return ret;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}*/
	
	public static String getBaseDay(List<String> date, boolean isStart) {
		List<String> baseStartDt = date;
		
		String temp;
		int cnt = 0;
		
		for (int i = baseStartDt.size(); i > 0; i--) { //
			for (int j = 0; j < i - 1; j++) {
				cnt++;
				if (baseStartDt.get(j).compareTo(baseStartDt.get(j+1)) > 0) {
					temp = baseStartDt.get(j);
					baseStartDt.add(j, baseStartDt.get(j+1));
					baseStartDt.remove(j+1);
					baseStartDt.add(j+1, temp);
					baseStartDt.remove(j+2);
				}
			}
		}
		return isStart ? baseStartDt.get(0) : baseStartDt.get(baseStartDt.size()-1);
	}

	/**
	 * 현재 시간에서 기준 날짜에서 지정된 값만큼 뺀 날짜를 구함.
	 * @param date
	 * @param isStart
	 * @return
	 */
	public static String getDefaultDay(String dateFormat, int addDay) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		cal.add(Calendar.DATE, addDay);
		
		long time = cal.getTimeInMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat(dateFormat);
		String str = dayTime.format(new Date(time));
		return str;
	}
}
